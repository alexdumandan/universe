<div style="width:100%;height:100%;text-align:center;" id="loader_container" class="loader-container">
  <i id="loader_icon" style="font-size:100px" class='fa fa-spin fa-circle-o-notch loader-icon'></i>
  <div class="input-group input-medium  loader-filecontainer" style="width:100%;margin:auto;margin-bottom:30px;display:none" id="hots_file_container">
    <input type="file" class="form-control input-lg" id="hots_file" style="width:300px;cursor:pointer" />
    <span class="input-group-btn">
      <button id="upload_hots_btn" class="btn green btn-lg" disabled>
           <i class="material-icons">file_upload</i>Upload HOTs
      </button>
    </span>
  </div>
  <h1 id="loader_text"> Checking open orders..</h1>
  <div class="progress progress-striped active" id="loader_progress_bar_container" style="display:none">
  	<div id="loader_progress_bar" class="progress-bar" role="progressbar">
  	</div>
  </div>
</div>
