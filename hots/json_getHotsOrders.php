<?php
// header('Content-Type: text/event-stream');
// header('Cache-Control: no-cache');

include "../_libs/db_connect.php";
include "../functions.php";
date_default_timezone_set('US/Eastern');


$tableNamePrefix = "order_";
$group = $_GET["group"];
$arrayFields = array(

    $tableNamePrefix . "id",
    $tableNamePrefix . "account_number",
    $tableNamePrefix . "SO_number",
    $tableNamePrefix . "placed_by",
    $tableNamePrefix . "sales_mgr",
    $tableNamePrefix . "team_IS",
    $tableNamePrefix . "void_date",
    $tableNamePrefix . "customer_PO",
    $tableNamePrefix . "amount",
    $tableNamePrefix . "ship_from",
    $tableNamePrefix . "ship_via",
    $tableNamePrefix . "assign_to",
    $tableNamePrefix . "priority",
    $tableNamePrefix . "item_status",

);


$query = "SELECT ".implode(" , ", $arrayFields)." FROM tb_orders WHERE order_sales_group = '$group' AND order_source = 'HOT' AND DATE(order_uploaded_date) = '".date("Y-m-d")."' ORDER BY order_priority DESC";
echo "{\"message\":\"Downloading..\"}";
flush();
ob_flush();
// ob_clean();
echo $query;
$arr = array();
$i = 0;

$result = mysqli_query($con, $query);

$total = mysqli_num_rows($result);


while($r = mysqli_fetch_assoc($result)) {

    $arr[$i]["id"] = $r[$tableNamePrefix . "id"];
    $arr[$i]["accountNumber"] = $r[$tableNamePrefix . "account_number"];
    $arr[$i]["SOnumber"] = $r[$tableNamePrefix . "SO_number"];
    $arr[$i]["placedBy"] = $r[$tableNamePrefix . "placed_by"];
    $arr[$i]["salesMgr"] = $r[$tableNamePrefix . "sales_mgr"];
    $arr[$i]["teamIS"] = $r[$tableNamePrefix . "team_IS"];
    $arr[$i]["voidDate"] = "-";
    if ($r[$tableNamePrefix . "void_date"] != '0000-00-00 00:00:00'){
      $arr[$i]["voidDate"] = date('Y-m-d',strtotime($r[$tableNamePrefix . "void_date"]));
    }


    $arr[$i]["customerPO"] = $r[$tableNamePrefix . "customer_PO"];
    $arr[$i]["amount"] = $r[$tableNamePrefix . "amount"];
    $arr[$i]["shipFrom"] = $r[$tableNamePrefix . "ship_from"];
    $arr[$i]["shipVia"] = $r[$tableNamePrefix . "ship_via"];
    $arr[$i]["assignTo"] = $r[$tableNamePrefix . "assign_to"];

    $arr[$i]["priority"] = $r[$tableNamePrefix . "priority"];
    $arr[$i]["itemStatus"] = $r[$tableNamePrefix . "item_status"];
    $i++;

    echo ",{\"total\":\"".$total."\",\"current\":\"".($i+1)."\",\"message\":\"Downloading (".($i+1)." of ".$total.")\"}";
    flush();
    ob_flush();
    // ob_clean();
}

echo json_encode($arr);


 ?>
