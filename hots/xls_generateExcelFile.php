
<?php

include "../_libs/db_connect.php";
include "../functions.php";
require "../../_utils/userDetect.php";

$groupName = $_GET["groupName"];
$startDate = $_GET["dateStart"] . " 00:00:00";
$endDate = $_GET["dateEnd"]. " 23:59:59";

header("Content-type: application/vnd.ms-excel");
header("Content-Disposition: attachment; filename=hots.xls");

$arrayFields = [

	"order_id",
	"order_uploaded_date",
	"order_account_number",
	"order_team_IS",
	"order_team_OS",
	"order_customer_PO",
	"order_placed_by",
	"order_SO_number",
	"order_void_date",
	"order_amount",
	"order_ship_from",
	"order_ship_via",
	"order_item_status",
	"order_notes",
	"order_assign_to",


];


?>

<table>
		<tr style="font-weight:bold;">
			<td>Item ID</td>
			<td>Date Uploaded</td>
			<td>Account Number</td>
			<td>Team IS</td>
			<td>Team OS</td>
			<td>Customer PO</td>
			<td>Placed By</td>
			<td>SO number</td>
			<td>Void Date</td>
			<td>Amount</td>
			<td>Ship From</td>
			<td>Ship Via</td>
			<td>Item Status</td>
			<td>Note</td>
			<td>Assigned To</td>
    </tr>
<?php

$query="SELECT ".implode($arrayFields,",")." FROM tb_orders WHERE "
			 . " order_uploaded_date BETWEEN '" . $startDate . "' AND '" . $endDate ."'"
				. " AND order_sales_group = '" . $groupName . "' ;" ;
echo $query;
$sql_result = mysqli_query($con, $query);

while($r = mysqli_fetch_assoc($sql_result)){
?>

	<tr>
		<td><?php echo  $r['order_id']; ?></td>
		<td><?php echo  format_to_date_only($r['order_uploaded_date']); ?></td>
		<td><?php echo  $r['order_account_number']; ?></td>
		<td><?php echo  $r['order_team_IS']; ?></td>
		<td><?php echo  $r['order_team_OS'];; ?></td>
		<td><?php echo  $r['order_customer_PO']; ?></td>
		<td><?php echo  get_user_fullname_by_IS($r['order_placed_by'],"US"); ?></td>
		<td><?php echo  $r['order_SO_number']; ?></td>
		<td><?php echo  format_to_date_only($r['order_void_date']); ?></td>
		<td><?php echo  $r['order_amount']; ?></td>
		<td><?php echo  $r['order_ship_from']; ?></td>
		<td><?php echo  $r['order_ship_via']; ?></td>
		<td><?php echo  $r['order_item_status']; ?></td>
		<td><?php echo  get_last_note($r['order_notes']); ?></td>
		<td><?php echo  get_name($r['order_assign_to'],$con);?></td>
	</tr>

<?php
}
?>

</table>
