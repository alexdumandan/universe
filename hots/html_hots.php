
<div class="page-title" id="page_title">
  HOTs
  <div class="page-toolbar" style="float:right">
      <a class="btn purple-seance btn-sm" href="../attachments/sales_hub/_sitefiles/HOTs SALES HUB.xlsm">
        <i class="material-icons">file_download</i> Download Macro
      </a>
      <a class="btn green-jungle btn-sm" data-toggle="modal" href="#open_order_download_data_modal" >
        <i class="material-icons">grid_on</i> Generate Excel File
      </a>
  </div>
</div>


<div class="row">
	<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
		<div class="dashboard-stat green">
			<div class="visual">
				<i class="fa fa-dollar"></i>
			</div>
			<div class="details">
				<div class="number" id="order_total_amount">
					 <i class='fa fa-spin fa-circle-o-notch'></i>
				</div>
				<div class="desc" >
					 Total amount of HOTs orders
				</div>
			</div>
			<a class="more" href="javascript:;" style="cursor:context-menu">
			<!-- View more <i class="m-icon-swapright m-icon-white"></i> -->
      &nbsp
			</a>
		</div>
	</div>
  <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
		<div class="dashboard-stat blue-steel">
			<div class="visual">
				<i class="fa fa-shopping-cart"></i>
			</div>
			<div class="details">
				<div class="number" id="order_total_count">
					 <i class='fa fa-spin fa-circle-o-notch'></i>
				</div>
				<div class="desc">
					 Total number of orders
				</div>
			</div>
			<a class="more" href="javascript:;" id="order_total_count_filter">
			Show all <i class="material-icons">filter_list</i>
			</a>
		</div>
	</div>
  <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
		<div class="dashboard-stat yellow">
			<div class="visual">
				<i class="fa fa-cube"></i>
			</div>
			<div class="details">
				<div class="number" id="order_total_open">
					 <i class='fa fa-spin fa-circle-o-notch'></i>
				</div>
				<div class="desc">
					 Not Started
				</div>
			</div>
			<a class="more" href="javascript:;" id="order_total_open_filter">
			Filter list <i class="material-icons">filter_list</i>
			</a>
		</div>
	</div>
	<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
		<div class="dashboard-stat red">
			<div class="visual">
				<i class="fa  fa-edit"></i>
			</div>
			<div class="details">
				<div class="number" id="order_total_updated">
					 <i class='fa fa-spin fa-circle-o-notch'></i>
				</div>
				<div class="desc">
					 Updated
				</div>
			</div>
			<a class="more" href="javascript:;" id="order_total_updated_filter">
			Filter list <i class="material-icons">filter_list</i>
			</a>
		</div>
	</div>
</div>

<div class="row">
  <div class="col-md-12 ">
					<!-- BEGIN Portlet PORTLET-->
		<div class="portlet light">
			<div class="portlet-title tabbable-line">
				<div class="caption">
					<span class="caption-subject bold font-red">
          <i class="material-icons">shopping_cart</i>
					HOTs orders list </span>
					<span class="caption-helper"></span>
				</div>
				<ul class="nav nav-tabs">
					<li class="active">
						<a href="#todays_orders" data-toggle="tab" id="tab_todays_orders">
						List of today's orders </a>
					</li>
					<li>
						<a href="#archived_orders"  data-toggle="tab" id="tab_archive_orders">
						List of previous day orders </a>
					</li>
				</ul>
			</div>
			<div class="portlet-body tab-content">
        <div class="dataTable-container tab-pane active" id="todays_orders">
          <div class="table-toolbar">
          	<div class="row">
          		<div class="col-md-12" id="multiple_item_function_group" style="display:none">
                <div class="form-inline">
                  <div class="input-group input-medium" >
                    <select class="form-control" id="multiple_assignment_select" style="width:300px;"></select>
          					<span class="input-group-btn">
                      <button id="multiple_item_assignment" class="btn green">
              				      <i class="material-icons">assignment_ind</i>Assign items
              				</button>
          					</span>
          				</div>
                  <button id="send_to_other_department_btn" class="btn blue" style="margin-left:10px;">
                        <i class="material-icons">send</i>Send details to other department
                  </button>
                </div>

          		</div>

          	</div>
          </div>
          <div id="table_container" class="table-container">

          </div>

        </div>

        <div class="dataTable-container tab-pane" id="archived_orders">
          <div class="table-toolbar">
          	<div class="row">
          		<div class="col-md-6">
                <div class="input-group input-medium" id="open_order_toolbar" style="display:none">
                  <input type="text" class="form-control" id="archive_date_toolbar" style="width:300px;cursor:pointer;" data-date-end-date="-1d" placeholder="Click here to choose a date"  data-date-format="yyyy-mm-dd" readonly />
                  <span class="input-group-btn">
                    <button id="archive_date_btn_toolbar" class="btn green" disabled>
                         <i class="material-icons">file_download</i>View Archive
                    </button>
                  </span>
                </div>
          		</div>
              <div class="col-md-6">

          		</div>
          	</div>
          </div>
          <div id="table_archive_container" class="table-container">

          </div>

        </div>
			</div>
		</div>
	</div>
</div>


<div id="open_order_download_data_modal" class="modal" tabindex="-1" aria-hidden="false" data-backdrop="static">
    <div class="modal-dialog" id="open_order_download_data_modal_dialog">
      <div class="modal-content" style="width:400px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
          <h4 class="modal-title" id="open_order_download_data_modal_title">Generate Excel File</h4>
        </div>
        <div class="modal-body" id="open_order_download_data_modal_body" >
          <label>Select date range of the data you want to download</label>
          <div class="input-group input-large date-picker input-daterange"  style="width:100% !important;">
              <input type="text" class="form-control" name="from" id="download_date_start_date" data-date-format="yyyy-mm-dd">
              <span class="input-group-addon">
              to </span>
              <input type="text" class="form-control" name="to" id="download_date_end_date" data-date-format="yyyy-mm-dd">
          </div>
          <div class="modal-footer" id="open_order_download_data_modal_footer" >
              <button class="btn green-jungle btn-block" id="generate_excel_btn"> <i class="material-icons" >grid_on</i> Generate File</button>
          </div>
      </div>
    </div>
  </div>
</div>

<div id="send_details_modal" class="modal" tabindex="-1" aria-hidden="false" data-backdrop="static">
    <div class="modal-dialog" id="send_details_modal_dialog">
      <div class="modal-content" style="">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
          <h4 class="modal-title" id="send_details_modal_title">Send details to other department</h4>
        </div>
        <div class="modal-body" id="send_details_modal_body" >
          <div class="form-group" id="email_group">
  						<label>E-Mail From </label>
              
  				</div>
          <div class="form-group ">
              <label>E-Mail to</label>
              <input type="text" class="form-control email-fields req-field req-field-empty"  id = "email_to">
          </div>

          <div class="form-group">
  						<label>E-Mail CC</label>
              <input type="text" class="form-control email-fields"  id = "email_cc">
  				</div>
          <div class="form-group">
  						<label>E-Mail subject</label>
              <input type="text" class="form-control email-fields req-field"  id = "email_subject">
  				</div>


          <div class="form-group">
  						<label>E-Mail body</label>
              <div style="border:1px solid black;min-height:100px;width:100%;padding:10px" contenteditable id="email_body">

    					</div>
  				</div>
          <div class="modal-footer" id="send_details_modal_footer" >
              <button class="btn blue btn-block" id="email_details_btn"> <i class="material-icons" >send</i> E-Mail details to other department</button>
          </div>
      </div>
    </div>
  </div>
</div>
