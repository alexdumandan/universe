<table class="table" id="hot_orders_archive_table">
  <thead>
    <th>Item ID</th>
    <th style="width:70px">Account Number</th>
    <th style="width:100px">SO Number</th>
    <th style="width:50px">Placed By</th>
    <th>Team Lead</th>
    <th>Void Date</th>
    <th>Customer PO</th>
    <th>Amount</th>
    <th style="width:50px">Ship From</th>
    <th >Ship Via</th>
    <th>Order Status</th>
    <th>Assigned To</th>
  </thead>
  <tbody id="hot_orders_archive_table_body">

  </tbody>
</table>
