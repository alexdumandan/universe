<table class="table" id="hot_orders_table">

  <thead>
    <th class="table-checkbox"><input id="group_check" type="checkbox" class="group-checkable" data-set="#hot_orders_table .checkboxes" onchange="selectItem('multi')"/></th>
    <th>Item ID</th>
    <th>Account Number</th>
    <th>SO Number</th>
    <th>Placed By</th>
    <th>Team IS</th>
    <th>Void Date</th>
    <th>Customer PO</th>
    <th>Amount</th>
    <th>Ship From</th>
    <th >Ship Via</th>
    <th>Order Status</th>
    <th>Assigned To</th>
  </thead>
  <thead id="hot_orders_table_col_filter">
     <tr>
       <th></th>
       <th>Item ID</th>
       <th>Account Number</th>
       <th>SO Number</th>
       <th>Placed By</th>
       <th>Team IS</th>
       <th>Void Date</th>
       <th>Customer PO</th>
       <th>Amount</th>
       <th>Ship From</th>
       <th >Ship Via</th>
       <th>Order Status</th>
       <th>Assigned To</th>
     </tr>
 </thead>
  <tbody id="hot_orders_table_body">

  </tbody>
</table>
