<?php

require "../../_Utils/php/PHPExcel-1.8/classes/PHPExcel.php";
include '../_libs/db_connect.php';
include '../functions.php';
require "../../_utils/userDetect.php";

date_default_timezone_set('US/Eastern');

echo str_pad("{\"message\":\"Saving orders to database..\"}",1024," ");

$user_id = $MQPA_NTLogin;

$salesGroup = $_POST["group"];

$path = $_POST['filePath'];
$excelReader = PHPExcel_IOFactory::createReaderForFile($path);
$excelObject = $excelReader->load($path);
$workSheet = $excelObject->getSheetByName("OrderList");
$workSheet = $excelObject->getActiveSheet();
$lastRow = $workSheet->getHighestRow();
$row = 2;

$priorityCarriers = array("F1", "F2", "FO", "FS","US","UR","UO","UB","FW");

$userCountry = "US";
//
// echo substr($group,0,3);
if (substr($salesGroup,0,3) == "CA-"){

  $userCountry = "CA";

};


while( $row < $lastRow+1){

  if ($workSheet->getCell("B".$row) != ""){

    $id = null;
    $account_number = $workSheet->getCell("A".$row);
    $SO_number = $workSheet->getCell("B".$row);
    $team_IS = $workSheet->getCell("C".$row);
    $team_OS = $workSheet->getCell("D".$row);
    $sales_mgr = $workSheet->getCell("E".$row);
    $customer_PO = $workSheet->getCell("F".$row);
    $ingram_PO = $workSheet->getCell("G".$row);
    $placed_by = $workSheet->getCell("H".$row);
    $order_date = date("Y-m-d ", PHPExcel_Shared_Date::ExcelToPHP($workSheet->getCell("I".$row)->getValue()));
    $amount = $workSheet->getCell("J".$row);
    $ship_from = $workSheet->getCell("K".$row);
    $ship_via = $workSheet->getCell("L".$row);
    $order_type = $workSheet->getCell("M".$row);
    $payment_term = $workSheet->getCell("N".$row);
    $order_margin = $workSheet->getCell("O".$row);
    $overall_margin = $workSheet->getCell("P".$row);
    $ship_to = $workSheet->getCell("Q".$row);
    $cycl_info = $workSheet->getCell("R".$row);
    $sku_info = $workSheet->getCell("S".$row);
    $payment_info = $workSheet->getCell("T".$row);
    $credit_info = $workSheet->getCell("U".$row);
    $sp_notes = $workSheet->getCell("V".$row);
    $review_status = $workSheet->getCell("X".$row);
    //get void date
    if(isJSON($sku_info)){

      $skuInfoArr =  json_decode($sku_info);
      if(count($skuInfoArr)>0){
        $tempDate = "";// strtotime(date("Y-m-d"));
        //$tempDate = "0000-00-00 00:00:00";
        $i = 0;
        foreach($skuInfoArr as $sku){
          if ($sku->voidDate != ""){
              $i++;
              $time = strtotime($sku->voidDate);
              if ($tempDate == ""){
                $tempDate = $time;
              }else if ($tempDate > $time){
                $tempDate = $time;
              }
          }
        }

        if($i == 0 ){
          $void_date = "0000-00-00 00:00:00";
        }else{
          $void_date = date("Y-m-d",$tempDate);
        }

      }else{
        $void_date = "0000-00-00 00:00:00";
      }
    }else{
      $void_date = "0000-00-00 00:00:00";
    }

    $orderPlacer = get_userDetailsByIS($placed_by,$salesGroup);
    $lastTouch = get_orderDetailsFromPreviousItem($SO_number,$account_number,$userCountry,"order_assign_to");

    if(count($orderPlacer) > 0){

      $assign_to = $orderPlacer["ntid"];
      $assign_by = "SYSTEM";
      $assign_date = date("Y-m-d H:i:s");
      $assign_history = "{\"dateTime\":\"".$assign_date."\",\"assignedBy\":\"".$assign_by."\",\"assignedByName\":\"SYSTEM\",\"assignedTo\": \"".$assign_to."\"},";

    }else if(count($lastTouch) > 0){

      if ($lastTouch['order_assign_to'] != ""){

        $assign_to = $lastTouch['order_assign_to'];
        $assign_by = "SYSTEM";
        $assign_date = date("Y-m-d H:i:s");
        $assign_history = "{\"dateTime\":\"".$assign_date."\",\"assignedBy\":\"".$assign_by."\",\"assignedByName\":\"SYSTEM\",\"assignedTo\": \"".$assign_to."\"},";

      }else{

        $assign_to = "";
        $assign_by = "";
        $assign_date = "0000-00-00 00:00:00";
        $assign_history = "";

      }

    } else{

      $assign_to = "";
      $assign_by = "";
      $assign_date = "0000-00-00 00:00:00";
      $assign_history = "";

    }



    $uploaded_date = date("Y-m-d H:i:s");
    $hold_reason = "";
    $item_status = "Not Started";
    $ext_void_date = "0000-00-00 00:00:00";
    $contact_method = "";
    $reseller_contact = "";
    $reseller_phone = "";
    $print_time = "0000-00-00 00:00:00";
    $notes = "";
    $source = "HOT";
    $sales_group = $salesGroup;
    $country = $userCountry;
    $priority = 1;


    if (in_array(strtoupper(substr($ship_via,0,2)),$priorityCarriers)){

      $priority = $priority + 2;

    }

    if ($ship_from == "80"){

      $priority = $priority + 2;

    }

    if (strpos($customer_PO,"TEST")>-1 || strpos($customer_PO,"HOLD")>-1|| trim($customer_PO) == "" ){

      $priority = $priority - 2;

    }

    if ($void_date == date("Y-m-d")){

      $priority = $priority + 2;

    }



    $insert = mysqli_prepare($con,
            "INSERT INTO tb_orders  "
            . "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);");

    if ($insert === false) {
      echo  mysqli_error($con);
      mysqli_close($con);
      die ("::ERROR::INSERT_PREPARE::NEWRECORD");
    };

    $bind = mysqli_stmt_bind_param($insert, "ssssssssssssssssssssssssssssssssssssssssss",
                                    $id ,
                                    $account_number ,
                                    $SO_number ,
                                    $team_IS ,
                                    $team_OS ,
                                    $sales_mgr ,
                                    $customer_PO ,
                                    $ingram_PO ,
                                    $placed_by ,
                                    $order_date ,

                                    $amount ,
                                    $ship_from ,
                                    $ship_via ,
                                    $order_type ,
                                    $payment_term ,
                                    $order_margin ,
                                    $overall_margin ,
                                    $ship_to ,
                                    $cycl_info ,
                                    $sku_info ,

                                    $payment_info ,
                                    $credit_info ,
                                    $sp_notes ,
                                    $void_date ,
                                    $assign_to ,
                                    $assign_by ,
                                    $assign_date ,
                                    $assign_history ,
                                    $uploaded_date ,
                                    $hold_reason ,

                                    $item_status ,
                                    $ext_void_date ,
                                    $contact_method ,
                                    $reseller_contact ,
                                    $reseller_phone ,
                                    $print_time ,
                                    $notes ,
                                    $source ,
                                    $sales_group ,
                                    $country,
                                    $priority,
                                    $review_status

        );

    if ($bind === false) {

      echo  mysqli_error($con);
      mysqli_close($con);
      die ("::ERROR::INSERT_BIND::NEWRECORD");

    }

    $exec = mysqli_stmt_execute($insert);

    if ($exec === false) {

      echo  mysqli_error($con);
      mysqli_close($con);
      die ("::ERROR::INSERT_EXEC::NEWRECORD");

    }


    echo str_pad(",{\"total\":\"".($lastRow-1)."\",\"current\":\"".($row-1)."\",\"message\":\"Saving to database  (".($row-1)." of ".($lastRow-1).")\"}",1024*4," ");

  }


  $row++;

}






 ?>
