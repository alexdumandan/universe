var HOTorders;
var HOTordersDataTable;

var HOTordersArchive;
var HOTordersArchiveDataTable;

var USusers;

var activeTab = "HOTorders";

var group;
var signature;
var selectedItemIDs;

var versionDate = new Date();
var versionNumber = versionDate.getTime();

$(document).ready(function(){

    // alert("You will be directed to the test site.");
    // location.href = "/sales_hub_test/";

    biFrost.init({
      "maxFilesizeMB" : 25,
      "success" : uploadHOTordersFile,
      "error": errorUpload
    });

    group = getParameterByName("group");

    $("#page_content").load('hots/html_hots.php',function(){

      $("#table_container,#table_archive_container").css("height",($(window).height()-450) + "px");

      $("#table_container").load('hots/html_loading.php',function(){

        $.ajax({
            type: 'GET',
            url: "hots/json_getUsersList.php",
            data: {group:group},
            success: function(data){

              USusers = $.parseJSON(data);
              setUserForDropdown(USusers,"#multiple_assignment_select");
              getHOTorders();

            }
        });

      });

      $("#order_total_open_filter").click(function(){

          if(activeTab == "HOTorders"){
            drawHOTordersDataTable(filterList(HOTorders,"itemStatus","Not Started"));
          }else{
            drawArchiveHOTordersDataTable(filterList(HOTordersArchive,"itemStatus","Not Started"));
          }

      });

      $("#order_total_updated_filter").click(function(){

          if(activeTab == "HOTorders"){
            drawHOTordersDataTable(filterList(HOTorders,"itemStatus","Not Started","!="));
          }else{
            drawArchiveHOTordersDataTable(filterList(HOTordersArchive,"itemStatus","Not Started","!="));
          }

      });

      $("#multiple_item_assignment").click(function(){

        multipleAssignment();

      });

      $("#send_to_other_department_btn").click(function(){

        $("#send_details_modal").modal("show");
        getDetailsOfSelectedItemsToSend();



      });

      getGroupTeamEmails();
      getSignature();



        $(".email-fields").formFields();

      $('#email_body').summernote({

        height: 300,
        minHeight: 200,
        maxHeight: 500,
        toolbar: [],
        popover: []

      });


      $("#archive_date_toolbar").change(function(){

        $("#archive_date_btn_toolbar").attr("disabled",false);

      });

      $("#archive_date_btn_toolbar").click(function(){

        $("#table_archive_container").load("hots/html_no_open_orders_archive.php",function(){

          $("#open_order_toolbar").hide();
          $("#hots_archive_date_container").hide();
          $("#hots_archive_date").datepicker();
          $("#hots_archive_date").change(function(){

            $("#archive_date_hots_btn").attr("disabled",false);

          });

          $("#archive_date_hots_btn").click(function(){

            getArchiveHOTorders($("#hots_archive_date").val());

          });

          getArchiveHOTorders($("#archive_date_toolbar").val());

        });



      });

      $("#email_details_btn").click(function(){

        if(checkInvalidEmailFieldsCount()<1){
          updateItemStatusAndSendEmailofSelectedItems();
        }

      });

      $("#order_total_count_filter").click(function(){

          if(activeTab == "HOTorders"){
            drawHOTordersDataTable(HOTorders);
          }else{
            drawArchiveHOTordersDataTable(HOTordersArchive);
          }

      });

      $("#tab_todays_orders").click(function(){
        activeTab = "HOTorders";
        updateStatsSummary(HOTorders);
      });

      $("#tab_archive_orders").click(function(){

        activeTab = "HOTordersArchive";

        if (HOTordersArchive == null){

            $("#table_archive_container").load("hots/html_loading_archive.php",function(){

              $("#hots_archive_date").datepicker();
              $("#hots_archive_date").change(function(){

                $("#archive_date_hots_btn").attr("disabled",false);

              });

              $("#archive_date_hots_btn").click(function(){

                getArchiveHOTorders($("#hots_archive_date").val());

              });

            });

        }

        updateStatsSummary(HOTordersArchive);

      });

      $("#generate_excel_btn").click(function(){

        generateExcelFile();

      });

      $("#download_date_start_date,#download_date_end_date").datepicker();
      $("#archive_date_toolbar").datepicker();

    });

});

function updateItemStatusAndSendEmailofSelectedItems(){

  showMultiModal("","<div style=\"font-size:20px\"><i class='fa fa-spin fa-circle-o-notch' style=\"font-size:20px\"></i> UpdatingItems </div>","","250");
  selectedItemIDs = [];

  $.each($("#hot_orders_table_body .checker .checked .checkboxes "),function(i,item){

    selectedItemIDs.push($(this).attr("data-itemid"));

  });



  recordActionOfSelectedItems(selectedItemIDs,"hots","EmailMultipleItems")

  sendEmailtoOtherDepartment();

}

function updateItemStatusSelectedItem(itemID){

  var updateData = new Object();

  updateData.id = itemID;
  updateData.notes = "Order details sent to " + $("#email_to").val();
  updateData.item_status = "Pending";

  $.ajax({

        type: "POST",
        url: "hots_view/update_openOrder.php",
        data: {data:JSON.stringify(updateData)},
        success: function(res){

          if (res.indexOf("NO ACCESS")>-1){
            toastr["warning"]("You don't have the right permission to update this item!", "")

          }else if (res.indexOf("::ERROR::")<0){



            $("#assign_to_col_"+itemID).parent().find(".checker .checked").removeClass("checked");
            var row = $("#assign_to_col_"+itemID).parent("tr");
            var rowData = HOTordersDataTable.row(row).data();
            rowData[11] = updateData.item_status;
            HOTordersDataTable.row(row).data(rowData).draw();

            if($("#hot_orders_table_body .checker .checked .checkboxes ").length == 0 ){
              $(".checker").children("span").removeClass("checked");
              toastr["success"]("Items updated!", "");
              hideMultiModal();
              $("#multiple_item_function_group").fadeOut();
              $("#send_details_modal").modal("hide");
            }


          }else{
            toastr["error"]("Error in updating the item!", "");
            // ESEO.customLog("CA OPENORDERS UPDATE ERROR",res);
          }

        },
        error:function(res) {
          // ESEO.customLog("CA OPENORDERS UPDATE ERROR OUT",res);
          toastr["error"]("Error in updating the item!", "");
        },
        complete:function(){

        }
      })


}

function getGroupTeamEmails(){

  $.ajax({
    type: "GET",
    url: "hots_view/json_getGroupTeamEmails.php",
    data: {group:group},
    success: function(data){

        var select = "<select class=\"form-control email-fields req-field req-field-empty\" id=\"email_from\">";
        select += "<option value=\"\" disabled selected>Select team mailbox</option>";

        $.each(JSON.parse(data),function(i,email){

          select += "<option value=\""+email.email+"\">"+email.email+"</option>";

        });


          select += "</select>";

          $("#email_group").append(select);

          getTeamEmail();

    }

  });

}

function sendEmailtoOtherDepartment(){

  emailSender.sendEmail({
  		"clientScriptLoc":"email_API/client.php",
  		"email_subject":$("#email_subject").val(),
  		"email_from":$("#email_from").val(),
  		"email_to":$("#email_to").val(),
  		"email_cc":$("#email_from").val() + ";" + $("#email_cc").val(),
      // "email_cc":"mark.eseo@ingrammicro.com",
  		"email_body":$("#email_body").parent().find(".note-editable").html(),
  		"email_format":1,
      "success": doEmailSuccessActivities,
      "error":doEmailErrorActivities
  	});

}

function doEmailSuccessActivities(data){
  toastr["success"]("E-Mail Processed!", "");

  saveEmailToDatabase(data.split("_")[1]);


}

function doEmailErrorActivities(){
  toastr["error"]("Error in processing email!", "");
  hideMultiModal();
}

function saveEmailToDatabase(emailID){

  $.each(selectedItemIDs,function(i,itemID){

    $.ajax({
        type: 'POST',
        url: "hots_view/insert_saveEmail.php",
        data: {
            emailID:emailID,
            orderID:itemID,
            subject:$("#email_subject").val(),
            from:$("#email_from").val(),
            to:$("#email_to").val(),
            cc:$("#email_from").val() + ";" + $("#email_cc").val(),
            body:$("#email_body").parent().find(".note-editable").html()
          },
        success: function(data){

          updateItemStatusSelectedItem(itemID);


        }

      });

  });


}


function getDetailsOfSelectedItemsToSend(){

  var emailBodyStyle = "<style>";
    emailBodyStyle += "#email_table_body td,#email_table_body th { border:1px solid black;text-align:left;}";
  emailBodyStyle += "</style>";

  var emailMessage = "<p>Hi,</p>";
  emailMessage += "<p>Please take care of the orders below</p>";

  var detailsTable = "<table id=\"email_table_body\" style=\"width:500px\">";
  detailsTable += "<tr><th>Account Number</th><th>SO Number</th><th>PO Number</th></tr>";

  var emailSubject = "SO Numbers : ";

  var selectedItemCount = 0;
  var itemIDsSelectedArray = [];
  $.each($("#hot_orders_table_body .checker .checked .checkboxes "),function(i,item){


    var orderDetails = getHotsOrderDetails($(this).attr("data-itemid"));

    detailsTable += "<tr><td>" + orderDetails.accountNumber + "</td><td>" +orderDetails.SOnumber+ "</td><td>" + orderDetails.customerPO +"</td></tr>";
    emailSubject +=  orderDetails.SOnumber + " / ";
    selectedItemCount++;
    itemIDsSelectedArray.push($(this).attr("data-itemid"));

  });



  recordActionOfSelectedItems(itemIDsSelectedArray,"hots","selectMultipleItems")



  detailsTable += "</table><br>";

  $("#email_subject").val(emailSubject);

  $("#email_body").parent().find(".note-editable").html(emailBodyStyle +emailMessage + detailsTable + signature);

}

function getTeamEmail(){

  $.ajax({
    type: "GET",
    url: "hots_view/json_getTeamEmail.php",
    data: {},
    success: function(data){
      var teamEmail = data;

      if (teamEmail != null || teamEmail !=undefined){
        $("#email_from").val(teamEmail).removeClass("req-field-empty");
      }
    }

  });
}

function checkInvalidEmailFieldsCount(){

  $("#send_details_modal_body .req-field-empty").trigger("change");
  $("#send_details_modal_body .req-field-empty").addClass("req-field-invalid");

  var invalid = $("#send_details_modal_body .req-field-invalid");

  if( invalid.length > 0){

    $.each(invalid,function(){

      scrollToElement($(this));
      return false;

    });

  }
    return invalid.length;
}

function getSignature(){

  $.ajax({
    type: "GET",
    url: "signature/json_getSignature.php",
    data: {},
    success: function(data){
      signature = data;
      if (signature == undefined || signature == null || signature == "") {
        signature +=  "Thank you, <br>";
        signature +=  G_USERPROFILE.firstname + " " + G_USERPROFILE.lastname;
      }
    }

  });

}

function getHotsOrderDetails(itemID){

  var HotOrder = new Object();
  $.each(HOTorders,function(i,order){

      if(order.id == itemID){

          HotOrder = order;
          return false;
      }


  });

  return HotOrder;

}

function getNameByISnumber(ISnumber){

  var name = ISnumber;

  $.each(USusers,function(i,user){

    if(ISnumber == user.ISnumber ){

      name = user.firstname + " " + user.lastname;

    }

  });

  return name;

}

function generateExcelFile(){

  location.href = "hots/xls_generateExcelFile.php?dateStart="+$("#download_date_start_date").val()+"&dateEnd="+$("#download_date_end_date").val()+"&groupName="+getParameterByName("group");

}

function uploadHOTordersFile(files){

  if (files != null){

  }else{
    toastr["warning"]("Select a file first.", "")
    return 0;
  }

  $("#hots_file_container").hide();
  $("#loader_progress_bar_container").show();
  $("#loader_text").text("Saving HOTs orders to the server..");

  $.ajax({
      type: 'POST',
      xhrFields: {
          onprogress: function(e) {
              updateProgressBar(e.target.responseText,"#loader_progress_bar","#loader_text");
              console.log(e.target.responseText);

          }
      },
      url: "hots/insert_uploadHotsOrder.php",
      data: {filePath:files.fileNames[0],group:group},
      success: function(data){


        if (data.indexOf("::ERROR::")<0){

          $("#hots_file").val("");
          getHOTorders();

        }else{


        }


      },
      error:function(){


      }
  });

}

function updateProgressBar(updatesText,progressBarSelector,textSelector){

    if(updatesText != undefined && updatesText != "" ){
      updateJSON = "["+updatesText+"]";

      try {
        JSON.parse(updateJSON);
      } catch (e) {
          return false;
      }

      updateObj = $.parseJSON(updateJSON);
      updateObj.reverse();

      if(updateObj[0].message != null || updateObj[0].message != undefined){
        $(textSelector).text(updateObj[0].message);
      }
      if (updateObj[0].current != null || updateObj[0].current != undefined){
        $(progressBarSelector).css("width",((updateObj[0].current/updateObj[0].total)*100) + "%");
      }

    }

}

function errorUpload(){
  console.log("error in uploading file");
}

function getArchiveHOTorders(date){

  $("#loader_archive_date_container").hide();
  $("#loader_archive_progress_bar_container").show();
  $("#loader_archive_text").text("Checking archives...");

  $.ajax({
      type: 'GET',
      xhrFields: {
          onprogress: function(e) {

              updateProgressBar(e.target.responseText,"#loader_archive_progress_bar","#loader_archive_text");
          }
      },
      url: "hots/json_getArchiveHOTorders.php",
      data: {date:date,group:group},
      success: function(data){

        data = data.substring(data.indexOf("["),data.length);

        HOTordersArchive = $.parseJSON(data);

        if (HOTordersArchive.length == 0 || HOTordersArchive.length == null || HOTordersArchive.length == undefined){

              $("#table_archive_container").css("height",($(window).height()-450) + "px");

              $("#table_archive_container").load("hots/html_loading_archive.php",function(){

                $("#hots_toolbar").hide();
                $("#hots_archive_date").datepicker();
                $("#hots_archive_date").change(function(){

                  $("#archive_date_hots_btn").attr("disabled",false);

                });

                $("#loader_archive_text").text("We can't find any orders on that date. Choose another date");

                $("#archive_date_hots_btn").click(function(){

                  getArchiveHOTorders($("#hots_archive_date").val());

                });

              });

        }else{

          $("#loader_archive_text").text("Preparing datatable..");
          drawArchiveHOTordersDataTable(HOTordersArchive);

        }

        updateStatsSummary(HOTordersArchive);

      }
  });


}

function drawArchiveHOTordersDataTable(openOrderList){

  if (HOTordersArchiveDataTable != null || HOTordersArchiveDataTable != undefined){
    HOTordersArchiveDataTable.destroy();
  }


  $("#table_archive_container").load("hots/html_hot_orders_archive_table.php",function(){

        var orders = "";


        $.each(openOrderList,function(i,order){

          orders += "<tr>";
          orders += "<td><a href=\"?page=hots_view&itemID="+order.id+"\" >"+order.id+"</a></td>";
          orders += "<td>"+order.accountNumber+"</td>";
          orders += "<td>"+order.SOnumber+"</td>";
          orders += "<td>"+getNameByISnumber(order.placedBy)+"</td>";

          if(group == "SMB"){
            orders += "<td>"+order.salesMgr+"</td>";
          }else{
            orders += "<td>"+order.teamIS+"</td>";
          }

          orders += "<td>"+order.voidDate+"</td>";
          orders += "<td>"+order.customerPO+"</td>";
          orders += "<td>"+order.amount.toLocaleString()+"</td>";
          orders += "<td>"+order.shipFrom+"</td>";
          orders += "<td>"+order.shipVia+"</td>";
          orders += "<td>"+order.itemStatus+"</td>";
          if (order.assignTo == "" || order.assignTo == null){
                orders += "<td>Not Assigned</td>"
          }else{
                orders += "<td>"+getFullName(order.assignTo)+"</td>"
          }

          orders += "</tr>";


        });


        $("#hot_orders_archive_table_body").html(orders);

        initHOTordersArchiveDataTable("#hot_orders_archive_table");

        $("#table_archive_container").css("height","100%");
        $("#open_order_toolbar").fadeIn();


  });



}

function getHOTorders(){

  $("#loader_icon").hide();
  $("#loader_progress_bar_container").show();

  $.ajax({
      type: 'GET',
      xhrFields: {
          onprogress: function(e) {
            updateProgressBar(e.target.responseText,"#loader_progress_bar","#loader_text");
          }
      },
      url: "hots/json_getHotsOrders.php",
      data: {group:group,version:versionNumber},
      success: function(data){

        data = data.substring(data.indexOf("["),data.length);

        HOTorders = $.parseJSON(data);

        if (HOTorders.length == 0 || HOTorders.length == null || HOTorders.length == undefined){

          $("#table_container").load("hots/html_loading.php",function(){

            $("#loader_icon").hide();

            $("#hots_file_container").show();
            $("#loader_text").text("No HOTs found for today. Please upload HOTs file. (Filename starts with: SALES HUB HOTS)");

            $("#upload_hots_btn").click(function(){
              biFrost.upload();
              $("#loader_text").text("Saving file to the server..");
            });


            $("#hots_file").change(function(){

                var filename = $(this).val();
                if(filename != ""){

                  if(filename.indexOf("SALES HUB HOTS ") > -1 ){
                    $("#upload_hots_btn").prop("disabled",false);


                  }else{
                    $(this).val("");
                    toastr["warning"]("Invalid file selected", "");
                    $("#loader_text").text("Invalid File. Select the file that starts with \"SALES HUB HOTS \"");
                  }


                }else{
                  $("#upload_hots_btn").prop("disabled",true);
                }

            });

          });
        }else{

          $("#loader_text").text("Preparing datatable..");
          drawHOTordersDataTable(HOTorders);

        }

        updateStatsSummary(HOTorders);



      }
  });


}

function drawHOTordersDataTable(hotOrderList){

  if (HOTordersDataTable != null || HOTordersDataTable != undefined){
    HOTordersDataTable.destroy();
  }


  $("#table_container").load("hots/html_hot_orders_table.php",function(){

        var orders = "";
        var users = "<option value=\"0\"></option>";


        $.each(USusers,function(i,user){
          users += "<option value=\""+user.ntid+"\" >"+ user.firstname + " " + user.lastname + "</option>";
        });

        $.each(hotOrderList,function(i,order){

          orders += "<tr>";
          orders += "<td><input type='checkbox' class='checkboxes' value='1' data-itemID = \""+order.id+"\"/ onchange=\"selectItem('single')\"></td>";
          orders += "<td><a href=\"?page=hots_view&itemID="+order.id+"\" >"+order.id+"</a></td>";
          orders += "<td>"+order.accountNumber+"</td>";
          orders += "<td>"+order.SOnumber+"</td>";
          orders += "<td>"+getNameByISnumber(order.placedBy)+"</td>";
          if(group == "SMB"){
            orders += "<td>"+order.salesMgr+"</td>";
          }else{
            orders += "<td>"+order.teamIS+"</td>";
          }

          orders += "<td>"+order.voidDate+"</td>";
          orders += "<td>"+order.customerPO+"</td>";
          orders += "<td>$ "+Number(order.amount).toLocaleString()+"</td>";
          orders += "<td>"+order.shipFrom+"</td>";
          orders += "<td>"+order.shipVia+"</td>";
          orders += "<td>"+order.itemStatus+"</td>";
          if (order.assignTo == "" || order.assignTo == null){
                orders += "<td class=\"assign-to-col\" id=\"assign_to_col_"+order.id+"\"data-itemID=\""+order.id+"\" data-value = \""+order.assignTo+"\" >Not Assigned</td>"
          }else{
                orders += "<td class=\"assign-to-col\" id=\"assign_to_col_"+order.id+"\"data-itemID=\""+order.id+"\" data-value = \""+order.assignTo+"\" >"+getFullName(order.assignTo)+"</td>"
          }

          orders += "</tr>";


        });



        $("#hot_orders_table_body").html(orders);


        if(G_USERPROFILE.accountType != "Associate"){
          $(".assign-to-col").mouseenter(function(){

              $.each($(".assigned-rep"),function(i,item){
                var col = $(item).parent(".assign-to-col")
                if ($(col).attr("data-value") != ""){
                  $(col).text(getFullName($(col).attr("data-value")));
                }else{
                  $(col).text("Not Assigned");
                }

                $(item).select2('destroy').remove();

              });

              var el = $(this);
              el.html("<select class=\"assigned-rep\" id=\"assigned_rep_"+el.attr("data-itemID")+"\"  data-itemID=\""+el.attr("data-itemID")+"\" data-value = \""+el.attr("data-value")+"\" onchange= \"reassignItem(this)\">"+users+"</select>");
              $("#assigned_rep_"+el.attr("data-itemID")).val(el.attr("data-value"));
              $("#assigned_rep_"+el.attr("data-itemID")).select2();

          });
        }else{

          $(".checkboxes,.group-checkable").attr("disabled",true);

        }



        $.each($(".assigned-rep.not-select2"),function(i,item){

            $(this).val($(this).attr("data-value"))
        });

        initHOTordersDataTable("#hot_orders_table");

        $("#hot_orders_table").on( 'draw.dt', function () {
          $(".checkboxes").uniform();

        });



        $("#table_container").css("height","100%");

  });



}

function multipleAssignment(){

    var ntid = $("#multiple_assignment_select").val();

    showMultiModal("","<div style=\"font-size:20px\"><i class='fa fa-spin fa-circle-o-notch' style=\"font-size:20px\"></i> Reassigning Items..</div>","","250");

    $.each($("#hot_orders_table_body .checker .checked .checkboxes "),function(i,item){

        reassignMultipleItem(ntid,$(this).attr("data-itemid"));
        //setUserForDropdown(USusers,"#assigned_rep_"+$(this).attr("data-itemid"),ntid);

    });

}

function selectItem(type){

  if (type == "multi"){

    if($(".table-checkbox .checker .checked" ).length > 0 ){

        $("#multiple_item_function_group").fadeIn();

    }else{
        $("#multiple_item_function_group").fadeOut();
    }


  }else{

    var numberOfCheckedItems = $("#hot_orders_table_body .checker .checked").length;

    if(numberOfCheckedItems > 0){
      $("#multiple_item_function_group").fadeIn();
    }else{
      $("#multiple_item_function_group").fadeOut();
    }
  }

}

function initHOTordersArchiveDataTable(tableID){

      var table = $(tableID);
      HOTordersArchiveDataTable = table.DataTable({

          "language": {
              "aria": {
                  "sortAscending": ": activate to sort column ascending",
                  "sortDescending": ": activate to sort column descending"
              },
              "emptyTable": "<h1>No matching records found</h1>",
              "info": "Showing _START_ to _END_ of _TOTAL_ entries",
              "infoEmpty": "No entries found",
              "infoFiltered": "(filtered1 from _MAX_ total entries)",
              "lengthMenu": "Show _MENU_ entries",
              "search": "Search:",
              "zeroRecords": "<h1>No matching records found</h1>"
          },

          "order": [
              [0, 'desc']
          ],

          "lengthMenu": [
              [15, 30, 50, 100,-1],
              [15, 30, 50, 100,"all"] // change per page values here
          ],
          "pageLength": 15,

      });


      var wrapper = tableID+"_wrapper";
      var tableWrapper = $(wrapper);

      tableWrapper.find('.dataTables_length select').select();

}

function initHOTordersDataTable(tableID){

      $('#group_check').change(function () {

          var set = jQuery(this).attr("data-set");
          var checked = jQuery(this).is(":checked");
          jQuery(set).each(function () {
              if (checked) {
                  $(this).attr("checked", true);

              } else {
                  $(this).attr("checked", false);
              }

          });

          jQuery.uniform.update(set);

      });

      $(tableID + '_col_filter th').each( function (i) {

        if(i > 0){

         var title = $(this).text();

           $(this).html( '<input class="datatable_col_filter" data-colIndex = '+i+' style="width:100%" type="text" />' );

         }


       } );



      var table = $(tableID);


      HOTordersDataTable = table.DataTable({

          "language": {
              "aria": {
                  "sortAscending": ": activate to sort column ascending",
                  "sortDescending": ": activate to sort column descending"
              },
              "emptyTable": "<h1>No matching records found</h1>",
              "info": "Showing _START_ to _END_ of _TOTAL_ entries",
              "infoEmpty": "No entries found",
              "infoFiltered": "(filtered1 from _MAX_ total entries)",
              "lengthMenu": "Show _MENU_ entries",
              "search": "Search all fields:",
              "zeroRecords": "<h3>No matching records found</h3>"
          },

          "order": [
              [0, 'desc']
          ],
          "columns": [{
                       "orderable": false
                   }, {
                       "orderable": true
                   }, {
                       "orderable": true
                   }, {
                       "orderable": true
                   }, {
                       "orderable": true
                   }, {
                       "orderable": true
                   }, {
                       "orderable": true
                   }, {
                       "orderable": true
                   }, {
                       "orderable": true
                   }, {
                       "orderable": true
                   }, {
                       "orderable": true
                   }, {
                       "orderable": true
                   }, {
                       "orderable": true}
                   ],

          "lengthMenu": [
              [15, 30, 50, 100,-1],
              [15, 30, 50, 100,"all"]
          ],
          // "pageLength": -1,

      });


      var wrapper = tableID+"_wrapper";
      var tableWrapper = $(wrapper);

      $(".datatable_col_filter").keyup(function(){

        var thisInput = $(this);
        var columnIndex = $(this).attr("data-colIndex");
        if (columnIndex == ""){
          HOTordersDataTable.search(thisInput.val()).draw();
        }else{

          var keywords = thisInput.val().split(",");


          if(keywords.length > 1){

            var trimKeywords = [];

            $.each(keywords,function(i,kw){

              trimKeywords.push(".*"+kw.trim()+".*");

            })


            var regexString = trimKeywords.join("|");


            HOTordersDataTable.columns(columnIndex)
            .search(regexString,true,true)
            .draw();


          }else{

            HOTordersDataTable.columns(columnIndex)
            .search(thisInput.val())
            .draw();


          }

        }

      });


      tableWrapper.find('.dataTables_length select').select();

      $(".checkboxes,.group-checkable").uniform();
      //drawUserDropDown();
}

function reassignItem(select){


    var ntid = $(select).val();
    var itemID = $(select).attr("data-itemID");
    $.ajax({
      url: "hots/update_reassignItem.php",
      type: "POST",
      data: {userNTID:ntid,itemID:itemID},
      success:function(data) {

        $("#assign_to_col_"+itemID).attr("data-value",ntid);
        var row = $("#assign_to_col_"+itemID).parent("tr");
        var rowData = HOTordersDataTable.row(row).data();
        rowData[12] = getFullName(ntid);
        HOTordersDataTable.row(row).data(rowData).draw();
        $(".checker").children("span").removeClass("checked");
        $("#multiple_item_function_group").fadeOut();
        sendReassignNotification(ntid,itemID);


      },
      error:function(){

        console.log("error:reassign")
        hideMultiModal();
      }
    })

}

function sendReassignNotification(recipientNTID,itemID){

  var contentObj = {

    message:"Item ID : " + itemID + " has been assigned to you.",
    itemID: itemID,
    assignedBy: G_USERPROFILE.ntid,
    type:"Reassignment"

  };

  pepperPottsNotif.sendNotif([recipientNTID],contentObj);

}

function reassignMultipleItem(ntid,itemID){

    $.ajax({
      url: "hots/update_reassignItem.php",
      type: "POST",
      data: {userNTID:ntid,itemID:itemID},
      success:function(data) {

        $("#assign_to_col_"+itemID).attr("data-value",ntid).text(ntid);
        var row = $("#assign_to_col_"+itemID).parent("tr");
        var rowData = HOTordersDataTable.row(row).data();
        rowData[12] = getFullName(ntid);
        HOTordersDataTable.row(row).data(rowData).draw();
        $("#assign_to_col_"+itemID).parents(".checker").children("span").removeClass("checked");
        $("#multiple_item_function_group").fadeOut();

        sendReassignNotification(ntid,itemID);

        if($("#hot_orders_table_body .checker .checked .checkboxes ").length == 0 ){
          $(".checker").children("span").removeClass("checked");
          hideMultiModal();

        }


      },
      error:function(){

        console.log("error:reassign")
        hideMultiModal();
      }
    })

}

function getFirstNameCA(ntid){

    var result = "";

    $.each(USusers,function(i,o){

      if (o.ntid == ntid){
        result = o.firstname;
      }

    });

    return result;

}

function getFullName(ntid){

    var result = "";

    $.each(USusers,function(i,o){

      if (o.ntid == ntid){
        result = o.firstname + " " + o.lastname;
      }

    });

    return result;
}

function updateStatsSummary(list){


  if (list == undefined || list == null || list.length == 0){

    $("#order_total_count").text("-");
    $("#order_total_amount").text("$ - ");
    $("#order_total_open").text("-");
    $("#order_total_updated").text("-");
    return 0;

  }

  var HOTordersSummary = new Object();
  HOTordersSummary.totalOrders = 0;
  HOTordersSummary.totalAmount = 0;
  HOTordersSummary.totalPendingorders = 0;
  HOTordersSummary.totalTouchedOrders = 0;

  $.each(list,function(i,order){

    if(order.itemStatus == "Not Started"){
      HOTordersSummary.totalPendingorders++;
    }else{
      HOTordersSummary.totalTouchedOrders++;
    }

    HOTordersSummary.totalAmount = HOTordersSummary.totalAmount + (order.amount*1);
    HOTordersSummary.totalOrders++;

  });

  $("#order_total_count").text(HOTordersSummary.totalOrders);
  $("#order_total_amount").text("$ "+ HOTordersSummary.totalAmount.toLocaleString());
  $("#order_total_open").text(HOTordersSummary.totalPendingorders);
  $("#order_total_updated").text(HOTordersSummary.totalTouchedOrders);

}

function filterList(list,filterField,filterValue,operator){

  if (operator == undefined){
    operator = "==";
  }
  var newList = [];

  $.each(list, function(i, o) {

    if (o[filterField] == filterValue && operator == "=="){
      newList.push(o);

    }else if(o[filterField] != filterValue && operator == "!="){

      newList.push(o);

    }

  });

  return newList;


}
