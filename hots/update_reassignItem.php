<?php

include '../_libs/db_connect.php';
include '../functions.php';
require "../../_Utils/userDetect.php";
require "../../EULA/API.php";
require "../../ESEO/V1/API.php";

date_default_timezone_set('US/Eastern');

$user_id = $MQPA_NTLogin;

$reassignTo = $_POST["userNTID"];
$itemID = $_POST["itemID"];
$reassignDateTime = date("Y-m-d H:i:s");
$assignToHistory = "order_assign_history = CONCAT(order_assign_history,'{\"dateTime\":\"".$reassignDateTime."\",\"assignedBy\":\"".$user_id."\",\"assignedByName\":\"".get_lastname($user_id). ", " . get_firstname($user_id) ."\",\"assignedTo\": \"',?,'\"}' ,',')";
$update = mysqli_prepare($con,"UPDATE tb_orders SET order_assign_to = ? , order_assign_date = ?, order_assign_by = ?, ".$assignToHistory."  WHERE order_id =  ?;" );

if ($update === false) {

  ESEO_log($con);
  mysqli_close($con);
  die ("::ERROR::UPDATE");

};

$bind = mysqli_stmt_bind_param($update, "sssss",$reassignTo,$reassignDateTime, $user_id, $reassignTo, $itemID);

if ($bind === false) {

  ESEO_log($con);
  mysqli_close($con);
  die ("::ERROR::BIND");

}

$exec = mysqli_stmt_execute($update);

if ($exec === false) {

  ESEO_log($con);
  mysqli_close($con);
   die ("::ERROR::EXEC");

}



 ?>
