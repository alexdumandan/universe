<div style="width:100%;text-align:center;" class="loader-container">
  <div class="input-group input-medium" style="width:100%;margin:auto;margin-bottom:30px" id="loader_archive_date_container">
    <input type="text" class="form-control input-lg" id="hots_archive_date" style="width:300px;cursor:pointer" data-date-end-date="-1d" data-date-format="yyyy-mm-dd" placeholder="Click here to choose a date" readonly />
    <span class="input-group-btn">
      <button id="archive_date_hots_btn" class="btn green btn-lg" disabled>
           <i class="material-icons">file_download</i>View Archive
      </button>
    </span>
  </div>
  <h1 id="loader_archive_text">Choose the date that you want to load.</h1>
  <div class="progress progress-striped active" id="loader_archive_progress_bar_container" style="display:none">
  	<div id="loader_archive_progress_bar" class="progress-bar" role="progressbar">
  	</div>
  </div>
</div>
