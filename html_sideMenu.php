
<li class="nav-item start">
    <a href="?page=new_enquiry" class="nav-link nav-toggle">
        <i class="fa fa-plus-square-o"></i>
        <span class="title">Create New Inquiry</span>
        <span class="selected"></span>

    </a>
</li>


<li class="heading">
    <h3 class="uppercase">Portfolios</h3>
</li>

<li class="nav-item start">
    <a href="?portfolio=paf" class="nav-link nav-toggle">
        <i class="fa fa-inbox"></i>
        <span class="title">PAF</span>
        <span class="selected"></span>
    </a>
</li>


<li class="heading">
    <h3 class="uppercase"></h3>
</li>

<li class="nav-item start">
    <a href="?page=dashboard" class="nav-link nav-toggle">
        <i class="material-icons">dashboard</i>
        <span class="title">Dashboard</span>
        <span class="selected"></span>

    </a>
</li>
<li class="nav-item start">
    <a href="?page=download" class="nav-link nav-toggle">
        <i class="fa fa-download"></i>
        <span class="title">Download Data</span>
        <span class="selected"></span>

    </a>
</li>
