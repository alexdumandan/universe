var admin_user_details;

$(document).ready(function(){

  $("#home_user_name").click(function(){
    $("#admin_change_access").modal("show");
  });

  $.ajax({
      type: 'GET',
      url: "json_getUserDetails.php",
      data: {},
      success: function(data){

        admin_user_details = $.parseJSON(data);
        $("#admin_country_change").val(admin_user_details.country);
        $("#admin_group_change").val(admin_user_details.group_name);

        $("#admin_country_change").change(function(){

          $.ajax({
              type: 'POST',
              url: "update_userDetail.php",
              data: {field:"country",value:$("#admin_country_change").val()},
              success: function(data){
                

              }
            });

        });

        $("#admin_group_change").change(function(){

          $.ajax({
              type: 'POST',
              url: "update_userDetail.php",
              data: {field:"group_name",value:$("#admin_group_change").val()},
              success: function(data){


              }
            });


        });



      }
  });


});
