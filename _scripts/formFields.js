(

  function($){

    $.widget( "custom.formFields", {

      options: {
       classInvalid: "req-field-invalid",
       classEmpty:"req-field-empty",
       classFieldMessage:"form-field-message"
      },

      _create: function() {

        var options = this.options;

        if ($(this.element).hasClass("req-field")){
          $(this.element).parent().children("label").append("<span style='color:red'>*</span>");
        }

        $(this.element).focusout(function(){

            _checkInput(this);

        });

        $(this.element).change(function(){

          _checkInput(this);

        });


        function _checkInput(element){
          //console.log(element);
          var e = $(element);
          if (e.hasClass("req-field")){

            if(!_checkIfEmpty(e)){
              _checkIfValidFormat(e);
            }

          }else{

              var empty = e.val()=="" || e.val()==null || e.val()==undefined;

              if(!empty){
                  _checkIfValidFormat(e);
              }else{

                e.removeClass(options.classInvalid);
                $("."+options.classFieldMessage + "[for=" + e.attr("id")+"]").html("");

              }

          }

        }

        function _checkIfEmpty(e){

          var checker = e.val()=="" || e.val()==null || e.val()==undefined || (e.val()==0 && e.is("select") ) || (e.val()=="Not Started" && e.is("select") );

          if(checker){

            e.addClass(options.classInvalid);
            e.addClass(options.classEmpty);
            $("."+options.classFieldMessage + "[for=" + e.attr("id")+"]").html("<span class='field-danger-message'>"+e.attr("data-empty-message")+"</span>");
            return true;

          }else{

            e.removeClass(options.classEmpty);
            e.removeClass(options.classInvalid);
            $("."+options.classFieldMessage + "[for=" + e.attr("id")+"]").html("");

            return false;

          }

        }

        function _checkIfValidFormat(e){

          if (e.is("input") && e.attr("data-regex") != undefined ) {

            if(e.attr("type")=="text"){

              if(_regexMatch(e.val(),e.attr("data-regex"))){

                e.removeClass(options.classEmpty);
                e.removeClass(options.classInvalid);
                $("."+options.classFieldMessage + "[for=" + e.attr("id")+"]").html("");

                return false;

              }else{

                e.addClass(options.classInvalid);
                e.addClass(options.classEmpty);
                $("."+options.classFieldMessage + "[for=" + e.attr("id")+"]").html("<span class='field-danger-message'>"+e.attr("data-invalidFormat-message")+"</span>");
                return true;

              }


            }

          }

        }


        function _regexMatch(str,pattern) {
            //path = path.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
            var regex = new RegExp(pattern),
                results = regex.exec(str);

            if (results == null){

              return false;

            }else{

              return true;

            }

        }




      },




    }); //end of widget;


  }(jQuery)

);
