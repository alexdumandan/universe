//Global variable
var active_alert_ntid;
var pending_items_count, pending_items_owner;
var daily_pending_items, daily_modified_items, daily_items_owner;
var ca_not_started_open_orders,ca_pending_open_orders, ca_open_orders_owner,
    daily_ca_not_started, daily_ca_pending, daily_ca_owner;
var hots_weekly_pending, hots_weekly_not_started, hots_weekly_owner,
    hots_daily_pending, hots_daily_not_started, hots_daily_owner;
var WEEKLY_OPEN_ITEMS,DAILY_OPEN_ITEMS,
    WEEKLY_CA_OPEN_ORDERS,DAILY_CA_OPEN_ORDERS,
    WEEKLY_HOTS,DAILY_HOTS,
    ACTIVE_ALERT;
//Global variable end

//Open Items email Start
function getWeeklyPendingOpenItems(){

  $.get("emails/json_get_weekly_open_items_notif.php", function(){
  }).done(function(weeklyData){

    WEEKLY_OPEN_ITEMS = $.parseJSON(weeklyData);
    var distinct_owner = [];
    var owner_ntid;


    $.each(WEEKLY_OPEN_ITEMS, function(index, _owner){

        if ($.inArray(_owner.owner_ntid, distinct_owner)==-1) {
          distinct_owner.push(_owner.owner_ntid);

        }
    });//each get  distinct_owner

    $.each(distinct_owner, function(index, _distinctItemOnwer){

      $.each(WEEKLY_OPEN_ITEMS, function(index, _weeklyItemsCount){
        if (_weeklyItemsCount.owner_ntid == _distinctItemOnwer) {
          pending_items_count = _weeklyItemsCount.itemCount;
          pending_items_owner = _weeklyItemsCount.owner;
        }

          console.log(pending_items_owner + " - "+ pending_items_count);
          sendEmailWeekly(pending_items_count, pending_items_owner);

      });// each _weeklyItemsCount

    });// each distinct_owner
    var sched = "weekly";
    var source = "Open Items";
      $.post("my_hub/Save_email_date.php",{sched:sched,source:source}, function(){
      }).done(function(emailData){
        console.log("Email sent by " + emailData);
        // $("#weekly_open_items").prop("disabled", true);
        // $("#weekly_open_items_text").html("Email sent by " + emailData);
      });//post Save_email_date

  });// done weeklyData

}

function sendEmailWeekly(pending_items_count, pending_items_owner){
  console.log("pending_items_owner: "+pending_items_owner)
  var owner_name = {
    function: "eulaGetFirstName",
    ntids : pending_items_owner
  };
  $.getJSON("http://pimawweb1001/EULA/api.php?callback=?", owner_name, function(resultData) {
    var name = resultData;

    var owner_email = {
      function: "eulaGetEmail",
      ntids: pending_items_owner
    };

    $.getJSON("http://pimawweb1001/EULA/api.php?callback=?", owner_email, function(resultData) {
      var email = resultData;

      console.log("name : " + name + " email : " + email);

      var month = new Array();
		    month[0] = "01";
		    month[1] = "02";
		    month[2] = "03";
		    month[3] = "04";
		    month[4] = "05";
		    month[5] = "06";
		    month[6] = "07";
		    month[7] = "08";
		    month[8] = "09";
		    month[9] = "10";
		    month[10] = "11";
		    month[11] = "12";
			var today = new Date();
			var months = month[today.getMonth()];
			var date = today.getDate();
			var year = today.getFullYear();
			var mdy = months + "/" + date + "/" + year;
			console.log(mdy);

      var email_format = 1;
      var email_subject = "Sales Hub: Weekly Open Items Aging Report - " + mdy;
      var email_from = "donotreply@ingrammicro.com";
      var email_to = email;
      // var email_to = "alexandra.dumandan@ingrammicro.com";
      var email_cc = "donotreply@ingrammicro.com";

      console.log("email to: "+email_to);

      var email_body = "<p style='font-family:Tahoma;font-size:14px;'> Hello " + name + ",</p>";
          email_body += "<p style='font-family:Tahoma;font-size:14px;'>You still have "+pending_items_count+ " pending item(s) on your My Hub that is more than 5 days.";
          email_body += "<p style='font-family:Tahoma;font-size:14px;'>You may view your pending item(s) by clicking this <a href=\'http://pimawweb1002/Sales_Hub/?page=my_hub\'>link</a>.</p>";
          email_body += "<b><p style='font-family:Tahoma;font-size:10px;font-style:italic;'>This is a system generated email. Please do not reply.</p></b>"
          console.log(email_subject + email_from + email_to + email_cc + email_body + email_format);
    			sendEmailData(email_subject, email_from, email_to, email_cc, email_body, email_format);


          // $("#weekly_open_items").prop("disabled", true);
          // $("#weekly_open_items_text").html("Date sent: "+mdy);

    });//eulaGetEmail

  });//eulaGetFullName

}

function getDailyPendingOpenItems(){

    $.get("emails/json_get_daily_open_items_notif.php", function(){
    }).done(function(dailyData){
      console.log(dailyData);
      DAILY_OPEN_ITEMS = $.parseJSON(dailyData);
        $.each(DAILY_OPEN_ITEMS, function(index, _dailyItemsCount){

            if (_dailyItemsCount.pending == 0 && _dailyItemsCount.modified == 0) {

            }else {
              daily_pending_items = _dailyItemsCount.pending;
              daily_modified_items = _dailyItemsCount.modified;
              daily_items_owner = _dailyItemsCount.owner;
              console.log(daily_items_owner + " - "+ daily_pending_items + " - "+daily_modified_items);
              sendEmailDaily(daily_pending_items,daily_modified_items, daily_items_owner);
            }

        });// each _dailyItemsCount


      // var sched = "daily";
      // var source = "Open Items";
      //   $.post("my_hub/Save_email_date.php",{sched:sched,source:source}, function(){
      //   }).done(function(emailData){
      //     console.log("Email sent by " + emailData);
      //     // $("#weekly_open_items").prop("disabled", true);
      //     // $("#weekly_open_items_text").html("Email sent by " + emailData);
      //   });//Save_email_date

    });// done dailyData
}

function sendEmailDaily(daily_pending_items,daily_modified_items, daily_items_owner){
  console.log("daily_items_owner: "+daily_items_owner);

  var owner_name =  {
		function: "eulaGetFirstName",
		ntids : daily_items_owner
	};

  $.getJSON("http://pimawweb1001/EULA/api.php?callback=?", owner_name, function(resultData) {
    var name = resultData;

    var owner_email = {
      function: "eulaGetEmail",
      ntids: daily_items_owner
    };

    $.getJSON("http://pimawweb1001/EULA/api.php?callback=?", owner_email, function(resultData) {
        var email = resultData;

        console.log("name : " + name + " email : " + email);
        var today = new Date();
        var month = today.getMonth();
        var date = today.getDate();
        var year = today.getFullYear();
        var mdy = month + "/" + date + "/" + year;

        var email_format = 1;
        var email_subject = "Sales Hub: Daily Open Items Status Report - " + mdy;
        var email_from = "donotreply@ingrammicro.com";
        var email_to = "alexandra.dumandan@ingrammicro.com";
        var email_to = email;
        // var email_cc = "donotreply@ingrammicro.com";
        console.log("email to: "+email_to);

        var email_body = "<p style='font-family:Tahoma;font-size:14px;'> Hello " + name + ",</p>";
            email_body += "<p style='font-family:Tahoma;font-size:14px;'>You have a total of "+daily_pending_items+" pending item(s)";
            if (daily_modified_items != 0) {
              email_body += " and "+daily_modified_items+" modified item(s)";
            }
            email_body += " on your My Hub.";
            email_body += "<p style='font-family:Tahoma;font-size:14px;'>You may view it by clicking this <a href=\'http://pimawweb1002/Sales_Hub/?page=my_hub\'>link</a>.</p>";
            email_body += "<b><p style='font-family:Tahoma;font-size:10px;font-style:italic;'>This is a system generated email. Please do not reply.</p></b>"
            console.log(email_subject + email_from + email_to + email_cc + email_body + email_format);
            sendEmailData(email_subject, email_from, email_to, email_cc, email_body, email_format);



      });//eulaGetEmail

  });//eullaGetFirstName


}
//Open Items email END

//CA Open Orders START
function getWeeklyCAopenOrder(){
  $.get("emails/json_getCAopenOrdersWeekly.php", function(){
  }).done(function(weeklyOpenData){

    WEEKLY_CA_OPEN_ORDERS = $.parseJSON(weeklyOpenData);
    var distinct_owner = [];
    var ca_owner_ntid;

    $.each(WEEKLY_CA_OPEN_ORDERS, function(index, owner_){

      if ($.inArray(owner_.ca_owner_ntid, distinct_owner)==-1) {
        distinct_owner.push(owner_.ca_owner_ntid);
      }
    });// get distinct_owner

    $.each(distinct_owner, function(index, _distinctOwner){

        $.each(WEEKLY_CA_OPEN_ORDERS, function(index, _weeklyOrdersCount){
          if (_weeklyOrdersCount.ca_owner_ntid == _distinctOwner) {
            ca_not_started_open_orders = _weeklyOrdersCount.ca_not_started;
            ca_pending_open_orders = _weeklyOrdersCount.ca_pending;
            ca_open_orders_owner = _weeklyOrdersCount.owner;
          }

          console.log(ca_open_orders_owner + " - " + ca_pending_open_orders + " , " + ca_not_started_open_orders);
          sendWeeklyEmailOpenOrders(ca_not_started_open_orders,ca_pending_open_orders, ca_open_orders_owner);

        });// each _weeklyOrdersCount

    });// each _distinctOwner

    var sched = "weekly";
    var source = "Open Orders";
      $.post("my_hub/Save_email_date.php",{sched:sched,source:source}, function(){
      }).done(function(emailData){
        console.log("Email sent by " + emailData);
        // $("#weekly_open_items").prop("disabled", true);
        // $("#weekly_open_items_text").html("Email sent by " + emailData);
      });//Save_email_date

  });//weeklyOpenData

}

function sendWeeklyEmailOpenOrders(ca_not_started_open_orders,ca_pending_open_orders, ca_open_orders_owner){
  console.log("ca_open_orders_owner: "+ca_open_orders_owner)
  var owner_name = {
    function: "eulaGetFirstName",
    ntids : ca_open_orders_owner
  };
  $.getJSON("http://pimawweb1001/EULA/api.php?callback=?", owner_name, function(resultData) {
    var name = resultData;

    var owner_email = {
      function: "eulaGetEmail",
      ntids: ca_open_orders_owner
    };

    $.getJSON("http://pimawweb1001/EULA/api.php?callback=?", owner_email, function(resultData) {
      var email = resultData;

      console.log("name : " + name + " email : " + email);

      var month = new Array();
		    month[0] = "01";
		    month[1] = "02";
		    month[2] = "03";
		    month[3] = "04";
		    month[4] = "05";
		    month[5] = "06";
		    month[6] = "07";
		    month[7] = "08";
		    month[8] = "09";
		    month[9] = "10";
		    month[10] = "11";
		    month[11] = "12";
			var today = new Date();
			var months = month[today.getMonth()];
			var date = today.getDate();
			var year = today.getFullYear();
			var mdy = months + "/" + date + "/" + year;
			console.log(mdy);

      var email_format = 1;
      var email_subject = "Sales Hub: Weekly CA Open Orders Aging Report - " + mdy;
      var email_from = "donotreply@ingrammicro.com";
      var email_to = email;
      // var email_to = "alexandra.dumandan@ingrammicro.com";
      var email_cc = "donotreply@ingrammicro.com";

      console.log("email to: "+email_to);

      var email_body = "<p style='font-family:Tahoma;font-size:14px;'> Hello " + name + ",</p>";
          email_body += "<p style='font-family:Tahoma;font-size:14px;'>You have a total of "+ca_pending_open_orders+" pending item(s)";
          if (ca_not_started_open_orders != 0) {
            email_body += " and "+ca_not_started_open_orders+" item(s) that is Not Started";
          }
          email_body += " on your My Hub.";
          email_body += "<p style='font-family:Tahoma;font-size:14px;'>You may view it by clicking this <a href=\'http://pimawweb1002/Sales_Hub/?page=my_hub\'>link</a>.</p>";
          email_body += "<b><p style='font-family:Tahoma;font-size:10px;font-style:italic;'>This is a system generated email. Please do not reply.</p></b>"
          console.log(email_subject + email_from + email_to + email_cc + email_body + email_format);
    			sendEmailData(email_subject, email_from, email_to, email_cc, email_body, email_format);

    });//eulaGetEmail

  });//eulaGetFullName
}

function getDailyCAopenOrder(){
  $.get("emails/json_getCAopenOrdersDaily.php", function() {
  }).done(function(dailyOpenData){
    console.log(dailyOpenData);
      DAILY_CA_OPEN_ORDERS = $.parseJSON(dailyOpenData);
      var distinct_owner = [];
      var ca_owner_ntid;

      $.each(DAILY_CA_OPEN_ORDERS, function(index, owner_){

        if ($.inArray(owner_.ca_owner_ntid, distinct_owner)==-1) {
          distinct_owner.push(owner_.ca_owner_ntid);
        }

      });// distinct_owner

      $.each(distinct_owner, function(index, _distinctOwner){

        $.each(DAILY_CA_OPEN_ORDERS, function(index, _dailyOrdersCount){
          if (_dailyOrdersCount.ca_owner_ntid == _distinctOwner) {
            if (_dailyOrdersCount.ca_pending == 0 && _dailyOrdersCount.ca_not_started == 0) {

            }else {
              daily_ca_not_started = _dailyOrdersCount.ca_not_started;
              daily_ca_pending = _dailyOrdersCount.ca_pending;
              daily_ca_owner = _dailyOrdersCount.owner;
            }
          }

          console.log(daily_ca_owner + " - "+ daily_ca_pending + " - "+daily_ca_not_started);
          // sendDailyEmailOpenOrders(daily_ca_not_started, daily_ca_pending, daily_ca_owner);

        });// each _dailyOrdersCount

      });// each _distinctOwner

      var sched = "daily";
      var source = "Open Orders";
        $.post("my_hub/Save_email_date.php",{sched:sched,source:source}, function(){
        }).done(function(emailData){
          console.log("Email sent by " + emailData);
          // $("#weekly_open_items").prop("disabled", true);
          // $("#weekly_open_items_text").html("Email sent by " + emailData);
        });//Save_email_date

  });// dailyOpenData
}

function sendDailyEmailOpenOrders(daily_ca_not_started, daily_ca_pending, daily_ca_owner){
  console.log("daily_ca_owner: "+daily_ca_owner)
  var owner_name = {
    function: "eulaGetFirstName",
    ntids : daily_ca_owner
  };
  $.getJSON("http://pimawweb1001/EULA/api.php?callback=?", owner_name, function(resultData) {
    var name = resultData;

    var owner_email = {
      function: "eulaGetEmail",
      ntids: daily_ca_owner
    };

    $.getJSON("http://pimawweb1001/EULA/api.php?callback=?", owner_email, function(resultData) {
      var email = resultData;

      console.log("name : " + name + " email : " + email);

      var month = new Array();
		    month[0] = "01";
		    month[1] = "02";
		    month[2] = "03";
		    month[3] = "04";
		    month[4] = "05";
		    month[5] = "06";
		    month[6] = "07";
		    month[7] = "08";
		    month[8] = "09";
		    month[9] = "10";
		    month[10] = "11";
		    month[11] = "12";
			var today = new Date();
			var months = month[today.getMonth()];
			var date = today.getDate();
			var year = today.getFullYear();
			var mdy = months + "/" + date + "/" + year;
			console.log(mdy);

      var email_format = 1;
      var email_subject = "Sales Hub: Daily CA Open Orders Status Report - " + mdy;
      var email_from = "donotreply@ingrammicro.com";
      var email_to = email;
      // var email_to = "alexandra.dumandan@ingrammicro.com";
      var email_cc = "donotreply@ingrammicro.com";

      console.log("email to: "+email_to);

      var email_body = "<p style='font-family:Tahoma;font-size:14px;'> Hello " + name + ",</p>";
          email_body += "<p style='font-family:Tahoma;font-size:14px;'>You have a total of "+daily_ca_pending+" pending item(s)";
          if (daily_ca_not_started != 0) {
            email_body += " and "+daily_ca_not_started+" item(s) that is Not Started";
          }
          email_body += " on your My Hub.";
          email_body += "<p style='font-family:Tahoma;font-size:14px;'>You may view it by clicking this <a href=\'http://pimawweb1002/Sales_Hub/?page=my_hub\'>link</a>.</p>";
          email_body += "<b><p style='font-family:Tahoma;font-size:10px;font-style:italic;'>This is a system generated email. Please do not reply.</p></b>"
          console.log(email_subject + email_from + email_to + email_cc + email_body + email_format);
    			sendEmailData(email_subject, email_from, email_to, email_cc, email_body, email_format);

    });//eulaGetEmail

  });//eulaGetFullName

}
//CA Open Orders END


//HOTs Email START
function getWeeklyHots(){
  $.get("emails/json_getHotsWeekly.php", function(){
  }).done(function(weeklyHotsData){

      WEEKLY_HOTS = $.parseJSON(weeklyHotsData);
      var distinct_owner = [];
      var hots_onwer;

      $.each(WEEKLY_HOTS, function(index, h_owner){

        if ($.inArray(h_owner.hots_owner, distinct_owner)==-1) {
          distinct_owner.push(h_owner.hots_owner);
        }
      });// get distinct_owner

      $.each(distinct_owner, function(index, _hotsDistinctOwner){

        $.each(WEEKLY_HOTS, function(index, _hotsWeeklyCount){
          if (_hotsWeeklyCount.hots_owner_ntid == _hotsDistinctOwner) {
            hots_weekly_pending = _hotsWeeklyCount.hots_pending;
            hots_weekly_not_started = _hotsWeeklyCount.hots_not_started;
            hots_weekly_owner = _hotsWeeklyCount.owner;
          }

          console.log(hots_weekly_owner + " - " + hots_weekly_pending + " , " + hots_weekly_not_started);
          // sendWeeklyEmailHots(hots_weekly_pending, hots_weekly_not_started, hots_weekly_owner);

        });//each _hotsWeeklyCount

      });// each _hotsDistinctOwner

      var sched = "weekly";
      var source = "HOTS";
        $.post("my_hub/Save_email_date.php",{sched:sched,source:source}, function(){
        }).done(function(emailData){
          console.log("Email sent by " + emailData);
          // $("#weekly_open_items").prop("disabled", true);
          // $("#weekly_open_items_text").html("Email sent by " + emailData);
        });//Save_email_date

  });//done weeklyHotsData
}

function sendWeeklyEmailHots(hots_weekly_pending, hots_weekly_not_started, hots_weekly_owner){
  console.log("hots_weekly_owner: "+hots_weekly_owner)
  var owner_name = {
    function: "eulaGetFirstName",
    ntids : hots_weekly_owner
  };
  $.getJSON("http://pimawweb1001/EULA/api.php?callback=?", owner_name, function(resultData) {
    var name = resultData;

    var owner_email = {
      function: "eulaGetEmail",
      ntids: hots_weekly_owner
    };

    $.getJSON("http://pimawweb1001/EULA/api.php?callback=?", owner_email, function(resultData) {
      var email = resultData;

      console.log("name : " + name + " email : " + email);

      var month = new Array();
		    month[0] = "01";
		    month[1] = "02";
		    month[2] = "03";
		    month[3] = "04";
		    month[4] = "05";
		    month[5] = "06";
		    month[6] = "07";
		    month[7] = "08";
		    month[8] = "09";
		    month[9] = "10";
		    month[10] = "11";
		    month[11] = "12";
			var today = new Date();
			var months = month[today.getMonth()];
			var date = today.getDate();
			var year = today.getFullYear();
			var mdy = months + "/" + date + "/" + year;
			console.log(mdy);

      var email_format = 1;
      var email_subject = "Sales Hub: Weekly CA Open Orders Aging Report - " + mdy;
      var email_from = "donotreply@ingrammicro.com";
      var email_to = email;
      // var email_to = "alexandra.dumandan@ingrammicro.com";
      var email_cc = "donotreply@ingrammicro.com";

      console.log("email to: "+email_to);

      var email_body = "<p style='font-family:Tahoma;font-size:14px;'> Hello " + name + ",</p>";
          email_body += "<p style='font-family:Tahoma;font-size:14px;'>You have a total of "+hots_weekly_pending+" pending item(s)";
          if (hots_weekly_not_started != 0) {
            email_body += " and "+hots_weekly_not_started+" item(s) that is Not Started";
          }
          email_body += " on your My Hub.";
          email_body += "<p style='font-family:Tahoma;font-size:14px;'>You may view it by clicking this <a href=\'http://pimawweb1002/Sales_Hub/?page=my_hub\'>link</a>.</p>";
          email_body += "<b><p style='font-family:Tahoma;font-size:10px;font-style:italic;'>This is a system generated email. Please do not reply.</p></b>"
          console.log(email_subject + email_from + email_to + email_cc + email_body + email_format);
    			sendEmailData(email_subject, email_from, email_to, email_cc, email_body, email_format);

    });//eulaGetEmail

  });//eulaGetFullName
}

function getDailyHots(){
  $.get("emails/json_getHotsDaily.php", function(){
  }).done(function(dailyHotsData){

      DAILY_HOTS = $.parseJSON(dailyHotsData);
      var distinct_owner = [];
      var hots_onwer;

      $.each(DAILY_HOTS, function(index, h_owner){

        if ($.inArray(h_owner.hots_owner, distinct_owner)==-1) {
          distinct_owner.push(h_owner.hots_owner);
        }
      });// get distinct_owner

      $.each(distinct_owner, function(index, _hotsDistinctOwner){

        $.each(DAILY_HOTS, function(index, _hotsDailyCOunt){
          if (_hotsDailyCOunt.hots_onwer == _hotsDistinctOwner) {
            if (_hotsDailyCOunt.hots_pending == 0 && _hotsDailyCOunt.hots_not_started == 0) {

            }else {
              hots_daily_pending = _hotsDailyCOunt.hots_pending;
              hots_daily_not_started = _hotsDailyCOunt.hots_not_started;
              hots_daily_owner = _hotsDailyCOunt.owner;
            }
          }

          console.log(hots_daily_owner + " - "+ hots_daily_pending + " - "+hots_daily_not_started);
          // sendDailyEmailHots(hots_daily_pending, hots_daily_not_started, hots_daily_owner);

        });//each _hotsDailyCOunt

      });// each _hotsDistinctOwner

      var sched = "daily";
      var source = "HOTS";
        $.post("my_hub/Save_email_date.php",{sched:sched,source:source}, function(){
        }).done(function(emailData){
          console.log("Email sent by " + emailData);
          // $("#weekly_open_items").prop("disabled", true);
          // $("#weekly_open_items_text").html("Email sent by " + emailData);
        });//Save_email_date

  });

}

function sendDailyEmailHots(hots_daily_pending, hots_daily_not_started, hots_daily_owner){

  console.log("hots_daily_owner: "+hots_daily_owner)
  var owner_name = {
    function: "eulaGetFirstName",
    ntids : hots_daily_owner
  };
  $.getJSON("http://pimawweb1001/EULA/api.php?callback=?", owner_name, function(resultData) {
    var name = resultData;

    var owner_email = {
      function: "eulaGetEmail",
      ntids: hots_daily_owner
    };

    $.getJSON("http://pimawweb1001/EULA/api.php?callback=?", owner_email, function(resultData) {
      var email = resultData;

      console.log("name : " + name + " email : " + email);

      var month = new Array();
        month[0] = "01";
        month[1] = "02";
        month[2] = "03";
        month[3] = "04";
        month[4] = "05";
        month[5] = "06";
        month[6] = "07";
        month[7] = "08";
        month[8] = "09";
        month[9] = "10";
        month[10] = "11";
        month[11] = "12";
      var today = new Date();
      var months = month[today.getMonth()];
      var date = today.getDate();
      var year = today.getFullYear();
      var mdy = months + "/" + date + "/" + year;
      console.log(mdy);

      var email_format = 1;
      var email_subject = "Sales Hub: Daily CA Open Orders Status Report - " + mdy;
      var email_from = "donotreply@ingrammicro.com";
      var email_to = email;
      // var email_to = "alexandra.dumandan@ingrammicro.com";
      var email_cc = "donotreply@ingrammicro.com";

      console.log("email to: "+email_to);

      var email_body = "<p style='font-family:Tahoma;font-size:14px;'> Hello " + name + ",</p>";
          email_body += "<p style='font-family:Tahoma;font-size:14px;'>You have a total of "+hots_daily_pending+" pending item(s)";
          if (hots_daily_not_started != 0) {
            email_body += " and "+hots_daily_not_started+" item(s) that is Not Started";
          }
          email_body += " on your My Hub.";
          email_body += "<p style='font-family:Tahoma;font-size:14px;'>You may view it by clicking this <a href=\'http://pimawweb1002/Sales_Hub/?page=my_hub\'>link</a>.</p>";
          email_body += "<b><p style='font-family:Tahoma;font-size:10px;font-style:italic;'>This is a system generated email. Please do not reply.</p></b>"
          console.log(email_subject + email_from + email_to + email_cc + email_body + email_format);
          sendEmailData(email_subject, email_from, email_to, email_cc, email_body, email_format);

    });//eulaGetEmail

  });//eulaGetFullName
}
// HOTs Email END


  function dispSuccess(){
  	console.log("email sent");
  }
  function dispError(){
  	console.log("error sending email");
  }
  function preProc(){
  }
  function postProc(){
  }

  function sendEmailData(email_subject, email_from, email_to, email_cc, email_body, email_format){
  	emailSender.sendEmail({
  		"clientScriptLoc":"email_API/client.php",
  		"email_subject":email_subject,
  		"email_from":email_from,
  		"email_to":email_to,
  		"email_cc":email_cc,
  		"email_body":email_body,
  		"email_format":email_format
  	});
  	// console.log(result);
  }
