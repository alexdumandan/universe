<?php
include "../_libs/db_connect.php";

$query ="SELECT
		header_ticket_id AS ticket_id,
    header_description AS description,
		portfolio_name AS portfolio,
    header_sap_proj_number AS sap_proj_number,
    header_requestor AS requestor,
    transaction_customer_number AS customer_number,
    transaction_customer_name AS customer_name,
    transaction_amount AS amount,
    transaction_admin_fee AS admin_fee,
    (transaction_amount + transaction_admin_fee) AS total,
    transaction_status AS status
FROM tb_header AS h
LEFT JOIN tb_transaction AS t
ON h.header_ticket_id=t.transaction_ticket_id
LEFT JOIN tb_portfolio AS p
ON h.header_portfolio_id=p.portfolio_id";

$trs = "";
$result = mysqli_query($con, $query);
if ($result) {
	while($row = mysqli_fetch_assoc($result)) {
			$trs = $trs . "<tr><td class='table-checkbox'><input id='group_check' type='checkbox' class='group-checkable'/></td>";
			$trs = $trs . "<td><b><a href='?portfolio=paf&page=view_claim&requestID=" . $row['ticket_id'] . "' style='color:#2962FF;'>" . $row['ticket_id'] . "</a></b></td>";
			$trs = $trs . "<td>". $row['portfolio'] . "</td>";
			$trs = $trs . "<td>". $row['customer_name'] . "</td>";
			$trs = $trs . "<td>". $row['description'] . "</td>";
			$trs = $trs . "<td>". $row['sap_proj_number'] . "</td>";
			$trs = $trs . "<td>". $row['amount'] . "</td>";
			$trs = $trs . "<td>". $row['admin_fee'] . "</td>";
			$trs = $trs . "<td>". $row['total'] . "</td>";
			$trs = $trs . "<td>0</td>";
			$trs = $trs . "<td>Credit Memo Number</td>";
			$trs = $trs . "<td></td>";
			$trs = $trs . "<td>". $row['status'] . "</td></tr>";
	}
}

echo $trs;
