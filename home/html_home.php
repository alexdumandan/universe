 <div class="row">
   <div class="col-md-12">
     <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
       <div class="dashboard-stat green">
         <div class="visual">
           <i class="fa fa-dollar"></i>
         </div>
         <div class="details">
           <div class="number" id="my_requests">0</div>
           <div class="desc">
             My Pending Requests/Inquiries
           </div>
         </div>
         <a class="more" href="javascript:;" style="cursor:context-menu">
         View list <i class="m-icon-swapright m-icon-white"></i>
         &nbsp;
         </a>
       </div>
     </div>

     <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
       <div class="dashboard-stat yellow">
         <div class="visual">
           <i class="fa fa-dollar"></i>
         </div>
         <div class="details">
           <div class="number" id="pending_claims">0</div>
           <div class="desc">
              Pending Claims/Inquiries Assigned To Me
           </div>
         </div>
         <a class="more" href="javascript:;" style="cursor:context-menu">
         View list <i class="m-icon-swapright m-icon-white"></i>
         &nbsp;
         </a>
       </div>
     </div>
     <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
       <div class="dashboard-stat red">
         <div class="visual">
           <i class="fa fa-dollar"></i>
         </div>
         <div class="details">
           <div class="number" id="with_findings">0</div>
           <div class="desc">
              For My Billing Approval
           </div>
         </div>
         <a class="more" href="javascript:;" style="cursor:context-menu">
         View list <i class="m-icon-swapright m-icon-white"></i>
         &nbsp;
         </a>
       </div>
     </div>
     <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
       <div class="dashboard-stat blue">
         <div class="visual">
           <i class="fa fa-dollar"></i>
         </div>
         <div class="details">
           <div class="number" id="for_billing">0</div>
           <div class="desc">
              For My Approval
           </div>
         </div>
         <a class="more" href="javascript:;" style="cursor:context-menu">
         View list <i class="m-icon-swapright m-icon-white"></i>
         &nbsp;
         </a>
       </div>
     </div>
   </div>
 </div>


 <div class="page-title" id="page_title">
 </div>
<<<<<<< HEAD
=======
<<<<<<< HEAD
=======

<div class="row">
  <div class="col-md-12">
        <!-- <ul class="nav nav-tabs ">
            <li class="active">
                <a href="#tab_5_1" data-toggle="tab"> CLAIMS </a>
            </li>
            <li>
                <a href="#tab_5_2" data-toggle="tab"> INQUIRY </a>
            </li>

        </ul> -->
        <div class="tab-content">
            <div class="tab-pane active" id="tab_5_1">

              <div class="portlet light ">
                   <div class="portlet-title tabbable-line">
                       <div class="caption">
                           <i class="material-icons font-red">work</i>
                           <span class="caption-subject font-dark bold uppercase">Claims/Inquiries List</span>
                       </div>
                       <ul class="nav nav-tabs">
                           <li class="active">
                               <a href="#h_pending_claims" data-toggle="tab" aria-expanded="true" id="paf_pending_claims"> Claim list </a>
                           </li>
                           <li class="">
                               <a href="#completed_claims" data-toggle="tab" aria-expanded="false" id="paf_completd_claims"> This month's Completed Claims </a>
                           </li>
                           <li class="">
                               <a href="#archived_claims" data-toggle="tab" aria-expanded="false" id="paf_archived_claims"> Archived Claims </a>
                           </li>
                       </ul>
                   </div>
                   <div class="portlet-body">
                       <div class="tab-content">
                           <div class="dataTable-container tab-pane active" id="h_pending_claims">

                             <div id="table_container" class="table-container">

                             </div>

                            </div>
                           <div class=" dataTable-container tab-pane" id="completed_claims">

                             <div id="table_completed_container" class="table-container">
                               <table class="table" id="hot_orders_table">

                                 <thead>
                                   <th class="table-checkbox"><input id="group_check" type="checkbox" class="group-checkable" data-set="#hot_orders_table .checkboxes" onchange="selectItem('multi')"/></th>
                                   <th>Claim ID</th>
                                   <th>SL Number</th>
                                   <th>Customer Number</th>
                                   <th>Customer Name</th>
                                   <th>Description</th>
                                   <th>SAP Project Number</th>
                                   <th>Amount</th>
                                 </thead>

                                 <tbody id="hot_orders_table_body">

                                 </tbody>
                               </table>

                             </div>

                           </div>
                           <div class="dataTable-container tab-pane " id="archived_claims">

                             <div id="table_archived_container" class="table-container">
                               archived_claims
                             </div>

                           </div>
                       </div>
                   </div>
               </div>

            </div>

            <div class="tab-pane" id="tab_5_2">

              <div class="portlet light ">
                   <div class="portlet-title tabbable-line">
                       <div class="caption">
                           <i class="material-icons font-red">work</i>
                           <span class="caption-subject font-dark bold uppercase">PAF inquiry list</span>
                       </div>
                       <ul class="nav nav-tabs">
                           <li class="active">
                               <a href="#pending_claims" data-toggle="tab" aria-expanded="true" id="paf_pending_claims"> Today's Pending Inquiry </a>
                           </li>
                           <li class="">
                               <a href="#completed_claims" data-toggle="tab" aria-expanded="false" id="paf_completd_claims"> This month's Completed Inquiry </a>
                           </li>
                           <li class="">
                               <a href="#archived_claims" data-toggle="tab" aria-expanded="false" id="paf_archived_claims"> Archived Inquiry </a>
                           </li>
                       </ul>
                   </div>
                   <div class="portlet-body">
                       <div class="tab-content">
                           <div class="dataTable-container tab-pane active" id="pending_claims">

                             <div id="table_container" class="table-container">

                             </div>

                            </div>
                           <div class=" dataTable-container tab-pane" id="completed_claims">

                             <div id="table_completed_container" class="table-container">
                               <table class="table" id="paf_completed_table">

                                 <thead>
                                   <th class="table-checkbox"><input id="group_check" type="checkbox" class="group-checkable" data-set="#hot_orders_table .checkboxes" onchange="selectItem('multi')"/></th>
                                   <th>Claim ID</th>
                                   <th>SL Number</th>
                                   <th>Customer Number</th>
                                   <th>Customer Name</th>
                                   <th>Description</th>
                                   <th>SAP Project Number</th>
                                   <th>Amount</th>
                                 </thead>

                                 <tbody id="paf_completed_table_body">

                                 </tbody>
                               </table>

                             </div>

                           </div>

                           <div class="dataTable-container tab-pane " id="archived_claims">

                             <div id="table_archived_container" class="table-container">
                               archived_claims
                             </div>

                           </div>
                       </div>
                   </div>
               </div>

            </div>

        </div>
  </div>

</div>
>>>>>>> 3d8d4d6c3400a176f42bc18d22434507c8ce91ed
>>>>>>> 48ad8ce1167930e1137c3691f5e24b63fe4cf170
