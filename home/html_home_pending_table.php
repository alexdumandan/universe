<table class="table" id="home_pending_table">

  <thead>
    <th class="table-checkbox"><input id="group_check" type="checkbox" class="group-checkable" /></th>
    <th>Ticket ID</th>
    <th>Portfolio Name</th>
    <th>Customer Name</th>
    <th>Description</th>
    <th>SAP Project Number</th>
    <th>Amount</th>
    <th>Admin Fee</th>
    <th>Total</th>
    <th>Age</th>
    <th>Credit Memo</th>
    <th>QA Status</th>
    <th>Claim Status</th>
  </thead>
  <thead id="home_pending_table_col_filter">
     <tr>
       <th></th>
       <th></th>
       <th></th>
       <th></th>
       <th></th>
       <th></th>
       <th></th>
       <th></th>
       <th></th>
       <th></th>
       <th></th>
       <th></th>
       <th></th>
     </tr>
 </thead>
  <tbody id="home_pending_table_body">

  </tbody>
</table>
