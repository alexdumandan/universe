<?php


 ?>

 <div class="row" id="claims_view">
   <div class="col-md-12" id="claims_view_details">

     <div class="panel" >
       <div class="panel-heading" style="background: white;">
         <h3 class="panel-title font-green"><i class="material-icons">list</i> PAF Claim Details</h3>
       </div>
       <table class="table" id="claims_view_details_table">
         <tr>
           <td>Ticket ID</td>
           <td id="description">Ticket ID here</td>
         </tr>
         <tr>
           <td>Description</td>
           <td id="description">description here</td>
         </tr>
         <tr>
           <td>Sap Proj #</td>
           <td id="description">sap project number here</td>
         </tr>
         <tr>
           <td>Date Received</td>
           <td id="description">Date Received here</td>
         </tr>
         <tr>
           <td>Requestor</td>
           <td id="description">requestor name here</td>
         </tr>
         <tr>
           <td>Date Entered</td>
           <td id="description">Date Entered</td>
         </tr>

       </table>
     </div>

     <div class="panel">
       <div class="panel-heading" style="background:white;">
         <h3 class="panel-title font-green"><i class="material-icons">attach_money</i> Claim Table Details</h3>
       </div>

       <div class="row" style="margin:10px;">
         <div class="table-responsive" >
           <table class="table table-bordered table-hover" id="claims_view_details_table">
             <thead>
               <tr>
                 <th class="table-checkbox"></th>
                 <th class="" style="">Customer Number</th>
                 <th class="" style="">Customer Name</th>
                 <th class="" style="">Amount</th>
                 <th style="">Admin Fee</th>
                 <th style="">CM#</th>
                 <th style="">Processor</th>
                 <th style="">Status</th>
                 <th style="">Date Submitted to QA</th>
                 <th style="">Billing Date</th>
                 <th style="">Action Date</th>
                 <th style="">Billed By</th>
                 <th style="">Remarks</th>
                 <th style="">Action</th>
               </tr>
             </thead>
             <tbody id="claim_fields">
               <tr>
                 <td><input type="checkbox" class="checkboxes line-status-fields" id="line-status" value="0"/></td>
                 <td id="customer_number" style="">1234567890</td>
                 <td id="customer_name" style="">customer name customer name customer name</td>
                 <td id="claim_amount" style="">$ 1234567890.00</td>
                 <td id="admin_fee" style="">$ 12345678.90</td>
                 <td style=""><input class="amount form-field req-field req-field-empty" id="cm_number" type="text" style="width:160px;" pattern="-?[0-9]*(\.[0-9]+)?" data-empty-message="Enter amount."></td>
                 <td style=""><input class="amount form-field req-field req-field-empty" id="cm_number" type="text" style="width:160px;" pattern="-?[0-9]*(\.[0-9]+)?" data-empty-message="Enter amount."></td>
                 <td style=""><input class="amount form-field req-field req-field-empty" id="cm_number" type="text" style="width:160px;" pattern="-?[0-9]*(\.[0-9]+)?" data-empty-message="Enter amount."></td>
                 <td style=""><input class="amount form-field req-field req-field-empty" id="cm_number" type="text" style="width:160px;" pattern="-?[0-9]*(\.[0-9]+)?" data-empty-message="Enter amount."></td>
                 <td style=""><input class="amount form-field req-field req-field-empty" id="cm_number" type="text" style="width:160px;" pattern="-?[0-9]*(\.[0-9]+)?" data-empty-message="Enter amount."></td>
                 <td style=""><input class="amount form-field req-field req-field-empty" id="cm_number" type="text" style="width:160px;" pattern="-?[0-9]*(\.[0-9]+)?" data-empty-message="Enter amount."></td>
                 <td style=""><input class="amount form-field req-field req-field-empty" id="cm_number" type="text" style="width:160px;" pattern="-?[0-9]*(\.[0-9]+)?" data-empty-message="Enter amount."></td>
                 <td style=""><input class="amount form-field req-field req-field-empty" id="cm_number" type="text" style="width:160px;" pattern="-?[0-9]*(\.[0-9]+)?" data-empty-message="Enter amount."></td>
                 <td style=""><span>
                   <a class="btn  green" id="addClaimRow_btn" >
                      Update
                   </a>
                 </span></td>
               </tr>
             </tbody>
           </table>
         </div>
       </div>

     </div>

     <div class="panel">
       <div class="panel-heading" style="background:white;">
         <h3 class="panel-title font-green"><i class="material-icons">note</i> Comments</h3>
       </div>

       <div class="" id="comments_history">

       </div>
     </div>

     <div class="panel">
       <div class="panel-heading" style="background:white;">
         <h3 class="panel-title font-green"><i class="material-icons">attach_file</i> Attachments</h3>
       </div>

       <div class="" id="attachment_history">

       </div>
     </div>

     <div class="panel">
 			<div class="panel-heading" style="background: white;">
 				<h3 class="panel-title font-green"><i class="material-icons">widgets</i> Other Details</h3>
 			</div>
       <table class="table" id="claims_view_details_table">
         <tr>
           <td>Comment </td>
           <td>
             <textarea class="form-control" rows="3" maxlength="1200" id="comment"></textarea>
           </td>
         </tr>
         <tr>
           <td>Attachment </td>
           <td>
             <div class="row" id="file_attachment_group">
               <div class="col-md-4">
                   <button class="btn red btn-block" type="button" id="add_attachment_btn" onclick="addAttachment()"><i class="material-icons">add</i> Add attachments</button>
               </div>

             </div>
           </td>
         </tr>

       </table>
 		</div>

   </div>
 </div>
