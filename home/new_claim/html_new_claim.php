<?php


 ?>

<div class="row" id="claims_new" >
  <div class="col-md-12" id="claims_new_details" >

    <div class="panel" id="claims" >
			<div class="panel-heading" style="background: white;">
				<h3 class="panel-title font-green"><i class="material-icons">list</i> PAF New Claim Information</h3>
			</div>
        <table class="table" id="claims_new_details_table">
          <tr>
            <td class="req-field">Description </td>
            <td>
              <textarea class="form-control form-field req-field req-field-empty" rows="2" maxlength="1200" id="description" placeholder="Enter claim description" data-empty-message="Enter claim description"></textarea>
            </td>
          </tr>
          <tr>
            <td class="req-field">SAP Proj# </td>
            <td>
                <input type="text" class="form-control input-sm form-field req-field req-field-empty" placeholder="Enter SAP Project Number" maxlength="50" id="sap_proj_number" data-empty-message="Enter SAP Project Number">
            </td>
          </tr>
          <tr>
            <td>Requestor </td>
            <td>
              <input type="text" class="form-control input-sm" placeholder="Enter Requestor Name" maxlength="50" id="requestor">
            </td>
          </tr>
          <tr>
            <td class="req-field">Date Received </td>
            <td>
              <div class="input-group">
      					<input type="text" class="form-control form-field req-field req-field-empty" data-date-format="yyyy-mm-dd" id="date_received" data-empty-message="Select a date received."  placeholder="Select a Date Received">
      					<span class="input-group-addon">
                  <i class="fa fa-calendar"></i>
      					</span>
      				</div>
            </td>
          </tr>
        </table>
		</div>

    <div class="panel">
			<div class="panel-heading" style="background: white;">
				<h3 class="panel-title font-green"><i class="material-icons">attach_money</i> Claim Table Details</h3>
			</div>

      <div class="row" style="margin:10px;">
        <div class="table-responsive" >
          <table class="table table-bordered table-hover" id="claims_new_details_table">
            <thead>
              <tr>
                <th class="req-field" style="">Customer Number</th>
                <th class="req-field" style="width:450px;">Customer Name</th>
                <th class="req-field" style="">Amount</th>
                <th style="">Admin Fee</th>
                <th style="width:50px;padding-right:0px;"><span>
                  <a class="btn btn-icon-only green" id="addClaimRow_btn" >
                    <i class="fa fa-plus"></i>
                  </a>
                </span></th>
                <!-- onclick="addClaimField()" -->
              </tr>
            </thead>
            <tbody id="claim_fields">
              <tr>
                <td style=""><input class="cust-num form-field req-field req-field-empty" id="customer_number" type="text" style="width:150px;" maxlength="20" data-empty-message="Enter customer number."></td>
                <td style="width:450px;"><input class="cust-name form-field req-field req-field-empty" id="customer_name" type="text" style="width:450px;" maxlength="150" data-empty-message="Enter customer name."></td>
                <td style=""><input class="amount form-field req-field req-field-empty" id="claim_amount" type="text" style="width:160px;" pattern="-?[0-9]*(\.[0-9]+)?" data-empty-message="Enter amount."></td>
                <td style=""><input class="admin-fee" id="admin_fee" type="text" style="width:160px;" pattern="-?[0-9]*(\.[0-9]+)?"></td>
                <td style="width:50px;"></td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>

		</div>

    <div class="panel">
			<div class="panel-heading" style="background: white;">
				<h3 class="panel-title font-green"><i class="material-icons">widgets</i> Other Details</h3>
			</div>
      <table class="table" id="claims_new_details_table">
        <tr>
          <td>Comment </td>
          <td>
            <textarea class="form-control" rows="3" maxlength="1200" id="comment"></textarea>
          </td>
        </tr>
        <tr>
          <td>Attachment </td>
          <td>
            <div class="row" id="file_attachment_group">
              <div class="col-md-4">
                  <button class="btn red btn-block" type="button" id="add_attachment_btn" onclick="addAttachment()"><i class="material-icons">add</i> Add attachments</button>
              </div>

            </div>
          </td>
        </tr>

      </table>
		</div>

    <div class="col-md-12">
      <div class="panel">
        <button class="btn blue btn-lg btn-block" type="button" id="submit_claim_btn" style="text-transform:uppercase;"><i class="material-icons">send</i> Submit claim request</button>
      </div>
    </div>

  </div>
</div>
<!--
<div id="saving_modal" class="modal" tabindex="-1" data-backdrop="static" data-keyboard="false" data-width-"350">
   <div class="modal-body">
     <p style="font-size:20px"><i class="fa fa-circle-o-notch fa-spin"></i> &nbsp Saving claim request...</p>
   </div>
   <div class="modal-footer">
       <a id="saving_modal_confirm" style="display:none" href="?page=my_requests" class="btn green">OK</a>
       <a type="button" id="saving_modal_error" style="display:none"  data-dismiss="modal" class="btn red">OK</a>
   </div>
</div> -->

<div id="saving_modal" class="modal fade in" tabindex="-1" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <p style="font-size:20px"><i class="fa fa-circle-o-notch fa-spin"></i> &nbsp Saving claim request...</p>
      </div>
      <div class="modal-footer">
          <a id="saving_modal_confirm" style="display:none" href="?page=my_requests" class="btn green">OK</a>
          <a type="button" id="saving_modal_error" style="display:none"  data-dismiss="modal" class="btn red">OK</a>
      </div>
    </div>
  </div>
</div>
