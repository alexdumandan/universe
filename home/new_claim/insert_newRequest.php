<?php
require "../../../EULA/API.php";
include '../../_libs/db_connect.php';
require "../../../_Utils/userDetect.php";
require "../../../ESEO/V1/API.php";

date_default_timezone_set('US/Eastern');

$date = date("Y-m-d H:i:s");

$arrFieldValues = array ();
$arrParams = array ();

$arrFields = array(

  "header_description",
  "header_sap_proj_number",
  "header_date_received",
  "header_date_entered",
  "header_portfolio_id",
  "header_requestor",

);
// echo $arrFields;

function returnZeroIfString($input){

  if(is_numeric($input)){
    return $input;
  }else{
    return "0";
  }

}

$arrFieldValues["header_description"] = $_POST["description"];
$arrFieldValues["header_sap_proj_number"] = $_POST["sap_proj_number"];
$arrFieldValues["header_date_received"] = $_POST["date_received"];
$arrFieldValues["header_date_entered"] = $date;
$arrFieldValues["header_portfolio_id"] = $_POST["portfolio_id"];
$arrFieldValues["header_requestor"] = $_POST["requestor"];

$insert = mysqli_prepare($con,
          "INSERT INTO tb_header (".implode($arrFields,",").")"
          . "VALUES (?,?,?,?,?,?)");

foreach($arrFields as $data){
  $arrParams[] = &$arrFieldValues[$data];
}

if ($insert === false) {

  ESEO_log($con);
  echo mysqli_error($con);
  mysqli_close($con);
  die ("::ERROR::QUERY");

};

$strFieldTypes = "ssssss";

$bind = call_user_func_array("mysqli_stmt_bind_param", array_merge(array($insert, $strFieldTypes), $arrParams));

if ($bind === false) {
  echo mysqli_error($con);
    mysqli_close($con);
  die ("::ERROR::BIND");

}

$exec = mysqli_stmt_execute($insert);

if ($exec === false) {

  echo mysqli_error($con);
  mysqli_close($con);
  die ("::ERROR::EXEC");

}

$newRequestID = mysqli_insert_id($con);

$attachDate = $date;
$attachBy = $MQPA_NTLogin;

$arrAttach = array (

  "attach_ticket_id",
  "attach_filename",
  "attach_links",
  "attach_uploaded_by",
  "attach_date",

);

$insertAttachment = mysqli_prepare($con,
                    "INSERT INTO tb_attachments (".implode($arrAttach,",").")"
                    . "VALUES (?,?,?,?,?)");

$i = 0;

if ($_POST["attachments"] != null) {
  foreach($_POST["attachments"]["fileNames"] as $fileLocation){

    $link = $fileLocation;
    $fileName = $_POST["attachments"]["fileNamesOrig"][$i];
    $i++;

    $bind = mysqli_stmt_bind_param($insertAttachment, "sssss",$newRequestID,$fileName,$link,$attachBy,$attachDate);
    if ($bind === false) {
      echo mysqli_error($con);
        mysqli_close($con);
      die ("::ERROR::BIND");

    }
    $exec = mysqli_stmt_execute($insertAttachment);
    if ($exec === false) {
      echo mysqli_error($con);
        mysqli_close($con);
      die ("::ERROR::BIND");

    }

  }
}

$commentDate = $date;
$commentBy = $MQPA_NTLogin;
$commentData = $_POST["comment"];
// $arrCommentFields = array ();
// $arrCommentParam = array ();

$arrComments = array (

  "comment_ticket_id",
  "comment_entry_date",
  "comment_history",
  "comment_entered_by",

);


$insertComment = mysqli_prepare($con,
                  "INSERT INTO tb_comments_history (".implode($arrComments,",").")"
                  . "VALUES (?,?,?,?)");

if ($_POST["comment"] != null) {

  if ($insert === false) {

    ESEO_log($con);
    echo mysqli_error($con);
    mysqli_close($con);
    die ("::ERROR::QUERY");

  };

  $bind = mysqli_stmt_bind_param($insertComment, "ssss",$newRequestID,$commentDate,$commentData,$commentBy);
  if ($bind === false) {
    echo mysqli_error($con);
      mysqli_close($con);
    die ("::ERROR::BIND");

  }
  $exec = mysqli_stmt_execute($insertComment);
  if ($exec === false) {

    echo mysqli_error($con);
    mysqli_close($con);
    die ("::ERROR::EXEC");

  }

}

// $arrClaims = array (
//
//   "transaction_ticket_id",
//   "transaction_customer_number",
//   "transaction_customer_name",
//   "transaction_amount",
//   "transaction_admin_fee",
//   "transaction_status",
//
// );
//
// if ($_POST["arrClaimsData"] != null) {
//
//   $insertClaims = mysqli_prepare($con,
//                   "INSERT INTO tb_transaction (".implode($arrClaims,",").")"
//                   . "VALUES (?,?,?,?,?,?)")
//
// }




echo  $newRequestID;


mysqli_close($con);

?>
