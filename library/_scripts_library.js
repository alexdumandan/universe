$(document).ready(function(){

    $("#page_content").load('library/html_library.php',function(){

    });

});

function getUserGuide(){
  $("#library_panel").load('library/files/US_Sales_Hub_Website_User_Guide.htm');
}

function getSiteMap(){
  $("#library_panel").load('library/files/US_Sales_Hub_Website_Pages.htm');
}

function getWarrantyCreate(){
  $("#library_panel").load('library/files/US_Sales_Hub_Website_User_Manual_New_Item.htm');
}

function getWarrantyModifyView(){
  $("#library_panel").load('library/files/US_Sales_Hub_Website_User_Manual_Modify_View.htm');
}

function getWarrantyModifyItem(){
  $("#library_panel").load('library/files/US_Sales_Hub_Website_User_Manual_Modify_Item.htm');
}

function getWarrantyExport(){
  $("#library_panel").load('library/files/US_Sales_Hub_Website_User_Manual_Export.htm');
}
