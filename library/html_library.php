<h3 class="page-title" id="page_title">
  Library <span id="groupName"></span>
  <a href="?page=library" class="btn warranty-create" style="float:right;">
  	 Back to Library
  	</a>

    <br>
</h3>

<div class="row" id="library">
<div class="panel panel-default">
	<!-- <div class="panel-heading">
		<h3 class="panel-title">Primary Panel</h3>
	</div> -->

	<div class="panel-body" id="library_panel">
    <div class="tiles" id="tiles_size">
      <a href="?page=documents">
      <div class="tile bg-red-sunglo">
					<div class="tile-body">
						<i class="fa fa-briefcase"></i>
					</div>
					<div class="tile-object">
						<div class="name">
							 Documents
						</div>
						<div class="number">
						</div>
					</div>
				</div></a>
        <div class="tile bg-blue-steel" onclick="getUserGuide()">
					<div class="tile-body">
						<i class="fa fa-file-text-o"></i>
					</div>
					<div class="tile-object">
						<div class="name">
							 User Guide
						</div>
						<div class="number">
						</div>
					</div>
				</div>

        <div class="tile bg-yellow-saffron" onclick="getSiteMap()">
					<div class="corner">
					</div>
					<div class="tile-body">
						<i class="fa fa-sitemap"></i>
					</div>
					<div class="tile-object">
						<div class="name">
							 Site Map
						</div>
						<div class="number">

						</div>
					</div>
				</div>
      </div><br>

      <h3 class="page-title" id="page_title">
        Open Items
          <br>
      </h3>

      <div class="tiles" id="tiles_size">
        <div class="tile double bg-grey-cascade" onclick="getWarrantyCreate()">
					<div class="tile-body">

						<h3>Creating New Item</h3>
            <i class="fa fa-plus-square" style="float:right;"></i><p><br></p>
						<p>
							 View instructions in creating new item in Open Items.
						</p>
					</div>
					<div class="tile-object">
						<div class="name">
						</div>
						<div class="number">
						</div>
					</div>
				</div>
        <div class="tile bg-purple-studio" onclick="getWarrantyModifyView()">
					<div class="corner">
					</div>
					<div class="check">
					</div>
					<div class="tile-body">
						<i class="fa fa-cogs"></i>
					</div>
					<div class="tile-object">
						<div class="name">
							 Modify View
						</div>
					</div>
				</div>
				<div class="tile double bg-blue-madison" onclick="getWarrantyModifyItem()">
					<div class="corner">
					</div>
					<div class="check">
					</div>
					<div class="tile-body">
						<h3>Modifying an Item</h3>
            <i class="fa fa-edit" style="float:right;"></i><p><br></p>
						<p>
							 View instructions on how to modify an item in Open Items.
						</p>
					</div>
					<div class="tile-object">
						<div class="name">
						</div>
						<div class="number">

						</div>
					</div>
				</div>

        <div class="tile bg-green-meadow" onclick="getWarrantyExport()">
					<div class="tile-body">
						<i class="fa fa-download"></i>
					</div>
					<div class="tile-object">
						<div class="name">
							 Exporting List
						</div>
						<div class="number">

						</div>
					</div>
				</div>

			</div><br>

      <h3 class="page-title" id="page_title">
        Claims / Inquiries
          <br>
      </h3>

      <!-- <div class="tiles" id="tiles_size">
        <div class="tile double bg-grey-cascade" onclick="">
					<div class="tile-body">

						<h3>Creating New Item</h3>
            <i class="fa fa-plus-square" style="float:right;"></i><p><br></p>
						<p>
							 View instructions in creating new item in Open Items.
						</p>
					</div>
					<div class="tile-object">
						<div class="name">
						</div>
						<div class="number">
						</div>
					</div>
				</div>
        <div class="tile bg-purple-studio" onclick="">
					<div class="corner">
					</div>
					<div class="check">
					</div>
					<div class="tile-body">
						<i class="fa fa-cogs"></i>
					</div>
					<div class="tile-object">
						<div class="name">
							 Modify View
						</div>
					</div>
				</div>
				<div class="tile double bg-blue-madison" onclick="">
					<div class="corner">
					</div>
					<div class="check">
					</div>
					<div class="tile-body">
						<h3>Modifying an Item</h3>
            <i class="fa fa-edit" style="float:right;"></i><p><br></p>
						<p>
							 View instructions on how to modify an item in Open Items.
						</p>
					</div>
					<div class="tile-object">
						<div class="name">
						</div>
						<div class="number">

						</div>
					</div>
				</div>

        <div class="tile bg-green-meadow" onclick="">
					<div class="tile-body">
						<i class="fa fa-download"></i>
					</div>
					<div class="tile-object">
						<div class="name">
							 Exporting List
						</div>
						<div class="number">

						</div>
					</div>
				</div>

			</div> -->

	</div>
</div>
</div>
