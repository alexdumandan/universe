<table class="table" id="cpp_completed_table">

  <thead>
    <th>Ticket ID</th>
    <th>Customer Name</th>
    <th>Description</th>
    <th>SAP Project Number</th>
    <th>Currency</th>
    <th>Amount</th>
    <th>Credit Memo</th>
    <th>QA Remarks</th>
    <th>Claim Status</th>
    <th>Date Completed</th>
    <th>Assigned To</th>
  </thead>

  <tbody id="cpp_completed_table_body">

  </tbody>
</table>
