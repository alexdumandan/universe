<?php
set_time_limit(0);
include "../_libs/db_connect.php";
require "../../_utils/userDetect.php";
require "../../EULA/API.php";
$user_id = $MQPA_NTLogin;
date_default_timezone_set('US/Eastern');
$archivedDate = $_POST['archiveDate'];

$query = "SELECT * FROM universe.tb_transaction LEFT JOIN universe.tb_header ON tb_header.header_ticket_id=tb_transaction.transaction_ticket_id
          WHERE transaction_status IN ('Completed') AND header_portfolio_id='4' AND DATE(transaction_billing_date)='$archivedDate'";
//  AND MONTH(transaction_billing_date) = MONTH(NOW())
$result = mysqli_query($con, $query);
$trs = "";
if ($result) {
while($row = mysqli_fetch_assoc($result)) {

  $trs = $trs . "<tr>";
  $trs = $trs . "<td><b><a href='?portfolio=cpp&page=view_claim&requestID=" . $row["transaction_ticket_id"] . "' style='color:#2962FF;text-decoration:underline;'>" . $row["transaction_ticket_id"] . "</a></b></td>";
  $trs = $trs . "<td>". $row["transaction_customer_name"] . "</td>";
  $trs = $trs . "<td>". $row["header_description"] . "</td>";
  $trs = $trs . "<td>". $row["header_sap_proj_number"] . "</td>";
  $trs = $trs . "<td>". $row["transaction_amount"] . "</td>";
  $trs = $trs . "<td>". $row["transaction_credit_memo"] . "</td>";
  $trs = $trs . "<td>". $row["transaction_remarks"] . "</td>";
  $trs = $trs . "<td>". $row["transaction_status"] . "</td>";
  $trs = $trs . "<td>". date("Y-m-d", strtotime($row["transaction_billing_date"])) . "</td>";
  $trs = $trs . "<td>". eulaGetFullName(strtolower($row["transaction_processor"])) . "</td></tr>";
  }
}


mysqli_close($con);

echo $trs;

?>
