<?php
set_time_limit(0);
include "../../_libs/db_connect.php";
require "../../../_utils/userDetect.php";
require "../../../EULA/API.php";

$arrID = $_GET['selectedItemsID'];
$arrID = implode(",",$arrID);

$query = "SELECT * FROM tb_transaction WHERE transaction_id IN ($arrID)";

$arrJSON = array();

$result = mysqli_query($con, $query);
$rowNumber = 0;
while($row = mysqli_fetch_assoc($result)) {

  $arrJSON[$rowNumber]["claimID"] = $row["transaction_id"];
  $arrJSON[$rowNumber]["customer_name"] = $row["transaction_customer_name"];
  $arrJSON[$rowNumber]["customer_number"] = $row["transaction_customer_number"];
  $arrJSON[$rowNumber]["amount"] = $row["transaction_amount"];
  $arrJSON[$rowNumber]["total_claim"] = $row["transaction_amount"] + $row["transaction_admin_fee"];
  $arrJSON[$rowNumber]["currency"] = $row["transaction_currency"];
  $arrJSON[$rowNumber]["ppref"] = $row["transaction_ppref"];
  $arrJSON[$rowNumber]["vendor_debit_number"] = $row["transaction_vendor_debit_number"];
  $arrJSON[$rowNumber]["credit_memo"] = $row["transaction_credit_memo"];

  $rowNumber++;
}

mysqli_close($con);

echo json_encode($arrJSON);

?>
