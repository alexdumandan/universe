<?php
include "../../_libs/db_connect.php";
include "../../functions.php";
require "../../../_utils/userDetect.php";
$user_id = $MQPA_NTLogin;

date_default_timezone_set('US/Eastern');

$claimID = $_POST['claimID'];
$cust_number = $_POST['cust_number'];
$cust_name = $_POST['cust_name'];
$cm_number = $_POST['cm_number'];
$ppref = $_POST['ppref'];
$vendor_debit = $_POST['vendor_debit'];
$processor = $_POST['processor'];
$status = $_POST['status'];
// $date_submitted_qa = $_POST['date_submitted_qa'];
$date_submitted_qa = date("Y-m-d H:i:s");
// $action_date = $_POST['action_date'];
$action_date = date("Y-m-d H:i:s");

// if ($date_submitted_qa == "" || $date_submitted_qa == null) {
//   $date_submitted_qa = "0000-00-00 00:00:00";
// }
// if ($action_date == "" || $action_date == null) {
//   $action_date = "0000-00-00 00:00:00";
// }

$cm_date_entered = date("Y-m-d H:i:s");

$query = "UPDATE tb_transaction SET transaction_customer_number='$cust_number', transaction_customer_name='$cust_name', transaction_credit_memo='$cm_number',
          transaction_credit_memo_created_date='$cm_date_entered', transaction_status='$status', transaction_processor='$processor',
          transaction_date_submitted_to_qa='$date_submitted_qa', transaction_action_date='$action_date', transaction_ppref='$ppref',
          transaction_vendor_debit_number='$vendor_debit' WHERE transaction_id='$claimID'";
$result = mysqli_query($con, $query);
$lastid = $claimID;

$statusDate = date("Y-m-d H:i:s");
$statusName = $status;
$statusBy = $MQPA_NTLogin;

$arrStatus = array (

  "sh_transaction_id",
  "sh_status",
  "sh_status_date",
  "sh_updated_by",

);

  $insertStatus = mysqli_prepare($con,
                      "INSERT INTO tb_status_history (".implode($arrStatus,",").")"
                      . "VALUES (?,?,?,?)");

    if ($statusName != null) {
      if ($insert === false) {

        ESEO_log($con);
        echo mysqli_error($con);
        mysqli_close($con);
        die ("::ERROR::QUERY");

      };

      $bind = mysqli_stmt_bind_param($insertStatus, "ssss",$lastid,$statusName,$statusDate,$statusBy);
      if ($bind === false) {
        echo mysqli_error($con);
          mysqli_close($con);
        die ("::ERROR::BIND");

      }
      $exec = mysqli_stmt_execute($insertStatus);
      if ($exec === false) {

        echo mysqli_error($con);
        mysqli_close($con);
        die ("::ERROR::EXEC");

      }
    }

echo $lastid;
?>
