<?php
include "../_libs/db_connect.php";

$filter = isset($_POST['filter']) ? $_POST['filter'] : '';
// echo "filter " . $_GET['filter']; transaction_urgent
switch ($filter) {
    case "new_claims":
        $condition = " WHERE transaction_status='New Request' AND header_portfolio_id='4' order by transaction_urgent DESC";
        break;
    case "pending_claims":
				$condition = " WHERE (transaction_status<>'Completed' AND transaction_status<>'New Request' AND header_portfolio_id='4') order by transaction_urgent DESC";
        break;
		case "with_findings":
				$condition = " WHERE transaction_status='With Findings' AND header_portfolio_id='4' order by transaction_urgent DESC";
        break;
		case "for_billing":
				$condition = " WHERE transaction_status='For Billing' AND header_portfolio_id='4' order by transaction_urgent DESC";
        break;
    default:
        $condition = "WHERE header_portfolio_id='4' and transaction_status IN ('New Request', 'For Billing', 'For Validation', 'Resubmitted', 'Billed') order by transaction_urgent DESC";
}

$query ="SELECT
		header_ticket_id AS ticket_id,
    header_description AS description,
    header_sap_proj_number AS sap_proj_number,
    header_requestor AS requestor,
    transaction_customer_number AS customer_number,
    transaction_customer_name AS customer_name,
		transaction_credit_memo AS credit_memo,
    transaction_amount AS amount,
    transaction_currency AS currency,
    (transaction_amount + transaction_admin_fee) AS total,
    transaction_status AS status,
    transaction_remarks AS qa_status,
    transaction_action_date AS action_date,
    transaction_urgent AS urgent
FROM tb_header AS h
LEFT JOIN tb_transaction AS t
ON h.header_ticket_id=t.transaction_ticket_id
$condition";
// echo $query;
$trs = "";
$result = mysqli_query($con, $query);
if ($result) {
	while($row = mysqli_fetch_assoc($result)) {
    $now = new DateTime();
    $actionDate = new DateTime($row['action_date']);
    $age = date_diff($actionDate,$now);
    if ($row['urgent'] == 1) {
      $trs = $trs . "<tr style='color:red;'><td><input id='group_check' type='checkbox' class='checkboxes group_checkbox' value='1' data-itemID = '" . $row['ticket_id'] . "' chk_data='" . $row['ticket_id'] . "' onchange='selectItem('single')'/></td>";
			$trs = $trs . "<td><b><a href='?portfolio=cpp&page=view_claim&requestID=" . $row['ticket_id'] . "' style='color:red;text-decoration:underline;'>" . $row['ticket_id'] . "</a></b></td>";
			$trs = $trs . "<td>". $row['customer_name'] . "</td>";
			$trs = $trs . "<td>". $row['description'] . "</td>";
			$trs = $trs . "<td>". $row['sap_proj_number'] . "</td>";
			$trs = $trs . "<td>". $row['currency'] . "</td>";
			$trs = $trs . "<td>". $row['amount'] . "</td>";
			// $trs = $trs . "<td>". $row['admin_fee'] . "</td>";
			$trs = $trs . "<td>". $row['total'] . "</td>";
			$trs = $trs . "<td>". $age->format('%R%a days') . "</td>";
			//$trs = $trs . "<td>". $row['credit_memo'] . "</td>";
      if ($row['qa_status']=="With Findings"){
        $trs = $trs . "<td style='color:red'>". $row['qa_status'] . "</td>";
      } else {
        $trs = $trs . "<td>". $row['qa_status'] . "</td>";
      }
			$trs = $trs . "<td>". $row['status'] . "</td></tr>";
    }else{
			$trs = $trs . "<tr><td><input id='group_check' type='checkbox' class='checkboxes group_checkbox' value='1' data-itemID = '" . $row['ticket_id'] . "' chk_data='" . $row['ticket_id'] . "' onchange='selectItem('single')'/></td>";
			$trs = $trs . "<td><b><a href='?portfolio=cpp&page=view_claim&requestID=" . $row['ticket_id'] . "' style='color:#2962FF;text-decoration:underline;'>" . $row['ticket_id'] . "</a></b></td>";
			$trs = $trs . "<td>". $row['customer_name'] . "</td>";
			$trs = $trs . "<td>". $row['description'] . "</td>";
			$trs = $trs . "<td>". $row['sap_proj_number'] . "</td>";
			$trs = $trs . "<td>". $row['currency'] . "</td>";
			$trs = $trs . "<td>". $row['amount'] . "</td>";
			// $trs = $trs . "<td>". $row['admin_fee'] . "</td>";
			$trs = $trs . "<td>". $row['total'] . "</td>";
			$trs = $trs . "<td>". $age->format('%R%a days') . "</td>";
			//$trs = $trs . "<td>". $row['credit_memo'] . "</td>";
      if ($row['qa_status']=="With Findings"){
        $trs = $trs . "<td style='color:red'>". $row['qa_status'] . "</td>";
      } else {
        $trs = $trs . "<td>". $row['qa_status'] . "</td>";
      }
			$trs = $trs . "<td>". $row['status'] . "</td></tr>";
    }
	}
}

echo $trs;
