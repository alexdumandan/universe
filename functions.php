<?php
/**
 * Time difference between tserver imestamp
 */

 function get_MySQL_Connection() {

   return mysqli_connect("pimawdev1001", "usesem00", "", "universe");

 }

function get_firstname($ntid){

    $con = get_MySQL_Connection();

    $ntid=str_replace(" ", "", $ntid);
    $user_id_query="SELECT tb_users.firstname AS fName "
            . "FROM  tb_users "
            . "WHERE ntid = '$ntid'";
    $user_id_query_result =  mysqli_query($con, $user_id_query);
    $user_id_row =  mysqli_fetch_assoc($user_id_query_result);

    return $user_id_row['fName'];

}

function get_lastname($ntid){
    $con = get_MySQL_Connection();
    $ntid=str_replace(" ", "", $ntid);
    $user_id_query="SELECT tb_users.lastname AS lName "
            . "FROM  tb_users "
            . "WHERE ntid = '$ntid'";
    $user_id_query_result =  mysqli_query($con, $user_id_query);
    $user_id_row =  mysqli_fetch_assoc($user_id_query_result);

    return $user_id_row['lName'];

}

function get_name($ntid,$con){
    if(!isset($con)){
      $con = get_MySQL_Connection();
    }

    $ntid=str_replace(" ", "", $ntid);
    $user_id_query="SELECT tb_users.full_name AS uName "
            . "FROM  tb_users "
            . "WHERE ntid = '$ntid'";
    $user_id_query_result =  mysqli_query($con, $user_id_query);
    $user_id_row =  mysqli_fetch_assoc($user_id_query_result);


    return $user_id_row['uName'];

}

function get_email($ntid,$con){
  if(!isset($con)){
    $con = get_MySQL_Connection();
  }
    $ntid=str_replace(" ", "", $ntid);
    $user_id_query="SELECT tb_users.email AS emailAdd "
            . "FROM  tb_users "
            . "WHERE ntid = '$ntid'";
    $user_id_query_result =  mysqli_query($con, $user_id_query);
    $user_id_row =  mysqli_fetch_assoc($user_id_query_result);


            return $user_id_row['emailAdd'];

}

function get_user_level ($ntid, $con){

  if(!isset($con)){
    $con = get_MySQL_Connection();
  }
    $level='Associate';

    $user_query="SELECT account_type FROM tb_users WHERE ntid='$ntid'";
    $user_res=  mysqli_query($con, $user_query);
    if ($user_row=  mysqli_fetch_assoc($user_res)){

      return $user_row['account_type'];
        $level=$user_row;

    }

     return $level;

}

function check_uploader ($ntid,$con){
  if(!isset($con)){
    $con = get_MySQL_Connection();
  }
    $level='No';

    $user_query="SELECT uploader_ntid FROM tb_uploader WHERE uploader_ntid='$ntid'";
    $user_res=  mysqli_query($con, $user_query);
    if ($user_row=  mysqli_fetch_assoc($user_res)){


        $level='Yes';

    }

     return $level;

}

function get_group_name ($ntid,$con){
  if(!isset($con)){
    $con = get_MySQL_Connection();
  }
    $level='SMB';


    $user_query="SELECT group_name FROM tb_users WHERE ntid='$ntid'";
    $user_res=  mysqli_query($con, $user_query);
    if ($user_row=  mysqli_fetch_assoc($user_res)){

      return $user_row['group_name'];
        $level=$user_row;
    }

     return $level;

}

function get_country ($ntid,$con){
  if(!isset($con)){
    $con = get_MySQL_Connection();
  }
    $country='US';


    $user_query="SELECT country FROM tb_users WHERE ntid='$ntid'";
    $user_res=  mysqli_query($con, $user_query);
    if ($user_row=  mysqli_fetch_assoc($user_res)){

      return $user_row['country'];
        $country=$user_row;

    }

     return $country;

}

function get_isnumber($ntid){
    $con = get_MySQL_Connection();
    $ntid=str_replace(" ", "", $ntid);
    $user_id_query="SELECT tb_users.is_number AS isNumber "
            . "FROM  tb_users "
            . "WHERE ntid = '$ntid'";
    $user_id_query_result =  mysqli_query($con, $user_id_query);
    $user_id_row =  mysqli_fetch_assoc($user_id_query_result);


    return $user_id_row['isNumber'];

}



function clean_sql_input($string){


    $string = str_replace("'","''",$string);
    $string = str_replace('"','\"',$string);
    return $string ;
}




function isJSON($string) {
     json_decode($string);
    if (json_last_error()===JSON_ERROR_NONE){

        return true;
    }

    return false;
}

function get_userDetailsByIS($IS,$country){

    $con = get_MySQL_Connection();
    $IS=str_replace(" ", "", $IS);
    $details  = array();
    $user_id_query="SELECT * "
            . "FROM  tb_users "
            . "WHERE is_number = '$IS'  AND country = '$country' AND user_status = 'Active'";

    $user_id_query_result =  mysqli_query($con, $user_id_query);
    $user_id_row =  mysqli_fetch_assoc($user_id_query_result);

    if (count($user_id_row)>0){
      return $user_id_row;
    }else{
      return null;
    }

}

function get_orderDetailsFromPreviousItem($orderNumber,$accountNumber,$fields="*"){

      $con = get_MySQL_Connection();
      // if(!isset($fields)){
      //   $fields = "*";
      // }
      $query="SELECT $fields "
              . "FROM  tb_orders "
              . "WHERE order_account_number = '$accountNumber' and order_SO_number = '$orderNumber' order by order_id desc limit 1;";

      $result =  mysqli_query($con, $query);

      $row =  mysqli_fetch_assoc($result);

      if (count($row)>0){
        return $row;
      }else{
        return null;
      }


}

function get_orderDetailsFromPreviousItemForCanada($orderNumber,$accountNumber,$fields="*"){

      $con = get_MySQL_Connection();
      // if(!isset($fields)){
      //   $fields = "*";
      // }
      $query="SELECT $fields "
              . "FROM  tb_open_orders "
              . "WHERE openorder_br_cust_nbr = '$accountNumber' and openorder_order_number = '$orderNumber' order by openorder_id desc limit 1;";

      $result =  mysqli_query($con, $query);

      $row =  mysqli_fetch_assoc($result);

      if (count($row)>0){
        return $row;
      }else{
        return null;
      }

}

function get_userDetailsByNTID($NTID){

  $con = get_MySQL_Connection();
  $ntid=str_replace(" ", "", $NTID);
  $query="SELECT * "
          . "FROM  tb_users "
          . "WHERE ntid = '$NTID' ";
  $result =  mysqli_query($con, $query);
  $user_id_row =  mysqli_fetch_assoc($result);

// AND  user_status = 'Active'
  return $user_id_row;

}

?>
