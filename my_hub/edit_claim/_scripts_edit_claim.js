var attachmentCount = 0;
var requestDetails;
var claimDetails;
var status;
var G_USERDETAILS;
$(document).ready(function(){
  biFrost.init({
    "maxFilesizeMB" : 25,
    "success" : updateRequest,
    "error": errorUpload
  });
  $("#page_content").load('my_hub/edit_claim/html_edit_claim.php',function(){
    getUserDetails();
    $("#date_submitted_qa,#billing_date,#action_date").datepicker();
    getRequestDetails(getParameterByName("requestID"));

  });
});


function getUserDetails(){

  $.ajax({
    type: "GET",
    url: "json_getUserDetails.php",
    data: {},
    success: function(data){
      G_USERDETAILS = JSON.parse(data);

      console.log(G_USERDETAILS);
    }
  })
}


function getRequestDetails(requestID){

  $.ajax({
    type: "GET",
    url: "my_hub/edit_claim/json_getRequestDetails.php",
    data: {requestID:requestID},
    success: function(data){

      requestDetails = JSON.parse(data);

      $("#ticket_id").html(requestDetails.ticket_id);
      $("#description").html(requestDetails.description);
      $("#sap_proj_number").html(requestDetails.sap_proj_number);
      $("#date_received").html(requestDetails.date_received);
      $("#requestor").html(requestDetails.requestor);
      $("#date_entered").html(requestDetails.date_entered);

      displayAttachments(requestID);
      displayComments(requestID);
      displayClaimDetails(requestID);
    }
  });

  $("#save_claim_btn").click(function(){
    removeFileInputsWithoutFile();

    if($(".file-attachments").length > 0){
      console.log("bifrost");
      biFrost.upload();
    }else{
      console.log("submit");
      updateRequest(null);
    }
  });
<<<<<<< HEAD
  location.href='#my_hub_open';
=======
<<<<<<< HEAD
  location.href='#my_hub_open';
=======

>>>>>>> 3d8d4d6c3400a176f42bc18d22434507c8ce91ed
>>>>>>> 48ad8ce1167930e1137c3691f5e24b63fe4cf170
}

function updateRequest(files){
  var requestID = getParameterByName("requestID");
  // console.log("save claim btn" + requestID);

  var requestData = new Object();

  requestData.requestID = requestID;
  requestData.comment = $("#comment").val();
  requestData.attachments = files;

  $.ajax({
    type: "POST",
    url: "my_hub/edit_claim/updatePafRequest.php",
    data: requestData,
    success: function(result){
      console.log(result);

        if (result.indexOf("NO ACCESS")>-1) {

        }else if (result.indexOf("::ERROR::")<0) {
          alert("Claim Request ID : " + result + " updated.");
          // location.href='?portfolio='+getParameterByName("portfolio");
        }else{

        }
    },
    error:function(res){

    }
  });

}

function upDateClaimRow(claimID){
  $("#updateClaimRow_btn").button('loading');

  var cm_number = $('#cm_number'+claimID).val();
  var processor = $('#claim_processor'+claimID).val();
  var status = $('#claim_status'+claimID).val();
  var date_submitted_qa = $('#date_submitted_qa'+claimID).val();
  var action_date = $('#action_date'+claimID).val();

  $.ajax({
    type: "POST",
    url: "my_hub/edit_claim/updateClaimRequest.php",
    data: {claimID:claimID,cm_number:cm_number,processor:processor,
          status:status,date_submitted_qa:date_submitted_qa,action_date:action_date},
    success: function(result) {
      console.log(result);
      if (result.indexOf("::ERROR::")<0) {
        toastr["success"]("Item updated!", "");
        // sendDetailstoBilling(result);
      }
    },
    complete:function(){
      $("#updateClaimRow_btn").button('reset');
    }
  })
}

function displayClaimDetails(requestID){

  $.ajax({
    type: "GET",
    url: "my_hub/edit_claim/json_getClaimDetails.php",
    data: {requestID:requestID},
    success: function(data){

      claimDetails = JSON.parse(data);
      tb = "";
      tb_total = "";
      total_claim = 0;
      var transaction_id;

      if (claimDetails.length < 1) {
        tb = "<tr>";
        tb = "<td colspan='14'>";
        tb += "<div class='list-group-item item-note'>";
        tb += "<div class='item-note-data' style='text-align:center;'><i class='material-icons' style='font-size:60px;'>payment</i>";
        tb += "<span class='item-note-datetime'></span>";
        tb += "</div>";
        tb += "<div class='item-note-content' style='text-align:center;font-size:18px;'>No claim details found for this item.</div>";
        tb += "</div>";
        tb += "</td>";
        tb += "</tr>";

        $("#claim_fields").html(tb);
      }

      $.each(claimDetails, function(i, o){
        status = o.status;
        transaction_id = o.transaction_id;
        if (o.admin_fee == null || o.admin_fee == "") {
          o.admin_fee = 0;
        }
        if (o.date_submitted_to_qa == "0000-00-00 00:00:00" || o.date_submitted_to_qa == null) {
          o.date_submitted_to_qa = "0000-00-00";
        }
        if (o.action_date == "0000-00-00 00:00:00" || o.action_date == null) {
          o.action_date = "0000-00-00";
        }
        if (o.billing_date == "0000-00-00 00:00:00" || o.billing_date == null) {
          o.billing_date = "0000-00-00";
        }

        if (status == "Billed") {
          tb += "<tr>";
          tb += "<td >"+o.customer_number+"</td>";
          tb += "<td >"+o.customer_name+"</td>";
          tb += "<td>$ "+o.amount+"</td>";
          tb += "<td>$ "+o.admin_fee+"</td>";
          tb += "<td style='width:100px;'><input class='cm-number ' id='cm_number"+o.transaction_id+"' type='text' style='width:100px;' pattern='-?[0-9]*(\.[0-9]+)?' hidden>"+o.credit_memo+"</td>";
          tb += "<td>"+o.processor+"</td>";
          tb += "<td>"+o.status+"</td>";
          tb += "<td>"+o.billing_date+"</td>";
          tb += "</tr>";
<<<<<<< HEAD
        }
        else if (status == "For Billing") {
=======
<<<<<<< HEAD
        }
        else if (status == "For Billing") {
=======
        }else if (status == "For Billing") {
>>>>>>> 3d8d4d6c3400a176f42bc18d22434507c8ce91ed
>>>>>>> 48ad8ce1167930e1137c3691f5e24b63fe4cf170
          tb += "<tr>";
          tb += "<td >"+o.customer_number+"</td>";
          tb += "<td >"+o.customer_name+"</td>";
          tb += "<td>$ "+o.amount+"</td>";
          tb += "<td>$ "+o.admin_fee+"</td>";
          tb += "<td style='width:100px;'><input class='cm-number ' id='cm_number"+o.transaction_id+"' type='text' style='width:100px;' pattern='-?[0-9]*(\.[0-9]+)?' hidden>"+o.credit_memo+"</td>";
          tb += "<td>"+o.processor+"</td>";
          tb += "<td>"+o.status+"</td>";
          tb += "<td>"+o.date_submitted_to_qa+"</td>";
          tb += "</tr>";
<<<<<<< HEAD
        }
        else {
=======
<<<<<<< HEAD
        }
        else {
=======
        }else {
>>>>>>> 3d8d4d6c3400a176f42bc18d22434507c8ce91ed
>>>>>>> 48ad8ce1167930e1137c3691f5e24b63fe4cf170
          tb += "<tr>";
          tb += "<td >"+o.customer_number+"</td>";
          tb += "<td >"+o.customer_name+"</td>";
          tb += "<td>$ "+o.amount+"</td>";
          tb += "<td>$ "+o.admin_fee+"</td>";
          tb += "<td style='width:100px;'><input class='cm-number ' id='cm_number"+o.transaction_id+"' type='text' style='width:100px;' pattern='-?[0-9]*(\.[0-9]+)?' hidden>"+o.credit_memo+"</td>";
          tb += "<td>"+o.processor+"</td>";
          tb += "<td>"+o.status+"</td>";
          tb += "<td>"+o.action_date+"</td>";
          tb += "</tr>";
        }


        total_claim = total_claim + o.total;

      });
      $("#claim_fields").append(tb);
        tb = "";
        tb_total += tb;
        tb_total += "<tr>";
        tb_total += "<td colspan='2'>TOTAL CLAIM</td>";
        tb_total += "<td colspan='2' style='font-weight:bold;'>$ "+total_claim.toLocaleString()+"</td>";
        tb_total += "<td colspan='4'></td>";
        tb_total += "</tr>";

      $("#claim_fields").append(tb_total);

      checkStatus(status);
      // $("#date_submitted_qa,#action_date").datepicker();
    }
  });
}

function checkStatus(status){
  if (status == "Billed") {
    $("#other_details").hide();
    $("#save_claim_btn").hide();
  }else {
    $("#other_details").show();
  }
}

function displayComments(requestID){
  $.ajax({
    type: "GET",
    url: "my_hub/edit_claim/json_getComments.php",
    data: {requestID:requestID},
    success: function(data){

      var comment = JSON.parse(data);
      var list = "";
      // console.log(comment.length);

      if (comment.length < 1) {
        // console.log("no comment");

        list += "<div class='list-group-item item-note'>";
        list += "<div class='item-note-data' style='text-align:center;'><i class='material-icons' style='font-size:60px;'>note</i>";
        list += "<span class='item-note-datetime'></span>";
        list += "</div>";
        list += "<div class='item-note-content' style='text-align:center;font-size:18px;'>No comment found for this item.</div>";
        list += "</div>";

        $("#comments_history_list").html(list);
      }

      $.each(comment, function(i, c){

        list += "<div class='list-group-item item-note'>";
        list += "<div class='item-note-data'>"+c.entered_by;
        list += "<span class='item-note-datetime'>"+$.timeago(c.entry_date)+"</span>";
        list += "</div>";
        list += "<div class='item-note-content'>"+c.comment_history+"</div>";
        list += "</div>";

        $("#comments_history_list").html(list);
      });

    }
  });

}

function displayAttachments(requestID){
  $.ajax({
    type:"GET",
    url: "my_hub/edit_claim/json_getAttachments.php",
    data: {requestID:requestID},
    success: function(data){

      var attach = JSON.parse(data);
      // console.log(attach);
      var div = "";

      if (attach.length < 1) {
        // console.log("no attachment");
        div += "<div class='list-group-item item-note'>";
        div += "<div class='item-note-data' style='text-align:center;'><i class='material-icons' style='font-size:60px;'>attach_file</i>";
        div += "<span class='item-note-datetime'></span>";
        div += "</div>";
        div += "<div class='item-note-content' style='text-align:center;font-size:18px;'>No attachment found for this item.</div>";
        div += "</div>";
        // $("#attachment_history").append(div);
      }
      $.each(attach,function(i,o){
        console.log(o);
        var filename = o["link"];
        var n = filename.lastIndexOf("/");
        var len = filename.length;
        var fname = filename.substring(n+1, len);

        div += "<div class='list-group-item item-note'>";
        div += "<div class='item-note-data'>"+o.uploaded_by;
        div += "<span class='item-note-datetime'>"+$.timeago(o.uploaded_date)+"</span>";
        div += "</div>";
        div += "<div class='item-note-content'><a href='"+o["link"]+"' target='_blank' style='color:#40c4ff;font-size:16px;'><i class='material-icons' style='font-size:20px;'>file_download</i>"+fname+"</a></div>";
        div += "</div>";



      });
      $("#attachment_history").append(div);
    }
  });
}

function addAttachment(){
  var newAttachment = "";

  newAttachment +="<div class='col-md-4 attach-file-group'>";
  newAttachment +="	<div class='input-group'>";
  newAttachment +=" 		<input type='file' class='form-control file-attachments' id='file_"+attachmentCount+"'>";
  newAttachment +="		<span class='input-group-btn'>";
  newAttachment +="			<button class='btn red delete-attachment' type='button'  onclick=\"removeAttachment(this)\"><i class='material-icons'>remove</i> </button>";
  newAttachment +=" 		</span>";
  newAttachment +="	</div>";
  newAttachment +="</div>";

  attachmentCount++;

  $("#file_attachment_group").append(newAttachment);

}

function removeAttachment(element){

  $(element).parent().parent().parent().remove();

}

function errorUpload(){

  console.log("error on upload");
}

function removeFileInputsWithoutFile(){


  $.each($(".file-attachments"),function(i,o){

    if($(o).val()==""){
      $(o).parents(".col-md-4").remove();
    }

  });

}
