<?php
require "../../../EULA/API.php";
include '../../_libs/db_connect.php';
require "../../../_Utils/userDetect.php";
require "../../../ESEO/V1/API.php";

date_default_timezone_set('US/Eastern');

$date = date("Y-m-d H:i:s");

function returnZeroIfString($input){

  if(is_numeric($input)){
    return $input;
  }else{
    return "0";
  }

}

$requestID = $_POST["requestID"];
$status = "Resubmitted";
$action_date = date("Y-m-d H:i:s");

$updateTransaction = "UPDATE tb_transaction SET transaction_status='$status', transaction_action_date='$action_date'
                      WHERE transaction_ticket_id='$requestID'";
$resultTransaction = mysqli_query($con, $updateTransaction);


$attachDate = $date;
$attachBy = $MQPA_NTLogin;

$arrAttach = array (

  "attach_ticket_id",
  "attach_filename",
  "attach_links",
  "attach_uploaded_by",
  "attach_date",

);

$insertAttachment = mysqli_prepare($con,
                    "INSERT INTO tb_attachments (".implode($arrAttach,",").")"
                    . "VALUES (?,?,?,?,?)");

$i = 0;

if ($_POST["attachments"] != null) {
  foreach($_POST["attachments"]["fileNames"] as $fileLocation){

    $link = $fileLocation;
    $fileName = $_POST["attachments"]["fileNamesOrig"][$i];
    $i++;

    $bind = mysqli_stmt_bind_param($insertAttachment, "sssss",$requestID,$fileName,$link,$attachBy,$attachDate);
    if ($bind === false) {
      echo mysqli_error($con);
        mysqli_close($con);
      die ("::ERROR::BIND");

    }
    $exec = mysqli_stmt_execute($insertAttachment);
    if ($exec === false) {
      echo mysqli_error($con);
        mysqli_close($con);
      die ("::ERROR::BIND");

    }

  }
}

$commentDate = $date;
$commentBy = $MQPA_NTLogin;
$commentData = $_POST["comment"];

$arrComments = array (

  "comment_ticket_id",
  "comment_entry_date",
  "comment_history",
  "comment_entered_by",

);

$insertComment = mysqli_prepare($con,
                  "INSERT INTO tb_comments_history (".implode($arrComments,",").")"
                  . "VALUES (?,?,?,?)");

if ($_POST["comment"] != null) {

  if ($insertComment === false) {

    ESEO_log($con);
    echo mysqli_error($con);
    mysqli_close($con);
    die ("::ERROR::QUERY");

  };

  $bind = mysqli_stmt_bind_param($insertComment, "ssss",$requestID,$commentDate,$commentData,$commentBy);
  if ($bind === false) {
    echo mysqli_error($con);
      mysqli_close($con);
    die ("::ERROR::BIND");

  }
  $exec = mysqli_stmt_execute($insertComment);
  if ($exec === false) {

    echo mysqli_error($con);
    mysqli_close($con);
    die ("::ERROR::EXEC");

  }

}

echo $requestID;

mysqli_close($con);

?>
