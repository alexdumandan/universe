<table class="table" id="my_hub_pending_table">

  <thead>
    <th class="table-checkbox"><input id="group_check" type="checkbox" class="group-checkable" /></th>
    <th>Ticket ID</th>
    <th>Portfolio</th>
    <th>Customer Name</th>
    <th>Description</th>
    <th>SAP Project Number</th>
    <th>Amount</th>
    <th>Admin Fee</th>
    <th>Total</th>
    <th>Age</th>
    <!-- <th>Credit Memo</th> -->
    <th>QA Status</th>
    <th>Claim Status</th>
  </thead>
  <thead id="my_hub_pending_table_col_filter">
     <tr>
       <th></th>
       <th>Ticket ID</th>
       <th>Portfolio</th>
       <th>Customer Name</th>
       <th>Description</th>
       <th>SAP Project Number</th>
       <th>Amount</th>
       <th>Admin Fee</th>
       <th>Total</th>
       <th>Age</th>
       <!-- <th>Credit Memo</th> -->
       <th>QA Status</th>
       <th>Claim Status</th>
     </tr>
 </thead>
  <tbody id="my_hub_pending_table_body">

  </tbody>
</table>
