<?php

 ?>
 <h3 class="page-title" id="page_title">
   My Hub
 </h3>

 <div class="tabbable-line ">
   <ul class="nav nav-tabs">
     <li class="my-hub">
       <a href="#my_hub_prof" data-toggle="tab" aria-expanded="true">
       My Profile </a>
     </li>
     <li class="my-hub active">
       <a href="#my_hub_open" data-toggle="tab" aria-expanded="false">
       Claims </a>
     </li>
     <li class="my-hub">
       <a href="#my_hub_inquiries" data-toggle="tab" aria-expanded="false">
       Inquiries </a>
     </li>

   </ul>

   <div class="tab-content">

     <div class="tab-pane" id="my_hub_prof">

       <div class="row" >
         <div class="col-md-12">
           <div class="profile-sidebar">
             <div class="portlet light profile-sidebar-portlet">
               <div class="profile-userpic font-blue-madison" id="profile_avatar" style="cursor:pointer;">
                 <!-- <i class="material-icons">account_circle</i> -->
                 <img id="profile_avatar_img" src="../_Utils/php/avatars/IM_avatars/default.png" class="img-responsive" alt="Create your avatar">
               </div>

               <div class="profile-usertitle">
                 <div class="profile-usertitle-name" id="profile_name">
 									 <!-- Alex Dumandan -->
   								</div>
   								<div class="profile-usertitle-job" id="title_group">
   									 <!-- Admin -->
   								</div>
               </div>

               <div class="profile-userbuttons" id="activate_alerts">
   								<button type="button" id="activate" class="btn btn-circle green-haze btn-sm">Activate Alerts</button>
   								<button type="button" id="deactivate" class="btn btn-circle red-flamingo btn-sm">Deactivate Alerts</button>
							 </div>

               <div class="profile-usermenu">
 								<ul class="nav">
 									<li class="active" id="myhub_overview">
 										<a onclick="myHubOverview()">
 										<i class="icon-home"></i>
 										Overview </a>
 									</li>
 									<!-- <li class="" id="myhub_admin">
 										<a onclick="myHubAdmin()">
 										<i class="icon-settings"></i>
 										Admin </a>
 									</li> -->

 								</ul>
 							</div>

             </div>
           </div>

           <div class="profile-content">
             <div class="row">
               <div class="col-md-12">
                 <div class="portlet light" id="profile_portlet">
                   <div class="portlet-title tabbable-line">
 										<div class="caption caption-md">
 											<i class="icon-globe theme-font hide"></i>
 											<!-- <span class="caption-subject font-blue-madison bold uppercase">Profile Account</span> -->
 										</div>
 										<ul class="nav nav-tabs">
 											<li class="active">
 												<a href="#tab_1_1" data-toggle="tab">Personal Info</a>
 											</li>
 										</ul>
 									</div>

                  <div class="portlet-body">
										<div class="tab-content">
											<!-- PERSONAL INFO TAB -->
											<div class="tab-pane active" id="tab_1_1">
                        <table class="table" id="profile_table_info">
                          <tr>
                            <td>First Name</td><td id="profile_fname">Alex </td>
                          </tr>
                          <tr>
                            <td>Last Name</td><td id="profile_lname">Dumandan</td>
                          </tr>
                          <tr>
                            <td>NTID</td><td id="profile_ntid">usduma00</td>
                          </tr>
                          <tr>
                            <td>Portfolio</td><td id="profile_portfolio">PAF</td>
                          </tr>
                          <tr>
                            <td>Access/Account</td><td id="profile_account">Admin</td>
                          </tr>
                          <tr>
                            <td>Email</td><td id="profile_email">Alexandra.Dumandan@ingrammicro.com
                              <!-- <i class="fa fa-envelope"></i> -->
                            <!-- <a href="mailto:Alexandra.Dumandan@ingrammicro.com" style="color:blue;"></a> -->
                          </td>
                          </tr>
                          <!-- <tr>
                            <td>Signature</td><td id="signature"><a class="btn btn-sm btn-circle purple-seance" onclick="checkUserSignature()">Set E-mail Signature</a></td>
                          </tr> -->
                          <!-- <tr>
                            <td>Team Email</td><td id="team_email">

                            </td>
                          </tr> -->
                        </table>
											</div>
											<!-- END PERSONAL INFO TAB -->
										</div>
									</div>

                 </div>
               </div>
             </div>
           </div> <!-- profile-content -->

         </div>
       </div>

     </div><!-- my_hub_prof -->

     <div class="tab-pane active" id="my_hub_open">

<<<<<<< HEAD
       <div class="row" >
         <div class="col-md-3" id="row_my_pending_claims">
=======
<<<<<<< HEAD
       <div class="row" >
         <div class="col-md-3" id="row_my_pending_claims">
=======
       <div class="row" id="my_hub_status">

         <div class="col-md-2" id="row_my_pending_claims">
>>>>>>> 3d8d4d6c3400a176f42bc18d22434507c8ce91ed
>>>>>>> 48ad8ce1167930e1137c3691f5e24b63fe4cf170
           <div class="dashboard-stat green">
             <div class="visual">
               <i class="fa fa-dollar"></i>
             </div>
             <div class="details">
               <div class="number" id="my_pending_claims">0</div>
               <div class="desc">
<<<<<<< HEAD
                 Pending Claims That I Requested
=======
<<<<<<< HEAD
                 Pending Claims That I Requested
=======
                 My Pending Claims
>>>>>>> 3d8d4d6c3400a176f42bc18d22434507c8ce91ed
>>>>>>> 48ad8ce1167930e1137c3691f5e24b63fe4cf170
               </div>
             </div>
             <a class="more" href="javascript:;" style="cursor:context-menu" data="my_pending_claims">
             View list <i class="m-icon-swapright m-icon-white"></i>
             &nbsp;
             </a>
           </div>
         </div>
<<<<<<< HEAD

         <div class="col-md-3" id="row_pending_claims_atm">
=======
<<<<<<< HEAD

         <div class="col-md-3" id="row_pending_claims_atm">
=======
         <div class="col-md-2" id="row_my_pending_inquiries">
>>>>>>> 3d8d4d6c3400a176f42bc18d22434507c8ce91ed
>>>>>>> 48ad8ce1167930e1137c3691f5e24b63fe4cf170
           <div class="dashboard-stat yellow">
             <div class="visual">
               <i class="fa fa-dollar"></i>
             </div>
             <div class="details">
<<<<<<< HEAD
               <div class="number" id="pending_claims_atm">0</div>
               <div class="desc">
                  Pending Claims Assigned To Me
               </div>
             </div>
             <a class="more" href="javascript:;" style="cursor:context-menu" data="pending_claims_atm">
=======
<<<<<<< HEAD
=======
               <div class="number" id="my_pending_inquiries">0</div>
               <div class="desc">
                 My Pending Inquiries
               </div>
             </div>
             <a class="more" href="javascript:;" style="cursor:context-menu" data="my_pending_inquiries">
>>>>>>> 48ad8ce1167930e1137c3691f5e24b63fe4cf170
             View list <i class="m-icon-swapright m-icon-white"></i>
             &nbsp;
             </a>
           </div>
         </div>

<<<<<<< HEAD
         <div class="col-md-3" id="row_for_billing">
           <div class="dashboard-stat blue">
=======
         <div class="col-md-2" id="row_pending_claims_atm">
           <div class="dashboard-stat green">
>>>>>>> 48ad8ce1167930e1137c3691f5e24b63fe4cf170
             <div class="visual">
               <i class="fa fa-dollar"></i>
             </div>
             <div class="details">
<<<<<<< HEAD
               <div class="number" id="for_billing">0</div>
               <div class="desc">
                  For Billing
               </div>
             </div>
             <a class="more" href="javascript:;" style="cursor:context-menu" data="for_billing">
=======
>>>>>>> 3d8d4d6c3400a176f42bc18d22434507c8ce91ed
               <div class="number" id="pending_claims_atm">0</div>
               <div class="desc">
                  Pending Claims Assigned To Me
               </div>
             </div>
             <a class="more" href="javascript:;" style="cursor:context-menu" data="pending_claims_atm">
>>>>>>> 48ad8ce1167930e1137c3691f5e24b63fe4cf170
             View list <i class="m-icon-swapright m-icon-white"></i>
             &nbsp;
             </a>
           </div>
         </div>
<<<<<<< HEAD

         <div class="col-md-3"  id="row_for_approval">
           <div class="dashboard-stat green">
=======
<<<<<<< HEAD

         <div class="col-md-3" id="row_for_billing">
           <div class="dashboard-stat blue">
=======
         <div class="col-md-2" id="row_pending_inquiries_atm">
           <div class="dashboard-stat yellow">
             <div class="visual">
               <i class="fa fa-dollar"></i>
             </div>
             <div class="details">
               <div class="number" id="pending_inquiries_atm">0</div>
               <div class="desc">
                  Pending Inquiries Assigned To Me
               </div>
             </div>
             <a class="more" href="javascript:;" style="cursor:context-menu" data="pending_inquiries_atm">
             View list <i class="m-icon-swapright m-icon-white"></i>
             &nbsp;
             </a>
           </div>
         </div>

         <div class="col-md-2" id="row_for_billing">
           <div class="dashboard-stat red">
>>>>>>> 3d8d4d6c3400a176f42bc18d22434507c8ce91ed
>>>>>>> 48ad8ce1167930e1137c3691f5e24b63fe4cf170
             <div class="visual">
               <i class="fa fa-dollar"></i>
             </div>
             <div class="details">
               <div class="number" id="for_approval">0</div>
               <div class="desc">
                  For Billing
               </div>
             </div>
             <a class="more" href="javascript:;" style="cursor:context-menu" data="for_billing">
             View list <i class="m-icon-swapright m-icon-white"></i>
             &nbsp;
             </a>
           </div>
         </div>
<<<<<<< HEAD

         <div class="col-md-3"  id="row_for_approval">
           <div class="dashboard-stat green">
=======
         <div class="col-md-2"  id="row_for_approval">
           <div class="dashboard-stat blue">
>>>>>>> 3d8d4d6c3400a176f42bc18d22434507c8ce91ed
             <div class="visual">
               <i class="fa fa-dollar"></i>
             </div>
             <div class="details">
               <div class="number" id="for_approval">0</div>
               <div class="desc">
                  For My Approval
               </div>
             </div>
             <a class="more" href="javascript:;" style="cursor:context-menu" data="for_approval">
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> 48ad8ce1167930e1137c3691f5e24b63fe4cf170
             View list <i class="m-icon-swapright m-icon-white"></i>
             &nbsp;
             </a>
           </div>
         </div>

         <div class="col-md-3" id="row_completed_claims">
           <div class="dashboard-stat red">
             <div class="visual">
               <i class="fa fa-dollar"></i>
             </div>
             <div class="details">
               <div class="number" id="completed_claims">0</div>
               <div class="desc">
                  My Completed Claims
               </div>
             </div>
             <a class="more" href="javascript:;" style="cursor:context-menu" data="completed_claims">
             View list <i class="m-icon-swapright m-icon-white"></i>
             &nbsp;
             </a>
           </div>
         </div>

      </div>

      <div id="table_container" class="table-container">

      </div>

     </div><!-- tab-content -->

     <div class="tab-pane " id="my_hub_inquiries">

       <div class="row" >
         <div class="col-md-3" id="row_my_pending_inquiries">
           <div class="dashboard-stat green">
             <div class="visual">
               <i class="fa fa-dollar"></i>
             </div>
             <div class="details">
               <div class="number" id="my_pending_inquiries">0</div>
               <div class="desc">
                 My Pending Inquiries
               </div>
             </div>
             <a class="more" href="javascript:;" style="cursor:context-menu" data="my_pending_inquiries">
             View list <i class="m-icon-swapright m-icon-white"></i>
             &nbsp;
             </a>
           </div>
         </div>

         <div class="col-md-3" id="row_pending_inquiries_atm">
           <div class="dashboard-stat yellow">
             <div class="visual">
               <i class="fa fa-dollar"></i>
             </div>
             <div class="details">
               <div class="number" id="pending_inquiries_atm">0</div>
               <div class="desc">
                Pending Inquiries Assigned To Me
               </div>
             </div>
             <a class="more" href="javascript:;" style="cursor:context-menu" data="pending_inquiries_atm">
             View list <i class="m-icon-swapright m-icon-white"></i>
             &nbsp;
             </a>
           </div>
         </div>

         <div class="col-md-3" id="row_completed_inquiries">
           <div class="dashboard-stat red">
             <div class="visual">
               <i class="fa fa-dollar"></i>
             </div>
             <div class="details">
               <div class="number" id="completed_inquiries">0</div>
               <div class="desc">
                  Completed Inquiries
               </div>
             </div>
             <a class="more" href="javascript:;" style="cursor:context-menu" data="completed_inquiries">
<<<<<<< HEAD
=======
=======
>>>>>>> 3d8d4d6c3400a176f42bc18d22434507c8ce91ed
>>>>>>> 48ad8ce1167930e1137c3691f5e24b63fe4cf170
             View list <i class="m-icon-swapright m-icon-white"></i>
             &nbsp;
             </a>
           </div>
         </div>

      </div>

      <div id="table_container" class="table-container">

      </div>

     </div><!-- tab-content -->

     <div class="tab-pane" id="my_hub_inquiries">

     </div>

   </div> <!-- tab-content -->


 </div>


 <div id="modalAvatar" class="modal" tabindex="-1" data-backdrop="static" aria-hidden="false" >
   <div class="modal-dialog">
     <div class="modal-content" >
       <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
         <h2 class="modal-title">Create Avatar</h2>
       </div>
       <div class="modal-body">
         <div id="svgAvatars"></div>
       </div>


       </div>
     </div>
 </div>
