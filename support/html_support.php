<h3 class="page-title" id="page_title">
  Support <span id="groupName"></span>
    <br>
</h3>



<div class="panel panel-default">

	<div class="panel-body">
    <h4 class="suppot-head">For assistance and issue/s on the site, please go to following link:</h4>
    <h4 ><a class="support-link" href="http://pimawweb1001/qpa_portal/?adhoc=solutions" target="_blank">QP&amp;A Portal Solutions Page</a></h4>
    <h4 class="support-subhead">Select others in the Web Application dropdown</h4>
    <h4 class="support-subhead">Other contacts:</h4>

    <div class="list-group">
				<a href="mailto:MSSCBPITeam@ingrammicro.com?subject=UNIVERSE" class="list-group-item">
				<h4 class="list-group-item-heading"><i class="fa fa-user"></i> BPI Team</h4>
				</a>
				<a href="mailto:MNLQP_AProcessSol?subject=UNIVERSE" class="list-group-item">
				<h4 class="list-group-item-heading"><i class="fa fa-user"></i> Process Solutions Team</h4>
				</a>
				<a href="mailto:Katherine.Ibojos@ingrammicro.com?subject=UNIVERSE" class="list-group-item">
				<h4 class="list-group-item-heading"><i class="fa fa-user"></i> Katherine Ibojos</h4>
				<p class="list-group-item-text">
				   BPI Manager
				</p>
				</a>
				<a href="mailto:Argin.Lerit@ingrammicro.com?subject=UNIVERSE" class="list-group-item">
				<h4 class="list-group-item-heading"><i class="fa fa-user"></i> Argin Lerit</h4>
				<p class="list-group-item-text">
					 Business Solutions Lead
				</p>
				</a>
				<a href="mailto:MariaAngelita.Carlos@ingrammicro.com?subject=UNIVERSE" class="list-group-item">
				<h4 class="list-group-item-heading"><i class="fa fa-user"></i> Maria Angelita Carlos</h4>
				<p class="list-group-item-text">
					 BPI Specialist/Project Lead
				</p>
				</a>
        <a href="mailto:Noel.Cuevas@ingrammicro.com?subject=UNIVERSE" class="list-group-item">
				<h4 class="list-group-item-heading"><i class="fa fa-user"></i> Noel Cuevas</h4>
				<p class="list-group-item-text">
					 Bus &amp; Systems Analyst
				</p>
				</a>
				<a href="mailto:Alexandra.Dumandan@ingrammicro.com?subject=UNIVERSE" class="list-group-item">
				<h4 class="list-group-item-heading"><i class="fa fa-user"></i> Alexandra Dumandan</h4>
				<p class="list-group-item-text">
					 Bus &amp; Systems Analyst
				</p>
				</a>

			</div>
	</div>
</div>
