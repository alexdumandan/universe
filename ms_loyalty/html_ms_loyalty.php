<?php


 ?>
 <div class="row">
   <div class="col-md-12">
     <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
       <div class="dashboard-stat green">
         <div class="visual">
           <i class="fa fa-dollar"></i>
         </div>
         <div class="details">
           <div class="number" id="new_claims_count">0</div>
           <div class="desc">
              New Claims
           </div>
         </div>
         <a class="more" href="javascript:;" style="cursor:context-menu" data="new_claims">
         View list <i class="m-icon-swapright m-icon-white"></i>
         &nbsp;
         </a>
       </div>
     </div>
     <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
       <div class="dashboard-stat yellow">
         <div class="visual">
           <i class="fa fa-dollar"></i>
         </div>
         <div class="details">
           <div class="number" id="pending_claims_count">0</div>
           <div class="desc">
             WIP Claims
           </div>
         </div>
         <a class="more" href="javascript:;" style="cursor:context-menu" data="pending_claims">
         View list <i class="m-icon-swapright m-icon-white"></i>
         &nbsp;
         </a>
       </div>
     </div>
     <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
       <div class="dashboard-stat red">
         <div class="visual">
           <i class="fa fa-dollar"></i>
         </div>
         <div class="details">
           <div class="number" id="with_findings_count">0</div>
           <div class="desc">
              Total Claims With Findings
           </div>
         </div>
         <a class="more" href="javascript:;" style="cursor:context-menu" data="with_findings">
         View list <i class="m-icon-swapright m-icon-white"></i>
         &nbsp;
         </a>
       </div>
     </div>
     <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
       <div class="dashboard-stat blue">
         <div class="visual">
           <i class="fa fa-dollar"></i>
         </div>
         <div class="details">
           <div class="number" id="for_billing_count">0</div>
           <div class="desc">
              Total Claims For Billing
           </div>
         </div>
         <a class="more" href="javascript:;" style="cursor:context-menu" data="for_billing">
         View list <i class="m-icon-swapright m-icon-white"></i>
         &nbsp;
         </a>
       </div>
     </div>
   </div>
 </div>


 <div class="page-title" id="page_title">
   MS Loyalty
   <div class="page-toolbar" style="float:right">
       <a class="btn purple-seance btn-md" href="?portfolio=ms_loyalty&page=new_claim">
         <i class="material-icons">add_box</i> Create New Claim
       </a>
   </div>
 </div>

<div class="row">
  <div class="col-md-12">
    <!-- <div class="tabbable-custom "> -->
        <!-- <ul class="nav nav-tabs ">
            <li class="active">
                <a href="#tab_5_1" data-toggle="tab"> CLAIMS </a>
            </li>
            <li>
                <a href="#tab_5_2" data-toggle="tab"> INQUIRY </a>
            </li>

        </ul> -->
        <div class="tab-content">
            <div class="tab-pane active" id="tab_5_1">

              <div class="portlet light ">
                   <div class="portlet-title tabbable-line">
                       <div class="caption">
                           <i class="material-icons font-red">work</i>
                           <span class="caption-subject font-dark bold uppercase">MS Loyalty claim list</span>
                       </div>
                       <ul class="nav nav-tabs">
                           <li class="active">
                               <a href="#pending_claims" data-toggle="tab" aria-expanded="true" id="ms_loyalty_pending_claims"> Claim List </a>
                           </li>
                           <li class="">
                               <a href="#completed_claims" data-toggle="tab" aria-expanded="false" id="ms_loyalty_completd_claims" onclick="loadMSCompleted"> This month's Completed Claims </a>
                           </li>
                           <li class="">
                               <a href="#archived_claims" data-toggle="tab" aria-expanded="false" id="ms_loyalty_archived_claims"> Archived Claims </a>
                           </li>
                       </ul>
                   </div>
                   <div class="portlet-body">
                       <div class="tab-content">
                           <div class="dataTable-container tab-pane active" id="pending_claims">

                             <div id="table_container" class="table-container">

                             </div>

                            </div>
                           <div class=" dataTable-container tab-pane" id="completed_claims">

                             <div id="table_completed_container" class="table-container">

                             </div>

                           </div>
                           <div class="dataTable-container tab-pane " id="archived_claims">

                             <div id="table_archived_container" class="table-container">

                             </div>

                           </div>
                       </div>
                   </div>
               </div>

            </div>

            <div class="tab-pane" id="tab_5_2">

              <div class="portlet light ">
                   <div class="portlet-title tabbable-line">
                       <div class="caption">
                           <i class="material-icons font-red">work</i>
                           <span class="caption-subject font-dark bold uppercase">MS Loyalty inquiry list</span>
                       </div>
                       <ul class="nav nav-tabs">
                           <li class="active">
                               <a href="#pending_claims" data-toggle="tab" aria-expanded="true" id="ms_loyalty_pending_claims"> Today's Pending Inquiry </a>
                           </li>
                           <li class="">
                               <a href="#completed_claims" data-toggle="tab" aria-expanded="false" id="ms_loyalty_completd_claims"> This month's Completed Inquiry </a>
                           </li>
                           <li class="">
                               <a href="#archived_claims" data-toggle="tab" aria-expanded="false" id="ms_loyalty_archived_claims"> Archived Inquiry </a>
                           </li>
                       </ul>
                   </div>
                   <div class="portlet-body">
                       <div class="tab-content">
                           <div class="dataTable-container tab-pane active" id="pending_claims">

                             <div id="table_container" class="table-container">

                             </div>

                            </div>
                           <div class=" dataTable-container tab-pane" id="completed_claims">

                             <div id="table_completed_container" class="table-container">
                               <table class="table" id="ms_loyalty_completed_table">

                                 <thead>
                                   <th class="table-checkbox"><input id="group_check" type="checkbox" class="group-checkable" data-set="#hot_orders_table .checkboxes" onchange="selectItem('multi')"/></th>
                                   <th>Claim ID</th>
                                   <th>SL Number</th>
                                   <th>Customer Number</th>
                                   <th>Customer Name</th>
                                   <th>Description</th>
                                   <th>SAP Project Number</th>
                                   <th>Amount</th>
                                 </thead>

                                 <tbody id="ms_loyalty_completed_table_body">

                                 </tbody>
                               </table>

                             </div>

                           </div>

                           <div class="dataTable-container tab-pane " id="archived_claims">

                             <div id="table_archived_container" class="table-container">
                               archived_claims
                             </div>

                           </div>
                       </div>
                   </div>
               </div>

            </div>

        </div>
    <!-- </div> -->
  </div>

</div>
