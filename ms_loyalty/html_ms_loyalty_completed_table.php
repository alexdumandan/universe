<table class="table" id="ms_loyalty_completed_table">

  <thead>
    <th>Ticket ID</th>
    <th>Customer Name</th>
    <th>Description</th>
    <th>SAP Project Number</th>
    <th>Amount</th>
    <th>Credit Memo</th>
    <th>QA Remarks</th>
    <th>Claim Status</th>
    <th>Date Completed</th>
    <th>Assigned To</th>
  </thead>

  <tbody id="ms_loyalty_completed_table_body">

  </tbody>
</table>
