var oTable;
var completedTable;
var archivedTable;
$(document).ready(function(){

  $("#page_content").load('ms_loyalty/html_ms_loyalty.php',function(){
      $(".more").click(function(){
          loadTable($(this).attr("data"));
      });
      loadTable("");

    loadMSCompleted();
    loadMSArchived();
  });

});

function loadTable(filter){

  $("#table_container").load('ms_loyalty/html_ms_loyalty_pending_table.php',function(){
    loadDashCount();
    $.post("ms_loyalty/ajax_ms_loyalty_table.php", {filter:filter}, function(data){
      $("#ms_loyalty_pending_table_body").html(data);
      initClaimsDataTable("#ms_loyalty_pending_table");
    });
  });
}

function loadDashCount(){
  $.post("ms_loyalty/ajax_ms_loyalty_dash_count.php", function(data){
    // console.log(data);
    var stat_count = $.parseJSON(data);
    $.each(stat_count, function(index, cnt) {
      $("#new_claims_count").text(cnt["new_claims"]);
      $("#pending_claims_count").text(cnt["pending_claims"]);
      $("#for_billing_count").text(cnt["for_billing"]);
      $("#with_findings_count").text(cnt["with_findings"]);
    });
  });

}

function loadMSCompleted(){
  $("#table_completed_container").load('ms_loyalty/html_ms_loyalty_completed_table.php', function(){
    $.post("ms_loyalty/json_Completed_items.php", function(data){
      $("#ms_loyalty_completed_table_body").html(data);
      initCompletedTable("#ms_loyalty_completed_table");
    });
  });
}

function loadMSArchived(){
  $("#table_archived_container").load('ms_loyalty/html_archived_table.php', function(){
    $("#archive_date_toolbar").datepicker();
    $("#ms_loyalty_archived_table").hide();

    $("#archive_date_btn_toolbar").click(function(){
      var archiveDate = $("#archive_date_toolbar").val();

      $.post("ms_loyalty/json_archived_items.php", {archiveDate:archiveDate}, function(data){
        $("#ms_loyalty_archived_table").show();
        $("#ms_loyalty_archived_table").DataTable().destroy();
        $("#ms_loyalty_archived_table_body").html(data);
        initArchivedTable("#ms_loyalty_archived_table");
      });
    });

  });
}


function initClaimsDataTable (tableID) {

      $('#group_check').change(function () {

          var set = jQuery(this).attr("data-set");
          var checked = jQuery(this).is(":checked");
          jQuery(set).each(function () {
              if (checked) {
                  $(this).attr("checked", true);

              } else {
                  $(this).attr("checked", false);
              }

          });

          jQuery.uniform.update(set);

      });

      $(tableID + '_col_filter th').each( function (i) {

        if(i > 0){

         var title = $(this).text();

           $(this).html( '<input class="datatable_col_filter" data-colIndex = '+i+' style="width:100%" type="text" />' );

         }


       } );

        var table = $(tableID);

        oTable = table.DataTable({

            "language": {
                "aria": {
                    "sortAscending": ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                },
                "emptyTable": "<h1>No matching records found</h1>",
                "info": "Showing _START_ to _END_ of _TOTAL_ entries",
                "infoEmpty": "No entries found",
                "infoFiltered": "(filtered1 from _MAX_ total entries)",
                "lengthMenu": "Show _MENU_ entries",
                "search": "Search:",
                "zeroRecords": "<h1>No matching records found</h1>"
            },

            "order": [
                [0, 'desc']
            ],

            "columns": [{
                         "orderable": false
                     }, {
                         "orderable": true
                     }, {
                         "orderable": true
                     }, {
                         "orderable": true
                     }, {
                         "orderable": true
                     }, {
                         "orderable": true
                     }, {
                         "orderable": true
                     }, {
                         "orderable": true
                     }, {
                         "orderable": true
                     }, {
                         "orderable": true
                     }, {
                         "orderable": true
                     }
					 //, {
                       //  "orderable": true
                     //}
                     ],

            "lengthMenu": [
                [15, 30, 50,-1],
                [15, 30, 50,"All"] // change per page values here
            ],
            // "pageLength": 15,

        });


				var wrapper = tableID+"_wrapper";
        var tableWrapper = $(wrapper); // datatable creates the table wrapper by adding with id {your_table_jd}_wrapper

        $(".datatable_col_filter").keyup(function(){

          var thisInput = $(this);
          var columnIndex = $(this).attr("data-colIndex");
          if (columnIndex == ""){
            oTable.search(thisInput.val()).draw();
          }else{

            var keywords = thisInput.val().split(",");

            if(keywords.length > 1){

              var trimKeywords = [];

              $.each(keywords,function(i,kw){

                trimKeywords.push(".*"+kw.trim()+".*");

              })


              var regexString = trimKeywords.join("|");


              oTable.columns(columnIndex)
              .search(regexString,true,true)
              .draw();

            }else{

              oTable.columns(columnIndex)
              .search(thisInput.val())
              .draw();


            }

          }

        });

        // table.find('.group-checkable').change(function () {
        //     var set = jQuery(this).attr("data-set");
        //     var checked = jQuery(this).is(":checked");
        //     jQuery(set).each(function () {
        //         if (checked) {
        //             $(this).attr("checked", true);
        //         } else {
        //             $(this).attr("checked", false);
        //         }
        //     });
        //     jQuery.uniform.update(set);
        // });
        //
        // $(".checkboxes,.group-checkable").uniform();

        tableWrapper.find('.dataTables_length select').select(); // initialize select2 dropdown
        $(".checkboxes,.group-checkable").uniform();
}

function initCompletedTable(tableID){
  var table = $(tableID);
  completedTable = table.DataTable({

      "language": {
          "aria": {
              "sortAscending": ": activate to sort column ascending",
              "sortDescending": ": activate to sort column descending"
          },
          "emptyTable": "<h1>No matching records found</h1>",
          "info": "Showing _START_ to _END_ of _TOTAL_ entries",
          "infoEmpty": "No entries found",
          "infoFiltered": "(filtered1 from _MAX_ total entries)",
          "lengthMenu": "Show _MENU_ entries",
          "search": "Search:",
          "zeroRecords": "<h1>No matching records found</h1>"
      },

      "order": [
          [0, 'desc']
      ],

      "lengthMenu": [
          [15, 30, 50, 100,-1],
          [15, 30, 50, 100,"All"] // change per page values here
      ],
      // "pageLength": 15,

  });


  var wrapper = tableID+"_wrapper";
  var tableWrapper = $(wrapper);

  tableWrapper.find('.dataTables_length select').select();
}

function initArchivedTable(tableID){
  var table = $(tableID);
  archivedTable = table.DataTable({

      "language": {
          "aria": {
              "sortAscending": ": activate to sort column ascending",
              "sortDescending": ": activate to sort column descending"
          },
          "emptyTable": "<h1>No matching records found</h1>",
          "info": "Showing _START_ to _END_ of _TOTAL_ entries",
          "infoEmpty": "No entries found",
          "infoFiltered": "(filtered1 from _MAX_ total entries)",
          "lengthMenu": "Show _MENU_ entries",
          "search": "Search:",
          "zeroRecords": "<h1>No matching records found</h1>"
      },

      "order": [
          [0, 'desc']
      ],

      "lengthMenu": [
          [15, 30, 50, 100,-1],
          [15, 30, 50, 100,"All"] // change per page values here
      ],
      // "pageLength": 15,

  });


  var wrapper = tableID+"_wrapper";
  var tableWrapper = $(wrapper);

  tableWrapper.find('.dataTables_length select').select();
}
