<?php
set_time_limit(0);
include "../../_libs/db_connect.php";
include "../../functions.php";
require "../../../_utils/userDetect.php";
require "../../../EULA/API.php";
$id = $_GET["requestID"];
$user_id = $MQPA_NTLogin;
date_default_timezone_set('US/Eastern');

$query = "SELECT * FROM tb_transaction WHERE transaction_ticket_id = '$id'";

$arrJSON = array();

$result = mysqli_query($con, $query);
$rowNumber = 0;
while($row = mysqli_fetch_assoc($result)) {

  $arrJSON[$rowNumber]["transaction_id"] = $row["transaction_id"];
  $arrJSON[$rowNumber]["ticket_id"] = $row["transaction_ticket_id"];
  $arrJSON[$rowNumber]["customer_number"] = $row["transaction_customer_number"];
  $arrJSON[$rowNumber]["customer_name"] = $row["transaction_customer_name"];
  $arrJSON[$rowNumber]["amount"] = $row["transaction_amount"];
  $arrJSON[$rowNumber]["admin_fee"] = $row["transaction_admin_fee"];
  $arrJSON[$rowNumber]["status"] = $row["transaction_status"];
  $arrJSON[$rowNumber]["credit_memo"] = $row["transaction_credit_memo"];
  $arrJSON[$rowNumber]["cm_created_date"] = $row["transaction_credit_memo_created_date"];
  $arrJSON[$rowNumber]["processor_ntid"] = $row["transaction_processor"];
  $arrJSON[$rowNumber]["processor"] = get_name($row["transaction_processor"], $con);
  // $arrJSON[$rowNumber]["processor"] = eulaGetFullName(strtolower($row["transaction_processor"]));
  $arrJSON[$rowNumber]["date_submitted_to_qa"] = $row["transaction_date_submitted_to_qa"];
  $arrJSON[$rowNumber]["billing_date"] =  date("Y-m-d", strtotime($row["transaction_billing_date"]));
  $arrJSON[$rowNumber]["action_date"] = $row["transaction_action_date"];
  // $arrJSON[$rowNumber]["action_date"] = date("Y-m-d", strtotime($row["transaction_action_date"]));
  $arrJSON[$rowNumber]["billed_by"] = $row["transaction_billed_by"];
  $arrJSON[$rowNumber]["billed_by_name"] = get_name($row["transaction_billed_by"], $con);
  $arrJSON[$rowNumber]["remarks"] = $row["transaction_remarks"];
  $arrJSON[$rowNumber]["sla"] = $row["transaction_sla"];
  $arrJSON[$rowNumber]["aging_bucket"] = $row["transaction_aging_bucket"];
  $arrJSON[$rowNumber]["total"] = $row["transaction_amount"] + $row["transaction_admin_fee"];
  // $arrJSON[$rowNumber]["total"] = $row["transaction_amount"];

  // $arrJSON[$rowNumber]["uploaded_date"] = date("Y-m-d", strtotime($row["attach_date"]));
  // $arrJSON[$rowNumber]["uploaded_date"] = $row["attach_date"];

  $rowNumber++;
}

mysqli_close($con);

echo json_encode($arrJSON);

?>
