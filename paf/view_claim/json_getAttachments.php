<?php
set_time_limit(0);
include "../../_libs/db_connect.php";
require "../../../_utils/userDetect.php";
require "../../../EULA/API.php";
$id = $_GET["requestID"];
$user_id = $MQPA_NTLogin;
date_default_timezone_set('US/Eastern');

$query = "SELECT * FROM tb_attachments WHERE attach_ticket_id = '$id'";

$arrJSON = array();

$result = mysqli_query($con, $query);
$rowNumber = 0;
while($row = mysqli_fetch_assoc($result)) {

  $arrJSON[$rowNumber]["attach_id"] = $row["attach_id"];
  $arrJSON[$rowNumber]["ticket_id"] = $row["attach_ticket_id"];
  $arrJSON[$rowNumber]["filename"] = $row["attach_filename"];
  $arrJSON[$rowNumber]["link"] = $row["attach_links"];
  $arrJSON[$rowNumber]["uploaded_by_ntid"] = $row["attach_uploaded_by"];
  $arrJSON[$rowNumber]["uploaded_by"] = eulaGetFullName(strtolower($row["attach_uploaded_by"]));
  // $arrJSON[$rowNumber]["uploaded_date"] = date("Y-m-d", strtotime($row["attach_date"]));
  $arrJSON[$rowNumber]["uploaded_date"] = $row["attach_date"];

  $rowNumber++;
}

mysqli_close($con);

echo json_encode($arrJSON);

?>
