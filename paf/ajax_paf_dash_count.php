<?php
include "../_libs/db_connect.php";

$query =
		"SELECT transaction_status, count(transaction_status) AS status_count FROM tb_header AS tb_h
		LEFT JOIN tb_transaction AS tb_t ON tb_h.header_ticket_id = tb_t.transaction_ticket_id
		WHERE transaction_status IN ('New Request','For Billing') AND header_portfolio_id='1' GROUP BY transaction_status;";

$result = mysqli_query($con, $query);
$arrJSON = array();
if ($result) {
	while($row = mysqli_fetch_assoc($result)) {
    if ( trim($row["transaction_status"])=="For Billing" ){
        $arrJSON["1"]["for_billing"] = $row["status_count"];
    }
    if ( trim($row["transaction_status"])=="New Request" ){
        $arrJSON["1"]["new_claims"] = $row["status_count"];
    }

	}
}

$query2 =
		"SELECT count(transaction_status) AS pending_status FROM tb_header AS tb_h
		LEFT JOIN tb_transaction AS tb_t ON tb_h.header_ticket_id = tb_t.transaction_ticket_id
		WHERE transaction_status NOT IN ('Completed','New Request')  AND header_portfolio_id='1';";
$result2 = mysqli_query($con, $query2);
if ($result2) {
	while($row2 = mysqli_fetch_assoc($result2)) {
    $arrJSON["1"]["pending_claims"] = $row2["pending_status"];
	}
}


$query3 =
		"SELECT count(transaction_id) AS with_findings FROM tb_header AS tb_h
		LEFT JOIN tb_transaction AS tb_t ON tb_h.header_ticket_id = tb_t.transaction_ticket_id
		WHERE transaction_remarks='With Findings' AND transaction_status NOT IN ('Completed') AND header_portfolio_id='1' ;";
$result3 = mysqli_query($con, $query3);
if ($result3) {
	while($row3 = mysqli_fetch_assoc($result3)) {
    $arrJSON["1"]["with_findings"] = $row3["with_findings"];
	}
}

echo json_encode($arrJSON);
