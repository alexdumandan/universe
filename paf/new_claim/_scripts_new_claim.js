var attachmentCount = 0;
var currentIndex;
var portfolio;
var portfolio_id;
var G_PORTFOLIO;
var G_Requestor;
var customerInfo;
var userDetails;
$(document).ready(function(){

  portfolio = getParameterByName("portfolio");
  checkPortfolioName();
  getUserDetails();

  biFrost.init({
    "maxFilesizeMB" : 25,
    "success" : submitProject,
    "error": errorUpload
  });

  $("#page_content").load('paf/new_claim/html_new_claim.php',function(){
    loadRequestorNameList();
    $("#description,#sap_proj_number,#customer_number,#customer_name,#claim_amount").formFields();
    $("#addClaimRow_btn").click(function(){
      addClaimField();
      $("#description,#sap_proj_number,#date_received,#customer_number"+currentIndex+",#customer_name"+currentIndex+",#claim_amount"+currentIndex).formFields();
    });


    $("#date_received").datepicker({
      format: 'yyyy-mm-dd',
      todayHighlight: true,
      daysOfWeekDisabled: [0, 6]
    });

    $("#submit_claim_btn").click(function(){
          if (checkForInvalidFields() == 0) {
          // showSavingModal();

          removeFileInputsWithoutFile();

          if($(".file-attachments").length > 0){
            console.log("bifrost");
            biFrost.upload();
          }else{
            console.log("submit");
            submitProject(null);
          }
        }else {
          toastr["error"]("Please fill out the missing fields!", "");
        }


    });

  $(".form-field").formFields();


  });

});

function getUserDetails(){
  $.ajax({
    type: "GET",
    url: "ajax_get_user.php",
    data: {},
    success: function(result){
      userDetails = $.parseJSON(result);
      console.log(userDetails);
    }
  });
}

function checkPortfolioName(){

    $.ajax({
      type: "GET",
      url: "json_getPortfolioDetails.php",
      data: {},
      success: function(data){
        G_PORTFOLIO = $.parseJSON(data);

        if (portfolio == G_PORTFOLIO.portfolio_code) {
          portfolio_id = G_PORTFOLIO.portfolio_id;
          console.log(G_PORTFOLIO.portfolio_id);
        }
      }
    });
}

function addClaimField(){

  var claimBody = document.getElementById("claim_fields");
  currentIndex = claim_fields.rows.length;
  var currentRow = claim_fields.insertRow(-1);

  var customer_number = document.createElement("input");
  customer_number.setAttribute("class", "cust-num form-field req-field req-field-empty");
  customer_number.setAttribute("id", "customer_number" + currentIndex);
  customer_number.setAttribute("style", "width: 150px;");
  customer_number.setAttribute("maxlength", "20");
  customer_number.setAttribute("data-empty-message", "Enter customer number.");

  var customer_name = document.createElement("input");
  customer_name.setAttribute("class", "cust-name form-field req-field req-field-empty");
  customer_name.setAttribute("id", "customer_name" + currentIndex);
  customer_name.setAttribute("style", "width: 450px;");
  customer_name.setAttribute("maxlength", "150");
  customer_name.setAttribute("data-empty-message", "Enter customer name.");

  var claim_amount = document.createElement("input");
  claim_amount.setAttribute("class", "amount form-field req-field req-field-empty");
  claim_amount.setAttribute("id", "claim_amount" + currentIndex);
  claim_amount.setAttribute("style", "width: 160px;");
  claim_amount.setAttribute("pattern", "-?[0-9]*(\.[0-9]+)?");
  claim_amount.setAttribute("data-empty-message", "Enter amount.");

  // var admin_fee = document.createElement("input");
  // admin_fee.setAttribute("class", "admin-fee ");
  // admin_fee.setAttribute("id", "admin_fee" + currentIndex);
  // admin_fee.setAttribute("style", "width: 160px;");
  // admin_fee.setAttribute("pattern", "-?[0-9]*(\.[0-9]+)?");

  var deleteRowBox = document.createElement("a");
  deleteRowBox.setAttribute("class", "deleteField btn btn-icon-only red fa fa-times");
  deleteRowBox.setAttribute("id", "deleteRowBox" + currentIndex);

  currentCell = currentRow.insertCell(-1);
  currentCell.appendChild(customer_number);

  currentCell = currentRow.insertCell(-1);
  currentCell.appendChild(customer_name);

  currentCell = currentRow.insertCell(-1);
  currentCell.appendChild(claim_amount);

  // currentCell = currentRow.insertCell(-1);
  // currentCell.appendChild(admin_fee);

  currentCell = currentRow.insertCell(-1);
  currentCell.appendChild(deleteRowBox);

  $(".deleteField").click(function(){
    var deleteRow = this.id;
    console.log(deleteRow);
    $(this).closest('tr').remove();
  });

}


function getClaimsDetails(){
  var arrClaimDetails = [];

  $('#claim_fields tr').each(function() {

        var claimData = new Object();

        claimData.customer_number = $(this).find(".cust-num").val();
        claimData.customer_name = $(this).find(".cust-name").val();
        claimData.claim_amount = $(this).find(".amount").val();
        claimData.claim_amount = claimData.claim_amount.replace(",", "");
        // claimData.admin_fee = $(this).find(".admin-fee").val();

        arrClaimDetails.push(claimData);


  });
  return arrClaimDetails;
  // console.log(arrClaimDetails);
}

function submitProject(files) {

  var requestData = new Object();

  requestData.portfolio_id = portfolio_id;
  requestData.description = $("#description").val();
  requestData.sap_proj_number = $("#sap_proj_number").val();
  // requestData.requestor = $("#requestor").attr("data-value");
  requestData.requestor = $("#requestor").val();
  // requestData.requestor_email = $("#requestor").val();
  // requestData.date_received = $("#date_received").val();
  requestData.comment = $("#comment").val();
  requestData.attachments = files;
  requestData.arrClaimsData = getClaimsDetails();

  console.log(requestData);
  $.ajax({

    type: "POST",
    url: "paf/new_claim/insert_newRequest.php",
    data: requestData,
    success: function(result){
      console.log(result);

      if (result.indexOf("NO ACCESS")>-1) {

      }else if (result.indexOf("::ERROR::")<0) {
        // showRequestID(result);
        alert("Claim Request ID : " + result + " saved.");
        location.href='?portfolio='+getParameterByName("portfolio");
      }else{

      }

    },
    error:function(res){

    }

  });

}

function loadRequestorNameList(){
  var availableTags = [];

  $.ajax({
    type: "GET",
    url: "paf/json_getRequestorNames.php",
    data: {},
    success: function(result){
      G_Requestor = JSON.parse(result);
      var requestor = "<option value='"+ userDetails.ntid+"'>"+userDetails.full_name+"</option>";
      $.each(G_Requestor, function(i, r){
        requestor += "<option value='"+r.ntid+"'>"+r.full_name+"</option>";
      });
      $("#requestor").html(requestor);
      $("#requestor").select2();
    }
  });
}

function addAttachment(){
  var newAttachment = "";

  newAttachment +="<div class='col-md-4 attach-file-group'>";
  newAttachment +="	<div class='input-group'>";
  newAttachment +=" 		<input type='file' class='form-control file-attachments' id='file_"+attachmentCount+"'>";
  newAttachment +="		<span class='input-group-btn'>";
  newAttachment +="			<button class='btn red delete-attachment' type='button'  onclick=\"removeAttachment(this)\"><i class='material-icons'>remove</i> </button>";
  newAttachment +=" 		</span>";
  newAttachment +="	</div>";
  newAttachment +="</div>";

  attachmentCount++;

  $("#file_attachment_group").append(newAttachment);

}

function removeAttachment(element){

  $(element).parent().parent().parent().remove();

}

function scrollToElement(element){

  if (element.length > 0){

      $("body").animate({
            scrollTop: -100 + element.offset().top - $("body").offset().top
        }, 750);

  }
}

function checkForInvalidFields(){

  $(".req-field-empty").trigger("change");
  $(".req-field-empty").addClass("req-field-invalid");

  var invalid = $(".req-field-invalid");

  if( invalid.length > 0){

    $.each(invalid,function(){
      scrollToElement($(this));
      return false;

    });

  }
    return invalid.length;

}

function removeFileInputsWithoutFile(){


  $.each($(".file-attachments"),function(i,o){

    if($(o).val()==""){
      $(o).parents(".col-md-4").remove();
    }

  });

}

function errorUpload(){

  console.log("error on upload");
}

function showSavingModal(){

    $("#saving_modal").modal("show");
    // $("[class*='modal-backdrop in']").remove();
    $("[class*='modal-backdrop in']").removeAttr("style");
}

function showRequestID(RequestID){

    $("#saving_modal .modal-body p").html("Claim Request ID : " + RequestID + " saved.");
    $("#saving_modal_confirm").show();
}

function getCustomerInfo(){
  var availableTags = [];

  $.ajax({
    type: "GET",
    url: "paf/new_claim/json_getCustomerInfo.php",
    data: {},
    success: function(result){
      customerInfo = result;

      $.each(customerInfo, function(i, r){
        availableTags.push(r.customer_name + " (" + r.customer_number + ")")
      });
    }
  })
  // $("#")
}
