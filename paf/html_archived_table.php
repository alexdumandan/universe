<div class="table-toolbar">
 <div class="row">
   <div class="col-md-6">
      <div class="input-group input-medium" id="open_order_toolbar" style="padding-left: 450px;">
        <input type="text" class="form-control" id="archive_date_toolbar" style="width:300px;cursor:pointer;" data-date-end-date="-1d" placeholder="Click here to choose a date"  data-date-format="yyyy-mm-dd" readonly />
        <span class="input-group-btn">
          <button id="archive_date_btn_toolbar" class="btn green" >
               <i class="material-icons">file_download</i>View Archive
          </button>
        </span>
      </div>
   </div>
    <div class="col-md-6">

   </div>
 </div>
</div>

<table class="table" id="paf_archived_table">

  <thead>
    <th>Ticket ID</th>
    <th>Customer Name</th>
    <th>Description</th>
    <th>SAP Project Number</th>
    <th>Amount</th>
    <th>Credit Memo</th>
    <th>QA Remarks</th>
    <th>Claim Status</th>
    <th>Date Completed</th>
    <th>Assigned To</th>
  </thead>

  <tbody id="paf_archived_table_body">

  </tbody>
</table>
