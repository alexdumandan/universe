<?php
set_time_limit(0);
include "_libs/db_connect.php";
include "functions.php";
require "../_utils/userDetect.php";
require "../EULA/API.php";
// require "../PepperPotts/functions.php";
// header('Content-Type: application/xml');
date_default_timezone_set('US/Eastern');
$user_id = $MQPA_NTLogin;

$emailFrom = $_GET['SenderEmailAddress'];
$dateReceived = date("Y-m-d H:i:s");

$arrayInsert = array (
  "header_requestor_email",
  "header_date_received",
);

$queryInsert = mysqli_prepare($con,
                    "INSERT INTO tb_header (".implode($arrayInsert,",").")"
                    . "VALUES (?,?)");

if ($_GET['SenderEmailAddress'] != null) {
  $bind = mysqli_stmt_bind_param($queryInsert, "ss",$emailFrom,$dateReceived);
  if ($bind === false) {
    echo mysqli_error($con);
      mysqli_close($con);
    die ("::ERROR::BIND");

  }
  $exec = mysqli_stmt_execute($queryInsert);
  if ($exec === false) {
    echo mysqli_error($con);
      mysqli_close($con);
    die ("::ERROR::BIND");

  }
}

$newRequestID = mysqli_insert_id($con);

$resultXML = "<result>";
$resultXML = $resultXML . "<universe_items>";
$resultXML = $resultXML . "<id>" . $newRequestID . "</id>";
$resultXML = $resultXML . "</universe_items>";
$resultXML = $resultXML . "</result>";

echo $resultXML;
mysqli_close($con);
?>
