<?php
include "_libs/db_connect.php";
include "functions.php";
require "../_utils/userDetect.php";
require "../EULA/API.php";

$time = strtotime(date("Y-m-d h:i:sa"));

$styles = "";
$scripts = "";


if(isset($_GET["portfolio"])){
    $styles = '<link rel="stylesheet" href="'.$_GET["portfolio"].'/_styles_'.$_GET["portfolio"].'.css?'. $time.'">';
    $scripts = '<script type="text/javascript" src="'.$_GET["portfolio"].'/_scripts_'.$_GET["portfolio"].'.js?'. $time.'"></script>';

    if(isset($_GET["page"])){
        $styles = '<link rel="stylesheet" href="'.$_GET["portfolio"].'/'.$_GET["page"].'/_styles_'.$_GET["page"].'.css?'. $time.'">';
        $scripts = '<script type="text/javascript" src="'.$_GET["portfolio"].'/'.$_GET["page"].'/_scripts_'.$_GET["page"].'.js?'. $time.'"></script>';
    }
}elseif (isset($_GET["page"])) {
  $styles = '<link rel="stylesheet" href="'.$_GET["page"].'/_styles_'.$_GET["page"].'.css?'. $time.'">';
  $scripts = '<script type="text/javascript" src="'.$_GET["page"].'/_scripts_'.$_GET["page"].'.js?'. $time.'"></script>';
}

$user = get_userDetailsByNTID($MQPA_NTLogin);

 ?>

<!DOCTYPE html>
<!--
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.7
Version: 4.7.5
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
Renew Support: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title>UNIVERSE</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="Preview page of Metronic Admin Theme #1 for statistics, charts, recent events and reports" name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
        <link href="../_metronic_template/v3.9.0/theme/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="../_metronic_template/v3.9.0/theme/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css"/>
        <link href="../_metronic_template/v3.9.0/theme/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="../_metronic_template/v3.9.0/theme/assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
        <link href="../_metronic_template/v3.9.0/theme/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css"/>
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGIN STYLES -->
        <link rel="stylesheet" type="text/css" href="../_metronic_template/v3.9.0/theme/assets/global/plugins/select2/select2.css"/>
        <link rel="stylesheet" type="text/css" href="../_metronic_template/v3.9.0/theme/assets/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css"/>
        <link rel="stylesheet" type="text/css" href="../_metronic_template/v3.9.0/theme/assets/global/plugins/datatables/extensions/ColReorder/css/dataTables.colReorder.min.css"/>
        <link rel="stylesheet" type="text/css" href="../_metronic_template/v3.9.0/theme/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>

        <link rel="stylesheet" type="text/css" href="../_metronic_template/v3.9.0/theme/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css"/>
        <link rel="stylesheet" type="text/css" href="../_metronic_template/v3.9.0/theme/assets/global/plugins/select2/select2.css"/>
        <link rel="stylesheet" type="text/css" href="../_metronic_template/v3.9.0/theme/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css"/>
        <link rel="stylesheet" type="text/css" href="_libs/popover/src/jquery.webui-popover.css"/>
        <link rel="stylesheet" type="text/css" href="../_metronic_template/v3.9.0/theme/assets/global/plugins/bootstrap-toastr/toastr.min.css"/>
        <link href="../_metronic_template/v3.9.0/theme/assets/admin/pages/css/timeline.css" rel="stylesheet" type="text/css"/>
        <link href="../_metronic_template/v3.9.0/theme/assets/admin/pages/css/timeline-old.css" rel="stylesheet" type="text/css"/>
        <link href="../_metronic_template/v3.9.0/theme/assets/admin/pages/css/profile.css" rel="stylesheet" type="text/css"/>

        <link href="../_Utils/php/avatars/developer/svgavatars/css/normalize.css" rel="stylesheet">
        <link href="../_Utils/php/avatars/developer/svgavatars/css/spectrum.css" rel="stylesheet">
        <link href="../_Utils/php/avatars/developer/svgavatars/css/svgavatars.css" rel="stylesheet">

        <!-- <link href="../_metronic_template/v3.9.0/theme/assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css"/> -->
        <link href="../_metronic_template/v3.9.0/theme/assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>

        <!-- END PAGE LEVEL PLUGIN STYLES -->
        <!-- BEGIN PAGE STYLES -->
        <!-- END PAGE STYLES -->
        <!-- BEGIN THEME STYLES -->
        <link href="../_metronic_template/v3.9.0/theme/assets/global/css/components-md.css" id="style_components" rel="stylesheet" type="text/css"/>
        <link href="../_metronic_template/v3.9.0/theme/assets/global/css/plugins-md.css" rel="stylesheet" type="text/css"/>
        <link href="../_metronic_template/v3.9.0/theme/assets/admin/layout/css/layout.css" rel="stylesheet" type="text/css"/>
        <link id="style_color" href="../_metronic_template/v3.9.0/theme/assets/admin/layout/css/themes/darkblue.css" rel="stylesheet" type="text/css"/>
        <link href="../_metronic_template/v3.9.0/theme/assets/admin/layout/css/custom.css" rel="stylesheet" type="text/css"/>
        <!-- END THEME STYLES -->
        <!-- <link rel="shortcut icon" href="favicon.ico" /> -->
        <link rel="shortcut icon" href="_images/uni.PNG"/>
      </head>

        <link href="../_metronic_template/v4.7.5/theme/assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
        <link href="../_metronic_template/v4.7.5/theme/assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />

        <?php echo $styles; ?>
        <link href="_styles/common.css?<?php echo $time;?>" rel="stylesheet" type="text/css"/>
        <link href="_styles/material-icons-metronic.css?<?php echo $time;?>" rel="stylesheet" type="text/css"/>
        <link href="_styles/formFields.css?<?php echo $time;?>" rel="stylesheet" type="text/css"/>
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!-- END HEAD -->

    <body class="page-md page-header-fixed page-quick-sidebar-over-content page-full-width">
    <!-- BEGIN HEADER -->
    <div class="page-header md-shadow-z-1-i navbar navbar-fixed-top">
    	<!-- BEGIN HEADER INNER -->
    	<div class="page-header-inner">
    		<!-- BEGIN LOGO -->
    		<div class="page-logo">
    			<a href="?page=home">
    				<h3 style="color:white;margin-top:10px;">UNIVERSE</h3>
    			</a>
    		</div>
    		<!-- END LOGO -->
    		<!-- BEGIN HORIZANTAL MENU -->
    		<!-- DOC: Apply "hor-menu-light" class after the "hor-menu" class below to have a horizontal menu with white background -->
    		<!-- DOC: This is desktop version of the horizontal menu. The mobile version is defined(duplicated) sidebar menu below. So the horizontal menu has 2 seperate versions -->
    		<div class="hor-menu hidden-sm hidden-xs">

    			<ul class="nav navbar-nav" id="page_horizontalMenu">
    				<!-- DOC: Remove data-hover="dropdown" and data-close-others="true" attributes below to disable the horizontal opening on mouse hover -->
    				<?php include "html_horizontalMenu.php"; ?>

    			</ul>
    		</div>
    		<!-- END HORIZANTAL MENU -->
    		<a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
    		</a>
    		<!-- END RESPONSIVE MENU TOGGLER -->
        <div class="top-menu">
    			<ul class="nav navbar-nav pull-right">
            <li class="dropdown dropdown-extended dropdown-notification" id="header_notification_bar">
    					<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="true" id="notif_count_cont" >
    					       <i class="icon-bell"></i>
    					</a>
    					<ul class="dropdown-menu">
    						<li>
    							<ul class="dropdown-menu-list scroller" style="height: 300px; overflow: hidden; width: auto;" data-handle-color="#637283" data-initialized="1" id="notif_list">
    							</ul>
                </li>
    					</ul>
    				</li>
          	<li class="dropdown dropdown-user">
    					<a href="javascript:;" class="dropdown-toggle" id="home_user" style="padding-right:7px">
                <img alt="" class="img-circle" src="../_Utils/php/avatars/IM_avatars/<?php echo $user['ntid']; ?>.png?<?php echo $time;?>">
      					<span class="username username-hide-on-mobile" id="home_user_name">
    					       Hi, <?php echo $user["firstname"]; ?>
                </span>
              </a>

    				</li>
    				<!-- END USER LOGIN DROPDOWN -->
    				<!-- BEGIN QUICK SIDEBAR TOGGLER -->
    			</ul>
    		</div>
    	</div>
    	<!-- END HEADER INNER -->
    </div>
    <!-- END HEADER -->
    <div class="clearfix">
    </div>
    <!-- BEGIN CONTAINER -->
    <div class="page-container">
    	<!-- BEGIN SIDEBAR -->
    	<div class="page-sidebar-wrapper">
    	</div>
    	<!-- END SIDEBAR -->
    	<!-- BEGIN CONTENT -->
    	<div class="page-content-wrapper">
    		<div class="page-content">
    			<!-- BEGIN PAGE HEADER-->
    			<div id="page_header">

    			</div>
    			<!-- END PAGE HEADER-->
    			<div id="page_content">
            <!-- <iframe width="1280" height="720" src="http://www.powtoon.com/embed/geiT7vyvg86/" frameborder="0"></iframe> -->
    			</div>

    		</div>
        <!-- MODAL multimodal -->
        <div id="multi_modal" class="modal" tabindex="-1" aria-hidden="false" data-backdrop="static">
    			<div class="modal-dialog" id="multi_modal_dialog">
    				<div class="modal-content">
    					<div class="modal-header">
    						<h4 class="modal-title" id="multi_modal_title"></h4>
    					</div>
    					<div class="modal-body" id="multi_modal_body" >

    					</div>
    				</div>
    			</div>
    		</div>
        <!-- MODAL multimodal -->



    	</div>
    	<!-- END CONTENT -->

    </div>




    <!-- END CONTAINER -->
    <!-- BEGIN FOOTER -->
    <div class="page-footer">
    	<div class="page-footer-inner">
      <img src="_images/uni-logo.png" alt="" style="height:28px;background-color:#364150;">  UNIVERSE
      </div>
    	<div class="page-footer-inner" style="float:right;">
        <img src="_images/im-logo.png" alt="" style="height:24px;background-color:#364150;">
      </div>
    	<!-- <div class="scroll-to-top">
    		<i class="icon-arrow-up"></i>
    	</div> -->
    </div>
    <!-- END FOOTER -->

    <!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
  <!-- BEGIN CORE PLUGINS -->
  <!--[if lt IE 9]>
  <script src="../_metronic_template/v3.9.0/theme/assets/global/plugins/respond.min.js"></script>
  <script src="../_metronic_template/v3.9.0/theme/assets/global/plugins/excanvas.min.js"></script>
  <![endif]-->
  <script src="../_metronic_template/v3.9.0/theme/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
  <script src="../_metronic_template/v3.9.0/theme/assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
  <!-- IMPORTANT! Load jquery-ui.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
  <script src="../_metronic_template/v3.9.0/theme/assets/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
  <script src="../_metronic_template/v3.9.0/theme/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
  <script src="../_metronic_template/v3.9.0/theme/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
  <script src="../_metronic_template/v3.9.0/theme/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
  <script src="../_metronic_template/v3.9.0/theme/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
  <script src="../_metronic_template/v3.9.0/theme/assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
  <script src="../_metronic_template/v3.9.0/theme/assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
  <script src="../_metronic_template/v3.9.0/theme/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
  <!-- END CORE PLUGINS -->
  <!-- BEGIN PAGE LEVEL PLUGINS -->
  <script type="text/javascript" src="../_metronic_template/v3.9.0/theme/assets/global/plugins/select2/select2.min.js"></script>
  <script type="text/javascript" src="../_metronic_template/v3.9.0/theme/assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
  <!-- <script type="text/javascript" src="../_metronic_template/v3.9.0/theme/assets/global/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
  <script type="text/javascript" src="../_metronic_template/v3.9.0/theme/assets/global/plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js"></script> -->
  <script type="text/javascript" src="../_metronic_template/v3.9.0/theme/assets/global/plugins/datatables/extensions/Scroller/js/dataTables.scroller.min.js"></script>
  <script type="text/javascript" src="../_metronic_template/v3.9.0/theme/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>

  <script src="../_metronic_template/v3.9.0/theme/assets/global/plugins/amcharts/amcharts/amcharts.js" type="text/javascript"></script>
  <script src="../_metronic_template/v3.9.0/theme/assets/global/plugins/amcharts/amcharts/serial.js" type="text/javascript"></script>
  <script src="../_metronic_template/v3.9.0/theme/assets/global/plugins/amcharts/amcharts/pie.js" type="text/javascript"></script>
  <script src="../_metronic_template/v3.9.0/theme/assets/global/plugins/amcharts/amcharts/themes/light.js" type="text/javascript"></script>
  <script src="../_metronic_template/v3.9.0/theme/assets/global/plugins/amcharts/amcharts/themes/patterns.js" type="text/javascript"></script>
  <script src="../_metronic_template/v3.9.0/theme/assets/global/plugins/amcharts/amcharts/themes/chalk.js" type="text/javascript"></script>

  <!-- END PAGE LEVEL PLUGINS -->
  <!-- BEGIN PAGE LEVEL SCRIPTS -->
  <script src="../_metronic_template/v3.9.0/theme/assets/global/scripts/metronic.js" type="text/javascript"></script>
  <script src="../_metronic_template/v3.9.0/theme/assets/admin/layout/scripts/layout.js" type="text/javascript"></script>
  <script src="../_metronic_template/v3.9.0/theme/assets/admin/layout/scripts/quick-sidebar.js" type="text/javascript"></script>
  <script src="../_metronic_template/v3.9.0/theme/assets/admin/layout/scripts/demo.js" type="text/javascript"></script>
  <script src="../_metronic_template/v3.9.0/theme/assets/admin/pages/scripts/index.js" type="text/javascript"></script>
  <script src="../_metronic_template/v3.9.0/theme/assets/admin/pages/scripts/tasks.js" type="text/javascript"></script>

  <script type="text/javascript" src="../_metronic_template/v3.9.0/theme/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
  <script type="text/javascript" src="../_metronic_template/v3.9.0/theme/assets/global/plugins/select2/select2.min.js"></script>
  <script src="../_metronic_template/v3.9.0/theme/assets/global/plugins/jquery.pulsate.min.js" type="text/javascript"></script>
  <script type="text/javascript" src="../_metronic_template/v3.9.0/theme/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js"></script>
  <script type="text/javascript" src="_libs/popover/src/jquery.webui-popover.js"></script>

  <script src="../BIFROST_V3/bifrost/scripts.js?<?php echo $time;?>" type="text/javascript"></script>
  <script type="text/javascript" src="_libs/jquery-timeago-master/jquery.timeago.js"></script>
  <script src="../_metronic_template/v3.9.0/theme/assets/global/plugins/bootstrap-toastr/toastr.min.js"></script>
  <script src="http://momentjs.com/downloads/moment.js"></script>
  <script src="http://momentjs.com/downloads/moment-timezone-with-data.js"></script>

  <script src="../_metronic_template/v3.9.0/theme/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
  <script src="../_metronic_template/v3.9.0/theme/assets/admin/pages/scripts/profile.js" type="text/javascript"></script>
  <script src="../_metronic_template/v3.9.0/theme/assets/global/plugins/jquery.sparkline.min.js" type="text/javascript"></script>
  <!-- <script src="../_metronic_template/v3.9.0/theme/assets/admin/pages/scripts/timeline.js" type="text/javascript"></script> -->
  <!-- <script src="http://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script>
  <script src="../_metronic_template/v3.9.0/theme/assets/global/plugins/gmaps/gmaps.min.js" type="text/javascript"></script> -->
  <script src="../_metronic_template/v3.9.0/theme/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>


  <script src="../_metronic_template/v3.9.0/theme/assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
  <script src="../_metronic_template/v3.9.0/theme/assets/global/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>

  <script src="../_Utils/php/avatars/developer/svgavatars/js/svg.js"></script>
  <script src="../_Utils/php/avatars/developer/svgavatars/js/spectrum.js"></script>
  <script src="../_Utils/php/avatars/developer/svgavatars/js/jquery.scrollbar.js"></script>
  <script src="../_Utils/php/avatars/developer/svgavatars/js/canvg/rgbcolor.js"></script>
  <script src="../_Utils/php/avatars/developer/svgavatars/js/canvg/StackBlur.js"></script>
  <script src="../_Utils/php/avatars/developer/svgavatars/js/canvg/canvg.js"></script>
  <script src="../_Utils/php/avatars/developer/svgavatars/js/svgavatars.en.js"></script>
  <script src="../_Utils/php/avatars/developer/svgavatars/js/svgavatars.core.js"></script>

  <!-- END PAGE LEVEL SCRIPTS -->
    <!-- BEGIN THEME LAYOUT SCRIPTS -->
    <script src="../_metronic_template/v4.7.5/theme/assets/global/plugins/morris/morris.min.js" type="text/javascript"></script>
    <script src="../_metronic_template/v4.7.5/theme/assets/global/plugins/morris/raphael-min.js" type="text/javascript"></script>
    <!-- END THEME LAYOUT SCRIPTS -->
    <?php echo $scripts; ?>
    <script src="../BIFROST_V3/bifrost/scripts.js?<?php echo $time;?>" type="text/javascript"></script>
    <script src="_scripts/common.js?<?php echo $time;?>" type="text/javascript"></script>
    <script src="_scripts/formFields.js?<?php echo $time;?>" type="text/javascript"></script>
    <script src="_scripts/email_fxs.js?<?php echo $time;?>" type="text/javascript"></script>
    <script type="text/javascript" src="_libs/jquery-timeago-master/jquery.timeago.js"></script>

    <script src="../_metronic_template/v4.7.5/theme/assets/global/plugins/counterup/jquery.counterup.min.js" type="text/javascript"></script>

    <script>
        $(document).ready(function()
        {

          // UIExtendedModals.init();



        });
    </script>
    </body>

</html>
