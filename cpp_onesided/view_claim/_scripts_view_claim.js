var attachmentCount = 0;
var requestDetails;
var claimDetails, requestorClaims, processorClaims, billerClaims;
var status;
var G_USERDETAILS;
$(document).ready(function(){
  biFrost.init({
    "maxFilesizeMB" : 25,
    "success" : updateRequest,
    "error": errorUpload
  });

  $("#page_content").load('cpp_onesided/view_claim/html_view_claim.php',function(){
    getUserDetails();
    $("#date_submitted_qa,#billing_date,#action_date").datepicker();
    getRequestDetails(getParameterByName("requestID"));

  });
});

function getUserDetails(){

  $.ajax({
    type: "GET",
    url: "json_getUserDetails.php",
    data: {},
    success: function(data){
      G_USERDETAILS = JSON.parse(data);

      console.log(G_USERDETAILS);
    }
  })
}

function getRequestDetails(requestID){

  $.ajax({
    type: "GET",
    url: "cpp_onesided/view_claim/json_getRequestDetails.php",
    data: {requestID:requestID},
    success: function(data){

      requestDetails = JSON.parse(data);

      $("#ticket_id").html(requestDetails.ticket_id);
      $("#description").html(requestDetails.description);
      $("#sap_proj_number").html(requestDetails.sap_proj_number);
      //$("#date_received").html(requestDetails.date_received);
      $("#requestor").html(requestDetails.requestor_name);
      $("#date_entered").html(requestDetails.date_entered);

      displayAttachments(requestID);
      displayComments(requestID);
      displayClaimDetails(requestID,requestDetails.description);


    }
  });

  $("#save_claim_btn").click(function(){
    removeFileInputsWithoutFile();

    if($(".file-attachments").length > 0){
      console.log("bifrost");
      biFrost.upload();
    }else{
      console.log("submit");
      updateRequest(null);
    }
    location.href='?portfolio='+getParameterByName("portfolio");
  });

}

function updateRequest(files){
  var requestID = getParameterByName("requestID");
  // console.log("save claim btn" + requestID);

  var requestData = new Object();

  requestData.requestID = requestID;
  requestData.comment = $("#comment").val();
  requestData.description = $("#description").val();
  requestData.attachments = files;

  $.ajax({
    type: "POST",
    url: "cpp_onesided/view_claim/updateMSRequest.php",
    data: requestData,
    success: function(result){
      console.log(result);

        if (result.indexOf("NO ACCESS")>-1) {

        }else if (result.indexOf("::ERROR::")<0) {
          alert("Claim Request ID : " + result + " updated.");
          location.href='?portfolio='+getParameterByName("portfolio");
        }else{

        }
    },
    error:function(res){

    }
  });
}

function upDateClaimRow(claimID){
  $("#updateClaimRow_btn").button('loading');

  var cust_number = $('#customer_number'+claimID).val();
  var cust_name = $('#customer_name'+claimID).val();
  var cm_number = $('#cm_number'+claimID).val();
  var ppref = $('#pp_ref'+claimID).val();
  var vendor_debit = $('#vendor_debit'+claimID).val();
  var processor = $('#claim_processor'+claimID).val();
  var status = $('#claim_status'+claimID).val();
  var date_submitted_qa = $('#date_submitted_qa'+claimID).val();
  var action_date = $('#action_date'+claimID).val();
  // var claim_biller = $('#claim_biller'+claimID).val();
  var biller_remarks = $('#biller_remarks'+claimID).val();
  // console.log(status);
  if (G_USERDETAILS.account_type == "Processor") {
    console.log("Processor");
    $.ajax({
      type: "POST",
      url: "cpp_onesided/view_claim/updateClaimRequest.php",
      data: {claimID:claimID,cust_number:cust_number,cust_name:cust_name,cm_number:cm_number,processor:processor,
            status:status,date_submitted_qa:date_submitted_qa,action_date:action_date,
            ppref:ppref,vendor_debit:vendor_debit},
      success: function(result) {
        // console.log(result);
        if (result.indexOf("::ERROR::")<0) {
          toastr["success"]("Item updated!", "");
          // sendDetailstoBilling(result);
        }
      },
      complete:function(){
        $("#updateClaimRow_btn").button('reset');
      }
    });
  }else if (G_USERDETAILS.account_type == "Biller") {
    // console.log(cust_name);
    $.ajax({
      type: "POST",
      url: "cpp_onesided/view_claim/updateClaimBillerRequest.php",
      data: {claimID:claimID, status:status, biller_remarks:biller_remarks},
      success: function(result) {
        // console.log(result);
        if (result.indexOf("::ERROR::")<0) {
          toastr["success"]("Item updated!", "");
          // sendDetailstoBilling(result);
        }
      },
      complete:function(){
        $("#updateClaimRow_btn").button('reset');
      }
    });
  }
}
  var user_access;
function displayClaimDetails(requestID,description){

  $.post("cpp_onesided/ajax_get_user_access.php", function(data){
    user_access = data;
    console.log(user_access);

    if (user_access == "Requestor") {
      loadClaimsForRequestor(requestID,description);
    }else if (user_access == "Processor") {
      loadClaimsForProcessor(requestID,description);
    }else if (user_access == "Biller") {
      loadClaimsForBiller(requestID,description);
    }

  }); //end of user access function
}

function loadClaimsForRequestor(requestID,description){

  $.ajax({
  type: "GET",
  url: "cpp_onesided/view_claim/json_getClaimDetails.php",
  data: {requestID:requestID},
  success: function(data){

    requestorClaims = JSON.parse(data);
    tb = "";
    tb_total = "";
    total_claim = 0;
    var transaction_id;

    if (requestorClaims.length < 1) {
      tb = "<tr>";
      tb = "<td colspan='14'>";
      tb += "<div class='list-group-item item-note'>";
      tb += "<div class='item-note-data' style='text-align:center;'><i class='material-icons' style='font-size:60px;'>payment</i>";
      tb += "<span class='item-note-datetime'></span>";
      tb += "</div>";
      tb += "<div class='item-note-content' style='text-align:center;font-size:18px;'>No claim details found for this item.</div>";
      tb += "</div>";
      tb += "</td>";
      tb += "</tr>";

      $("#claim_fields").html(tb);
    }

    var i=0;
    $.each(requestorClaims, function(i, o){

      status = o.status;
      transaction_id = o.transaction_id;

      $("#description").hide();
      $("#description_view").html(description);
      $("#billing_date,#billed_by,#remarks").show();
      $("#action_date,#action").hide();

      if (status == "New Request") {
        o.processor = "";
        o.billing_date = "";
        o.billed_by = "";
        o.billed_by_name = "";
        o.remarks = "";
      }else if (status == "For Billing" || status == "Resubmitted") {
        o.billing_date = "";
        o.billed_by = "";
        o.remarks = "";
      }else if (o.processor == null) {
        o.processor = "";
      }else if (o.billing_date == null) {
        o.billing_date = "";
      }else if (o.billed_by == null) {
        o.billed_by = "";
      }else if (o.remarks == null) {
        o.remarks = "";
      }

      tb += "<tr>";
      tb += "<td style='width:20px;'><input type='checkbox' class='checkboxes line-status-fields' id='line-status' style='width:20px;' value='"+transaction_id+"'/></td>";
      tb += "<td><input class='cust-num' id='customer_number"+o.transaction_id+"' type='text' style='width:100px;' pattern='-?[0-9]*(\.[0-9]+)?' value='"+o.customer_number+"' hidden>"+o.customer_number+"</td>";
      tb += "<td ><input class='cust-name' id='customer_name"+o.transaction_id+"' type='text' style='width:100px;' pattern='-?[0-9]*(\.[0-9]+)?' value='"+o.customer_name+"' hidden>"+o.customer_name+"</td>";
      tb += "<td>"+o.currency+"</td>";
      tb += "<td>$ "+o.amount+"</td>";

      if (status == "Completed") {
        tb += "<td><input class='cm-number ' id='cm_number"+o.transaction_id+"' type='text' style='width:100px;' pattern='-?[0-9]*(\.[0-9]+)?' hidden>"+o.credit_memo+"</td>";
        tb += "<td><input class='pp-ref ' id='pp_ref"+o.transaction_id+"' type='text' style='width:100px;' pattern='-?[0-9]*(\.[0-9]+)?' hidden>"+o.ppref+"</td>";
        tb += "<td><input class='vendor-debit ' id='vendor_debit"+o.transaction_id+"' type='text' style='width:100px;' pattern='-?[0-9]*(\.[0-9]+)?' hidden>"+o.vendor_debit_number+"</td>";
      }else {
        tb += "<td><input class='cm-number ' id='cm_number"+o.transaction_id+"' type='text' style='width:100px;' pattern='-?[0-9]*(\.[0-9]+)?' hidden>No CM# created</td>";
        tb += "<td><input class='pp-ref ' id='pp_ref"+o.transaction_id+"' type='text' style='width:100px;' pattern='-?[0-9]*(\.[0-9]+)?' hidden>No PP Ref# created</td>";
        tb += "<td><input class='vendor-debit ' id='vendor_debit"+o.transaction_id+"' type='text' style='width:100px;' pattern='-?[0-9]*(\.[0-9]+)?' hidden>No Vendor Debit# created</td>";
      }

      tb += "<td>"+o.processor+"</td>";
      tb += "<td>"+o.status+"</td>";
      tb += "<td></td>";//management approval
      tb += "<td>"+o.billing_date+"</td>";
      tb += "<td>"+o.billed_by_name+"</td>";
      tb += "<td>"+o.remarks+"</td>";
      tb += "</tr>";

      total_claim = total_claim + o.total;

      $("#claim_fields").append(tb);
    });//$.each end

    tb = "";
    tb_total += tb;
    tb_total += "<tr>";
    tb_total += "<td colspan='3'>TOTAL CLAIM</td>";
    tb_total += "<td colspan='1' style='font-weight:bold;'>$ "+total_claim.toLocaleString()+"</td>";
    tb_total += "<td colspan='9'></td>";
    tb_total += "<td></td></tr>";

    $("#claim_fields").append(tb_total);

    checkStatus(status);

    }
  });//end of ajax
}

function loadClaimsForProcessor(requestID,description){

  $.ajax({
  type: "GET",
  url: "cpp_onesided/view_claim/json_getClaimDetails.php",
  data: {requestID:requestID},
  success: function(data){

      processorClaims = JSON.parse(data);
      tb = "";
      tb_total = "";
      total_claim = 0;
      var transaction_id;

      if (processorClaims.length < 1) {
        tb = "<tr>";
        tb = "<td colspan='14'>";
        tb += "<div class='list-group-item item-note'>";
        tb += "<div class='item-note-data' style='text-align:center;'><i class='material-icons' style='font-size:60px;'>payment</i>";
        tb += "<span class='item-note-datetime'></span>";
        tb += "</div>";
        tb += "<div class='item-note-content' style='text-align:center;font-size:18px;'>No claim details found for this item.</div>";
        tb += "</div>";
        tb += "</td>";
        tb += "</tr>";

        $("#claim_fields").html(tb);
      }

      var i=0;

      $.each(processorClaims, function(i, o){
        status = o.status;
        transaction_id = o.transaction_id;

        if (status == "Billed" || status == "Completed" || status == "For Billing") {
          $("#description").hide();
          $("#description_view").html(description);
        }
        $("#billed_by,#remarks").show();

        tb = "<tr>";
        tb += "<td style='width:20px;'><input type='checkbox' class='checkboxes line-status-fields' id='line-status' style='width:20px;' value='"+transaction_id+"'/></td>";
        // if (status == "Billed" || status == "Completed" || status == "For Billing") {
          tb += "<td><input class='cust-num' id='customer_number"+o.transaction_id+"' type='text' style='width:100px;' pattern='-?[0-9]*(\.[0-9]+)?' value='"+o.customer_number+"' hidden>"+o.customer_number+"</td>";
          tb += "<td ><input class='cust-name' id='customer_name"+o.transaction_id+"' type='text' style='width:100px;' pattern='-?[0-9]*(\.[0-9]+)?' value='"+o.customer_name+"' hidden>"+o.customer_name+"</td>";
        // }else {
        //   tb += "<td><input class='cust-num ' id='customer_number"+o.transaction_id+"' type='text' style='width:100px;' maxlength='20' value='"+o.customer_number+"'></td>";
        //   tb += "<td ><input class='cust-name ' id='customer_name"+o.transaction_id+"' type='text' style='width:250px;' maxlength='20' value='"+o.customer_name+"'></td>";
        // }
        tb += "<td>"+o.currency+"</td>";
        tb += "<td>$ "+o.amount+"</td>";
        if (o.credit_memo == "" || o.credit_memo == null) {
          tb += "<td><input class='cm-number ' id='cm_number"+o.transaction_id+"' type='text' style='width:100px;' pattern='-?[0-9]*(\.[0-9]+)?'></td>";
          tb += "<td><input class='pp-ref ' id='pp_ref"+o.transaction_id+"' type='text' style='width:100px;' pattern='-?[0-9]*(\.[0-9]+)?'></td>";
          tb += "<td><input class='vendor-debit ' id='vendor_debit"+o.transaction_id+"' type='text' style='width:100px;' pattern='-?[0-9]*(\.[0-9]+)?'></td>";
        }else {
          tb += "<td><input class='cm-number ' id='cm_number"+o.transaction_id+"' type='text' style='width:100px;' pattern='-?[0-9]*(\.[0-9]+)? value='"+o.credit_memo+"'' hidden>"+o.credit_memo+"</td>";
          tb += "<td><input class='pp-ref ' id='pp_ref"+o.transaction_id+"' type='text' style='width:100px;' pattern='-?[0-9]*(\.[0-9]+)?' value='"+o.ppref+"' hidden>"+o.ppref+"</td>";
          tb += "<td><input class='vendor-debit ' id='vendor_debit"+o.transaction_id+"' type='text' style='width:100px;' pattern='-?[0-9]*(\.[0-9]+)?' value='"+o.vendor_debit_number+"' hidden>"+o.vendor_debit_number+"</td>";
        }
        if (o.processor == "" || o.processor == null) {
          tb += "<td><input class='processor ' id='claim_processor"+o.transaction_id+"' type='text' style='width:160px;' value='"+G_USERDETAILS.ntid+"' value=''hidden>"+G_USERDETAILS.full_name+"</td>";
        }else {
          tb += "<td><input class='processor ' id='claim_processor"+o.transaction_id+"' type='text' style='width:160px;' value='"+o.processor_ntid+"' value=''hidden>"+o.processor+"</td>";
        }

        if (status == "For Validation" ) {
          tb += "<td> <select class='status' id='claim_status"+o.transaction_id+"' style='width:160px;'>";
          tb += "<option value='For Billing'>For Billing</option>";
          tb += "<option value='For Management Approval'>For Management Approval</option>";
          tb += "<option value='Pending'>Pending</option>";
          tb += "<option value='For Validation' selected>For Validation</option>";
          tb += "</select></td>";
        }else if (status == "New Request") {
          tb += "<td> <select class='status' id='claim_status"+o.transaction_id+"' style='width:160px;'>";
          tb += "<option value='New Request' disabled>New Request</option>";
          tb += "<option value='For Management Approval'>For Management Approval</option>";
          tb += "<option value='For Billing'>For Billing</option>";
          tb += "<option value='Pending'>Pending</option>";
          tb += "<option value='For Validation' selected>For Validation</option>";
          tb += "</select></td>";
        }else if (status == "Pending") {
          tb += "<td> <select class='status' id='claim_status"+o.transaction_id+"' style='width:160px;'>";
          tb += "<option value='For Validation'>For Validation</option>";
          tb += "<option value='Pending' selected>Pending</option>";
          tb += "</select></td>";
        }else if (status == "For Billing") {
            tb += "<td><input class='status ' id='claim_status"+o.transaction_id+"' type='text' style='width:100px;' value='"+o.status+"' hidden>"+o.status+"</td>";
        }else if (status == "Billed") {
            tb += "<td> <select class='status' id='claim_status"+o.transaction_id+"' style='width:160px;'>";
            tb += "<option value='Billed' selected>Billed</option>";
            tb += "<option value='For Billing'>For Billing</option>";
            tb += "<option value='Completed'>Completed</option>";
            tb += "</select></td>";
        }else if (status == "For Management Approval") {
            tb += "<td><input class='status ' id='claim_status"+o.transaction_id+"' type='text' style='width:100px;' value='"+o.status+"' hidden>"+o.status+"</td>";
        }else if (status == "Management Approved") {
          tb += "<td> <select class='status' id='claim_status"+o.transaction_id+"' style='width:160px;'>";
          tb += "<option value='Management Approved' disabled selected>Management Approved</option>";
          tb += "<option value='For Billing'>For Billing</option>";
          tb += "<option value='Pending'>Pending</option>";
          tb += "</select></td>";
        }else if (status == "Resubmitted") {
          tb += "<td> <select class='status' id='claim_status"+o.transaction_id+"' style='width:160px;'>";
          tb += "<option value='Resubmitted' selected>Resubmitted</option>";
          tb += "<option value='For Billing' >For Billing</option>";
          tb += "<option value='Pending' >Pending</option>";
          tb += "</select></td>";
        }else if (status == "With Findings") {
          tb += "<td> <select class='status' id='claim_status"+o.transaction_id+"' style='width:160px;'>";
          tb += "<option value='With Findings' selected>With Findings</option>";
          tb += "<option value='For Billing' >For Billing</option>";
          tb += "</select></td>";
          tb += "<td><input class='biller ' id='claim_biller"+o.transaction_id+"' type='text' style='width:100px;' value='"+o.billed_by+"' hidden>"+o.billed_by_name+"</td>";
          tb += "<td><input class='biller-remarks ' id='biller_remarks"+o.transaction_id+"' type='text' style='width:100px;' value='"+o.remarks+"' hidden>"+o.remarks+"</td>";
          tb += "<td></td>";
        }else if (status == "Completed") {
          tb += "<td><input class='status ' id='claim_status"+o.transaction_id+"' type='text' style='width:100px;' value='"+o.status+"' hidden>"+o.status+"</td>";
        }

        tb += "<td></td>";//management approval
        if (status != "Billed" || status != "Completed") {
          tb += "<td></td>";//billing date
        }else {
          tb += "<td><input class='billing-date' id='billing_date"+o.transaction_id+"' type='text' style='width:100px;' value='"+o.billing_date+"' hidden>"+o.billing_date+"</td>";//billing date
        }
        tb += "<td><input class='biller' id='claim_biller"+o.transaction_id+"' type='text' style='width:100px;' value='"+o.billed_by+"' hidden>"+o.billed_by_name+"</td>";
        tb += "<td><input class='biller-remarks ' id='biller_remarks"+o.transaction_id+"' type='text' style='width:100px;' value='"+o.remarks+"' hidden>"+o.remarks+"</td>";
        if (status == "For Management Approval" || status == "For Billing" || status == "Completed") {
          tb += "<td></td>";
        }else {
          tb += "<td><span><a class='btn green' id='updateClaimRow_btn' onclick='upDateClaimRow("+o.transaction_id+")'>Update</a></span></td>";
        }

        tb += "</tr>";

        total_claim = total_claim + o.total;

        $("#claim_fields").append(tb);

      });//end Each


      tb = "";
      tb_total += tb;
      tb_total += "<tr>";
      tb_total += "<td colspan='3'>TOTAL CLAIM</td>";
      tb_total += "<td colspan='2' style='font-weight:bold;'>$ "+total_claim.toLocaleString()+"</td>";
      tb_total += "<td colspan='9'></td>";
      tb_total += "<td></td></tr>";

      $("#claim_fields").append(tb_total);

      checkStatus(status);

    }
  });//ajax end
}

function loadClaimsForBiller(requestID,description){
  // console.log("biller");
  $.ajax({
  type: "GET",
  url: "cpp_onesided/view_claim/json_getClaimDetails.php",
  data: {requestID:requestID},
  success: function(data){

      billerClaims = JSON.parse(data);
      tb = "";
      tb_total = "";
      total_claim = 0;
      var transaction_id;

      if (billerClaims.length < 1) {
        tb = "<tr>";
        tb = "<td colspan='14'>";
        tb += "<div class='list-group-item item-note'>";
        tb += "<div class='item-note-data' style='text-align:center;'><i class='material-icons' style='font-size:60px;'>payment</i>";
        tb += "<span class='item-note-datetime'></span>";
        tb += "</div>";
        tb += "<div class='item-note-content' style='text-align:center;font-size:18px;'>No claim details found for this item.</div>";
        tb += "</div>";
        tb += "</td>";
        tb += "</tr>";

        $("#claim_fields").html(tb);
      }

      var i=0;

      $.each(billerClaims, function(i, o){
        status = o.status;
        transaction_id = o.transaction_id;

        if (status == "Billed" || status == "Completed" || status == "For Billing") {
          $("#description").hide();
          $("#description_view").html(description);
        }
        $("#billed_by,#remarks").show();

        tb = "<tr>";
        tb += "<td style='width:20px;'><input type='checkbox' class='checkboxes line-status-fields' id='line-status' style='width:20px;' value='"+transaction_id+"'/></td>";
        // if (status == "Billed" || status == "Completed" || status == "For Billing") {
          tb += "<td><input class='cust-num' id='customer_number"+o.transaction_id+"' type='text' style='width:100px;' pattern='-?[0-9]*(\.[0-9]+)?' value='"+o.customer_number+"' hidden>"+o.customer_number+"</td>";
          tb += "<td ><input class='cust-name' id='customer_name"+o.transaction_id+"' type='text' style='width:100px;' pattern='-?[0-9]*(\.[0-9]+)?' value='"+o.customer_name+"' hidden>"+o.customer_name+"</td>";
        // }else {
        //   tb += "<td><input class='cust-num ' id='customer_number"+o.transaction_id+"' type='text' style='width:100px;' maxlength='20' value='"+o.customer_number+"'></td>";
        //   tb += "<td ><input class='cust-name ' id='customer_name"+o.transaction_id+"' type='text' style='width:250px;' maxlength='20' value='"+o.customer_name+"'></td>";
        // }
        tb += "<td>"+o.currency+"</td>";
        tb += "<td>$ "+o.amount+"</td>";
        if (o.credit_memo == "" || o.credit_memo == null) {
          tb += "<td><input class='cm-number ' id='cm_number"+o.transaction_id+"' type='text' style='width:100px;' pattern='-?[0-9]*(\.[0-9]+)?'></td>";
          tb += "<td><input class='pp-ref ' id='pp_ref"+o.transaction_id+"' type='text' style='width:100px;' pattern='-?[0-9]*(\.[0-9]+)?'></td>";
          tb += "<td><input class='vendor-debit ' id='vendor_debit"+o.transaction_id+"' type='text' style='width:100px;' pattern='-?[0-9]*(\.[0-9]+)?'></td>";
        }else {
          tb += "<td><input class='cm-number ' id='cm_number"+o.transaction_id+"' type='text' style='width:100px;' pattern='-?[0-9]*(\.[0-9]+)? value='"+o.credit_memo+"'' hidden>"+o.credit_memo+"</td>";
          tb += "<td><input class='pp-ref ' id='pp_ref"+o.transaction_id+"' type='text' style='width:100px;' pattern='-?[0-9]*(\.[0-9]+)?' value='"+o.ppref+"' hidden>"+o.ppref+"</td>";
          tb += "<td><input class='vendor-debit ' id='vendor_debit"+o.transaction_id+"' type='text' style='width:100px;' pattern='-?[0-9]*(\.[0-9]+)?' value='"+o.vendor_debit_number+"' hidden>"+o.vendor_debit_number+"</td>";
        }
        if (o.processor == "" || o.processor == null) {
          tb += "<td><input class='processor ' id='claim_processor"+o.transaction_id+"' type='text' style='width:160px;' value='"+G_USERDETAILS.ntid+"' value=''hidden>"+G_USERDETAILS.full_name+"</td>";
        }else {
          tb += "<td><input class='processor ' id='claim_processor"+o.transaction_id+"' type='text' style='width:160px;' value='"+o.processor_ntid+"' value=''hidden>"+o.processor+"</td>";
        }

        if (status == "For Validation" ) {
          tb += "<td> <select class='status' id='claim_status"+o.transaction_id+"' style='width:160px;'>";
          tb += "<option value='For Billing'>For Billing</option>";
          tb += "<option value='For Management Approval'>For Management Approval</option>";
          tb += "<option value='Pending'>Pending</option>";
          tb += "<option value='For Validation' selected>For Validation</option>";
          tb += "</select></td>";
        }else if (status == "New Request") {
          tb += "<td> <select class='status' id='claim_status"+o.transaction_id+"' style='width:160px;'>";
          tb += "<option value='New Request' disabled>New Request</option>";
          tb += "<option value='For Management Approval'>For Management Approval</option>";
          tb += "<option value='For Billing'>For Billing</option>";
          tb += "<option value='Pending'>Pending</option>";
          tb += "<option value='For Validation' selected>For Validation</option>";
          tb += "</select></td>";
        }else if (status == "Pending") {
          tb += "<td> <select class='status' id='claim_status"+o.transaction_id+"' style='width:160px;'>";
          tb += "<option value='For Validation'>For Validation</option>";
          tb += "<option value='Pending' selected>Pending</option>";
          tb += "</select></td>";
        }else if (status == "For Billing") {
          tb += "<td> <select class='status' id='claim_status"+o.transaction_id+"' style='width:160px;'>";
          tb += "<option value='For Billing' selected>For Billing</option>";
          tb += "<option value='Billed'>Billed</option>";
          tb += "<option value='With Findings'>With Findings</option>";
          tb += "</select></td>";
            // tb += "<td></td>";
        }else if (status == "Billed") {
            tb += "<td><input class='status ' id='claim_status"+o.transaction_id+"' type='text' style='width:100px;' value='"+o.status+"' hidden>"+o.status+"</td>";
        }else if (status == "For Management Approval") {
            tb += "<td><input class='status ' id='claim_status"+o.transaction_id+"' type='text' style='width:100px;' value='"+o.status+"' hidden>"+o.status+"</td>";
        }else if (status == "Management Approved") {
          tb += "<td> <select class='status' id='claim_status"+o.transaction_id+"' style='width:160px;'>";
          tb += "<option value='Management Approved' disabled>Management Approved</option>";
          tb += "<option value='For Billing'>For Billing</option>";
          tb += "<option value='Pending'>Pending</option>";
          tb += "</select></td>";
        }else if (status == "Resubmitted") {
          tb += "<td> <select class='status' id='claim_status"+o.transaction_id+"' style='width:160px;'>";
          tb += "<option value='Resubmitted' selected>Resubmitted</option>";
          tb += "<option value='For Billing' >For Billing</option>";
          tb += "<option value='Pending' >Pending</option>";
          tb += "</select></td>";
        }else if (status == "With Findings") {
          tb += "<td> <select class='status' id='claim_status"+o.transaction_id+"' style='width:160px;'>";
          tb += "<option value='With Findings' selected>With Findings</option>";
          tb += "<option value='For Billing' >For Billing</option>";
          tb += "</select></td>";
          // tb += "<td><input class='biller ' id='claim_biller"+o.transaction_id+"' type='text' style='width:100px;' value='"+o.billed_by+"' hidden>"+o.billed_by_name+"</td>";
          // tb += "<td><input class='biller-remarks ' id='biller_remarks"+o.transaction_id+"' type='text' style='width:100px;' value='"+o.remarks+"' hidden>"+o.remarks+"</td>";
          // tb += "<td></td>";
        }else if (status == "Completed") {
          tb += "<td><input class='status ' id='claim_status"+o.transaction_id+"' type='text' style='width:100px;' value='"+o.status+"' hidden>"+o.status+"</td>";
          tb += "<td><input class='biller ' id='claim_biller"+o.transaction_id+"' type='text' style='width:100px;' value='"+o.billed_by+"' hidden>"+o.billed_by_name+"</td>";
          tb += "<td><input class='biller-remarks ' id='biller_remarks"+o.transaction_id+"' type='text' style='width:100px;' value='"+o.remarks+"' hidden>"+o.remarks+"</td>";
          tb += "<td></td>";

        }
        tb += "<td></td>";//management approval
        tb += "<td><input class='billing-date' id='billing_date"+o.transaction_id+"' type='text' style='width:100px;' value='"+o.billing_date+"' hidden>"+o.billing_date+"</td>";//billing date
        tb += "<td><input class='biller' id='claim_biller"+o.transaction_id+"' type='text' style='width:100px;' value='"+o.billed_by+"' hidden>"+o.billed_by_name+"</td>";
        if (status == "For Billing") {
          tb += "<td><input class='biller-remarks ' id='biller_remarks"+o.transaction_id+"' type='text' style='width:100px;' ></td>";
        }else {
          tb += "<td><input class='biller-remarks ' id='biller_remarks"+o.transaction_id+"' type='text' style='width:100px;' value='"+o.remarks+"' hidden>"+o.remarks+"</td>";
        }
        if (status == "Billed") {
          tb += "<td></td>";
        }else {
          tb += "<td><span><a class='btn green' id='updateClaimRow_btn' onclick='upDateClaimRow("+o.transaction_id+")'>Update</a></span></td>";
        }
        tb += "</tr>";

        total_claim = total_claim + o.total;

        $("#claim_fields").append(tb);

      });//end Each


      tb = "";
      tb_total += tb;
      tb_total += "<tr>";
      tb_total += "<td colspan='3'>TOTAL CLAIM</td>";
      tb_total += "<td colspan='2' style='font-weight:bold;'>$ "+total_claim.toLocaleString()+"</td>";
      tb_total += "<td colspan='9'></td>";
      tb_total += "<td></td></tr>";

      $("#claim_fields").append(tb_total);

      checkStatus(status);

    }
  });//ajax end
}

function checkStatus(status){
  if (status == "Billed") {
    if (user_access == "Processor") {
      $("#other_details").show();
      $("#save_claim_btn").show();
    }else {
      $("#other_details").hide();
      $("#save_claim_btn").hide();
    }
  }else if (status == "Completed") {
    $("#other_details").hide();
    $("#save_claim_btn").hide();
    // $("#description").hide();
    // $("#description_view").html(description);
  }else {
    $("#other_details").show();
  }
}

function displayComments(requestID){
  $.ajax({
    type: "GET",
    url: "cpp_onesided/view_claim/json_getComments.php",
    data: {requestID:requestID},
    success: function(data){

      var comment = JSON.parse(data);
      var list = "";
      // console.log(comment.length);

      if (comment.length < 1) {
        // console.log("no comment");

        list += "<div class='list-group-item item-note'>";
        list += "<div class='item-note-data' style='text-align:center;'><i class='material-icons' style='font-size:60px;'>note</i>";
        list += "<span class='item-note-datetime'></span>";
        list += "</div>";
        list += "<div class='item-note-content' style='text-align:center;font-size:18px;'>No comment found for this item.</div>";
        list += "</div>";

        $("#comments_history_list").html(list);
      }

      $.each(comment, function(i, c){

        list += "<div class='list-group-item item-note'>";
        list += "<div class='item-note-data'>"+c.entered_by;
        list += "<span class='item-note-datetime'>"+$.timeago(c.entry_date)+"</span>";
        list += "</div>";
        list += "<div class='item-note-content'>"+c.comment_history+"</div>";
        list += "</div>";

        $("#comments_history_list").html(list);
      });

    }
  });

}

function displayAttachments(requestID){
  $.ajax({
    type:"GET",
    url: "cpp_onesided/view_claim/json_getAttachments.php",
    data: {requestID:requestID},
    success: function(data){

      var attach = JSON.parse(data);
      // console.log(attach);
      var div = "";

      if (attach.length < 1) {
        // console.log("no attachment");
        div += "<div class='list-group-item item-note'>";
        div += "<div class='item-note-data' style='text-align:center;'><i class='material-icons' style='font-size:60px;'>attach_file</i>";
        div += "<span class='item-note-datetime'></span>";
        div += "</div>";
        div += "<div class='item-note-content' style='text-align:center;font-size:18px;'>No attachment found for this item.</div>";
        div += "</div>";
        // $("#attachment_history").append(div);
      }
      $.each(attach,function(i,o){
        // console.log(o);
        var filename = o["link"];
        var n = filename.lastIndexOf("/");
        var len = filename.length;
        var fname = filename.substring(n+1, len);

        div += "<div class='list-group-item item-note'>";
        div += "<div class='item-note-data'>"+o.uploaded_by;
        div += "<span class='item-note-datetime'>"+$.timeago(o.uploaded_date)+"</span>";
        div += "</div>";
        div += "<div class='item-note-content'><a href='"+o["link"]+"' target='_blank' style='color:#40c4ff;font-size:16px;'><i class='material-icons' style='font-size:20px;'>file_download</i>"+fname+"</a></div>";
        div += "</div>";



      });
      $("#attachment_history").append(div);
    }
  });
}

function addAttachment(){
  var newAttachment = "";

  newAttachment +="<div class='col-md-4 attach-file-group'>";
  newAttachment +="	<div class='input-group'>";
  newAttachment +=" 		<input type='file' class='form-control file-attachments' id='file_"+attachmentCount+"'>";
  newAttachment +="		<span class='input-group-btn'>";
  newAttachment +="			<button class='btn red delete-attachment' type='button'  onclick=\"removeAttachment(this)\"><i class='material-icons'>remove</i> </button>";
  newAttachment +=" 		</span>";
  newAttachment +="	</div>";
  newAttachment +="</div>";

  attachmentCount++;

  $("#file_attachment_group").append(newAttachment);

}

function removeAttachment(element){

  $(element).parent().parent().parent().remove();

}

function errorUpload(){

  console.log("error on upload");
}

function removeFileInputsWithoutFile(){


  $.each($(".file-attachments"),function(i,o){

    if($(o).val()==""){
      $(o).parents(".col-md-4").remove();
    }

  });

}
