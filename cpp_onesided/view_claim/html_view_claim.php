<?php


 ?>

 <div class="row" id="claims_view">
   <div class="col-md-12" id="claims_view_details">

     <div class="panel">
       <div class="portlet-body form">
            <form role="form">
                <div class="form-actions noborder">
                    <button type="button" class="btn blue">Upload to Impulse</button>
                    <button type="button" class="btn blue">Upload to Power</button>
                    <button type="button" class="btn blue">Upload to Dapasoft</button>
                    <button type="button" class="btn blue">Upload to Imatch</button>
                </div>
            </form>
        </div>
     </div>

     <div class="panel" >
       <div class="panel-heading" style="background: white;">
         <h3 class="panel-title font-green"><i class="material-icons">list</i> Canada Price Protection One-Sided Claim Details</h3>
       </div>
       <table class="table" id="claims_view_details_table">
         <tr>
           <td>Request ID</td>
           <td id="ticket_id"></td>
         </tr>
         <tr>
           <td>Description</td>
           <td id="description_view">
             <textarea class="form-control form-field req-field req-field-empty" rows="2" maxlength="1200" id="description" placeholder="Enter claim description" data-empty-message="Enter claim description"></textarea>
           </td>
         </tr>
         <tr>
           <td>Sap Proj #</td>
           <td id="sap_proj_number"></td>
         </tr>
         <!-- <tr>
           <td>Date Request Received</td>
           <td id="date_received"></td>
         </tr> -->
         <tr>
           <td>Requestor</td>
           <td id="requestor"></td>
         </tr>
         <tr>
           <td>Date Ticket Created</td>
           <td id="date_entered"></td>
         </tr>

       </table>
     </div>

     <div class="panel">
       <div class="panel-heading" style="background:white;">
         <h3 class="panel-title font-green"><i class="material-icons">attach_money</i> Claim Table Details</h3>
       </div>

       <div class="row" style="margin:10px;">
         <div class="table-responsive" >
           <table class="table table-bordered table-hover" id="claims_view_details_table">
             <thead>
               <tr>
                 <th class="table-checkbox"></th>
                 <th class="" style="">Customer Number</th>
                 <th class="" style="">Customer Name</th>
                 <th class="" style="">Currency</th>
                 <th class="" style="">Amount</th>
                 <!-- <th style="">Admin Fee</th> -->
                 <th style="">CM#</th>
                 <th style="">PP Ref#</th>
                 <th style="">Vendor Debit#</th>
                 <th style="">Processor</th>
                 <th style="">Status</th>
                 <!-- <th id="sub_to_qa" style="">Date Submitted to QA</th> -->
                 <!-- <th id="action_date" style="">Action Date</th> -->
                 <th id='mgmt_approval' style="">Management Approval</th>
                 <th id='billing_date' style="">Billing Date</th>
                 <th id='billed_by' style="">Billed By</th>
                 <th id='remarks' style="">Remarks</th>
                 <th id='action' style="">Action</th>
               </tr>
             </thead>
             <tbody id="claim_fields">

             </tbody>
           </table>
         </div>
       </div>

     </div>

     <div class="panel">
       <div class="panel-heading" style="background:white;">
         <h3 class="panel-title font-green"><i class="material-icons">note</i> Comments</h3>
       </div>

       <ul class="list-group" id="comments_history_list">

       </ul>
     </div>

     <div class="panel">
       <div class="panel-heading" style="background:white;">
         <h3 class="panel-title font-green"><i class="material-icons">attach_file</i> Attachments</h3>
       </div>

       <ul class="list-group" id="attachment_history">

       </ul>
     </div>

     <div class="panel" id="other_details">
 			<div class="panel-heading" style="background: white;">
 				<h3 class="panel-title font-green"><i class="material-icons">widgets</i> Other Details</h3>
 			</div>
       <table class="table" id="claims_view_details_table">
         <tr>
           <td>Comment </td>
           <td>
             <textarea class="form-control" rows="3" maxlength="1200" id="comment"></textarea>
           </td>
         </tr>
         <tr>
           <td>Attachment </td>
           <td>
             <div class="row" id="file_attachment_group">
               <div class="col-md-4">
                   <button class="btn red btn-block" type="button" id="add_attachment_btn" onclick="addAttachment()"><i class="material-icons">add</i> Add attachments</button>
               </div>

             </div>
           </td>
         </tr>

       </table>
 		</div>

   </div>


   <div class="col-md-12">
     <div class="panel">
       <button class="btn blue btn-lg btn-block" type="button" id="save_claim_btn" style="text-transform:uppercase;"> Save claim request</button>
     </div>
   </div>
 </div>
