<?php
include "../../_libs/db_connect.php";
include "../../functions.php";
require "../../../_utils/userDetect.php";
$user_id = $MQPA_NTLogin;

date_default_timezone_set('US/Eastern');

$claimID = $_POST['claimID'];
$status = $_POST['status'];
$date_submitted_qa = date("Y-m-d H:i:s");
$action_date = date("Y-m-d H:i:s");
$billing_date = date("Y-m-d H:i:s");
$cm_date_entered = date("Y-m-d H:i:s");
// $claim_biller = $_POST['claim_biller'];
$biller_remarks = $_POST['biller_remarks'];


if ($status == "Billed" || $status == "Completed" || $status == "With Findings") {
  $querybilled = "UPDATE tb_transaction SET transaction_status='$status',
            transaction_action_date='$action_date', transaction_billing_date='$billing_date',
            transaction_billed_by='$user_id', transaction_remarks='$biller_remarks' WHERE transaction_id='$claimID'";
  $resultbilled = mysqli_query($con, $querybilled);
}
$lastid = $claimID;

$statusDate = date("Y-m-d H:i:s");
$statusName = $status;
$statusBy = $MQPA_NTLogin;

$arrStatus = array (

  "sh_transaction_id",
  "sh_status",
  "sh_status_date",
  "sh_updated_by",

);

  $insertStatus = mysqli_prepare($con,
                      "INSERT INTO tb_status_history (".implode($arrStatus,",").")"
                      . "VALUES (?,?,?,?)");

    if ($statusName != null) {
      if ($insert === false) {

        ESEO_log($con);
        echo mysqli_error($con);
        mysqli_close($con);
        die ("::ERROR::QUERY");

      };

      $bind = mysqli_stmt_bind_param($insertStatus, "ssss",$lastid,$statusName,$statusDate,$statusBy);
      if ($bind === false) {
        echo mysqli_error($con);
          mysqli_close($con);
        die ("::ERROR::BIND");

      }
      $exec = mysqli_stmt_execute($insertStatus);
      if ($exec === false) {

        echo mysqli_error($con);
        mysqli_close($con);
        die ("::ERROR::EXEC");

      }
    }

echo $lastid;
?>
