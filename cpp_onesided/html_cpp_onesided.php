<?php

 ?>

 <div class="row">
   <div class="col-md-12">
     <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
       <div class="dashboard-stat green">
         <div class="visual">
           <i class="fa fa-dollar"></i>
         </div>
         <div class="details">
           <div class="number" id="new_claims_count">0</div>
           <div class="desc">
              New Claims
           </div>
         </div>
         <a class="more" href="javascript:;" style="cursor:context-menu" data="new_claims">
         View list <i class="m-icon-swapright m-icon-white"></i>
         &nbsp;
         </a>
       </div>
     </div>
     <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
       <div class="dashboard-stat yellow">
         <div class="visual">
           <i class="fa fa-dollar"></i>
         </div>
         <div class="details">
           <div class="number" id="pending_claims_count">0</div>
           <div class="desc">
             WIP Claims
           </div>
         </div>
         <a class="more" href="javascript:;" style="cursor:context-menu" data="pending_claims">
         View list <i class="m-icon-swapright m-icon-white"></i>
         &nbsp;
         </a>
       </div>
     </div>
     <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
       <div class="dashboard-stat red">
         <div class="visual">
           <i class="fa fa-dollar"></i>
         </div>
         <div class="details">
           <div class="number" id="with_findings_count">0</div>
           <div class="desc">
              Total Claims With Findings
           </div>
         </div>
         <a class="more" href="javascript:;" style="cursor:context-menu" data="with_findings">
         View list <i class="m-icon-swapright m-icon-white"></i>
         &nbsp;
         </a>
       </div>
     </div>
     <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
       <div class="dashboard-stat blue">
         <div class="visual">
           <i class="fa fa-dollar"></i>
         </div>
         <div class="details">
           <div class="number" id="for_billing_count">0</div>
           <div class="desc">
              Total Claims For Billing
           </div>
         </div>
         <a class="more" href="javascript:;" style="cursor:context-menu" data="for_billing">
         View list <i class="m-icon-swapright m-icon-white"></i>
         &nbsp;
         </a>
       </div>
     </div>
   </div>
 </div>

 <div class="page-title" id="page_title">
   Canada Price Protection One-Sided
   <div class="page-toolbar" style="float:right">
       <a class="btn purple-seance btn-md" href="?portfolio=cpp_onesided&page=new_claim">
         <i class="material-icons">add_box</i> Create New Claim
       </a>
   </div>
 </div>

<div class="row">
  <div class="col-md-12">
    <div class="tab-content">
      <div class="tab-pane active" id="tab_5_1">
        <div class="portlet light ">
             <div class="portlet-title tabbable-line">
                 <div class="caption">
                     <i class="material-icons font-red">work</i>
                     <span class="caption-subject font-dark bold uppercase">CPP One-Sided claim list</span>
                 </div>
                 <ul class="nav nav-tabs">
                     <li class="active">
                         <a href="#pending_claims" data-toggle="tab" aria-expanded="true" id="cpp_onesided_pending_claims"> Claim List </a>
                     </li>
                     <li class="">
                         <a href="#completed_claims" data-toggle="tab" aria-expanded="false" id="cpp_onesided_completd_claims" onclick="loadCPPoneSidedCompleted"> This month's Completed Claims </a>
                     </li>
                     <li class="">
                         <a href="#archived_claims" data-toggle="tab" aria-expanded="false" id="cpp_onesided_archived_claims"> Archived Claims </a>
                     </li>
                 </ul>
             </div>

             <div class="portlet-body">
               <div class="tab-content">
                   <div class="dataTable-container tab-pane active" id="pending_claims">

                     <div id="table_container" class="table-container">

                     </div>

                    </div>
                   <div class=" dataTable-container tab-pane" id="completed_claims">

                     <div id="table_completed_container" class="table-container">

                     </div>

                   </div>
                   <div class="dataTable-container tab-pane " id="archived_claims">

                     <div id="table_archived_container" class="table-container">

                     </div>

                   </div>
               </div>
             </div>

        </div>
      </div>
    </div>
  </div>
</div>
