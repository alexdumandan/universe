
<li class="classic-menu-dropdown" id="dashboard_menu">
  <a href="?page=dashboard">
  Dashboard
  </a>
</li>

<li class="classic-menu-dropdown" id="my_hub_menu">
  <a href="?page=my_hub">
  Uni Hub
  </a>
</li>

<li class="classic-menu-dropdown" id="claims_menu">
	<a data-toggle="dropdown" href="javascript:;" data-hover="megamenu-dropdown" data-close-others="true">
	Claims <i class="fa fa-angle-down"></i>
	</a>
	<ul class="dropdown-menu pull-left">
		<li>
			<a href="?portfolio=paf">PAF</a>
      <a href="?portfolio=ms_loyalty">MS Loyalty</a>
      <a href="?portfolio=cpp_onesided">CPP One-Sided</a>
      <a href="?portfolio=cpp">CPP</a>
      <a href="?portfolio=paf">VPVLAM</a>
      <a href="?portfolio=paf">US Price Protection</a>
      <a href="?portfolio=paf">Canada Marketing</a>
      <a href="?portfolio=paf">US Marketing</a>
      <a href="?portfolio=paf">US Notification</a>
      <a href="?portfolio=paf">Bill Only</a>
      <a href="?portfolio=paf">Payback</a>
      <a href="?portfolio=paf">HPQ</a>
      <a href="?portfolio=paf">Brandsource</a>
      <a href="?portfolio=paf">EMC-Cisco</a>
      <a href="?portfolio=paf">Coop</a>
      <a href="?portfolio=paf">CVB</a>
      <!-- <a href="?portfolio=paf">CVB (Non-Zeroes</a>
      <a href="?portfolio=paf">CVB (Zeroes)</a> -->
      <a href="?portfolio=paf">PP Errors</a>
 		</li>
	</ul>
</li>

<li class="classic-menu-dropdown" id="inquiry_menu">
  <a href="?page=new_inquiry">
  Inquiries
  </a>
</li>

<li class="classic-menu-dropdown" id="library_menu">
  <a href="?page=library">
  Library
  </a>
</li>

<li class="classic-menu-dropdown" id="support_menu">
  <a href="?page=support">
  Support
  </a>
</li>
